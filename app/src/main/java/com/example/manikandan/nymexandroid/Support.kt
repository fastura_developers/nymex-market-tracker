@file:Suppress("DEPRECATION")

package com.example.manikandan.nymexandroid

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.ComponentName
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import org.json.JSONException
import org.json.JSONObject

/**
 * A simple [Fragment] subclass.
 */
class Support : Fragment() {
    private lateinit var whatsapp: ImageView
    private lateinit var facebook: ImageView
    private lateinit var gmail: ImageView
    private lateinit var mobileNo: TextView
    private lateinit var emailId: TextView
    private lateinit var facebookId: TextView
    private lateinit var ObjectsURLS: ObjectsURLS
    lateinit var coordinatorLayout: CoordinatorLayout
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val v = inflater.inflate(R.layout.fragment_support, container, false)
        ObjectsURLS = ObjectsURLS()
        whatsapp = v.findViewById(R.id.whatsapp)
        facebook = v.findViewById(R.id.facebook)
        gmail = v.findViewById(R.id.gmail)
        coordinatorLayout = v.findViewById(R.id.support)
        mobileNo = v.findViewById(R.id.mobileno)
        emailId = v.findViewById(R.id.emailid)
        facebookId = v.findViewById(R.id.facebookid)
        setWhatsapp()
        setFacebook()
        setgmail()
        NYMEXsupport().execute(ObjectsURLS.support,"",ObjectsURLS.post)
        return v
    }

    private fun setWhatsapp() {
        whatsapp.setOnClickListener {
            val uri = Uri.parse("smsto: " + mobileNo.text.toString())
            //Timber.e("smsNumber %s", uri.toString());
            val i = Intent(Intent.ACTION_SENDTO, uri)
            //i.setPackage("com.whatsapp");
            i.component = ComponentName("com.whatsapp", "com.whatsapp.Conversation")
            startActivity(Intent.createChooser(i, ""))
        }
    }

    private fun setFacebook() {
        facebook.setOnClickListener {
            val intent = Intent(context, Mainframe::class.java)
            intent.putExtra("url", facebookId.text.toString())
            startActivity(intent)
        }
    }

    private fun setgmail() {
        gmail.setOnClickListener {
            val intent = Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                    "mailto", emailId.text.toString(), null)).putExtra(Intent.EXTRA_SUBJECT, "Commodity Market Tracker")
            startActivity(intent)
        }
    }

    @SuppressLint("StaticFieldLeak")
    inner class NYMEXsupport : com.example.manikandan.nymexandroid.AsyncTask.AsyncTaskClass() {
        var progressDialog: ProgressDialog? = null
        var alertDialog: AlertDialog? = null
        var bitmap: Bitmap? = null
        private var result: String = ""

        override fun onPreExecute() {
            progressDialog = ProgressDialog.show(context, "", "Please wait...", true)
            alertDialog = AlertDialog.Builder(context).create()
        }

        override fun onPostExecute(result: String?) {
            progressDialog!!.dismiss()
            alertDialog!!.dismiss()

            //check if exist or not
            try {
                val jsonObject= JSONObject(result)
                if(jsonObject.has(objects.code)) {
                    if (jsonObject.getString(objects.code).equals(objects.success)) {
                        if (jsonObject.has(objects.message)) {
                            objects.snackbarMessage(
                                coordinatorLayout,
                                resources.getColor(R.color.colorAccent),
                                resources.getColor(R.color.white),
                                jsonObject.getString("message")
                            )
                        }
                        val jsonArray = jsonObject.getJSONArray(objects.result)
                        var i = 0
                        while (i < jsonArray.length()) {
                            val obj = jsonArray.getJSONObject(i)
                            mobileNo.text = obj.getString("MobileNo")
                            emailId.text = obj.getString("Gmail")
                            facebookId.text = obj.getString("Facebook")
                            i++
                        }
                    }
                    else if(jsonObject.getString(objects.code).equals(objects.fail))
                    {
                        if (jsonObject.has(objects.message)) {
                            objects.snackbarMessage(
                                coordinatorLayout,
                                resources.getColor(R.color.colorAccent),
                                resources.getColor(R.color.white),
                                jsonObject.getString("message")
                            )
                        }
                    }
                }
            } catch (e: JSONException) {
            } catch (e: NullPointerException) {
                ObjectsURLS = ObjectsURLS()
                Snackbar.make(coordinatorLayout, ObjectsURLS.error, Snackbar.LENGTH_INDEFINITE).show()
            }
            progressDialog!!.dismiss()
            alertDialog!!.dismiss()
        }
    }
}