package com.example.manikandan.nymexandroid

import android.content.Intent
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.MemoryPolicy
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.Picasso

class NewsList (val newsDataItems: List<NewsDataItems>) : RecyclerView.Adapter<NewsList.MyHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.activity_newslist,parent, false)
        return MyHolder(view)
    }

    override fun onBindViewHolder(holder: MyHolder, position: Int) {
        holder.bindItems(newsDataItems[position])
    }

    // return total item from List
    public override fun getItemCount(): Int {
        return newsDataItems.size
    }

    class MyHolder constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var publisheddate = itemView.findViewById<View>(R.id.pubdate) as TextView
        var title = itemView.findViewById<View>(R.id.title) as TextView
        var description = itemView.findViewById<View>(R.id.description) as TextView
        var share = itemView.findViewById<View>(R.id.share) as ImageView
        var thumbnail = itemView.findViewById<View>(R.id.thumbnail) as ImageView
        var relativeLayout = itemView.findViewById<View>(R.id.newscontainer) as RelativeLayout
        lateinit var intent:Intent
        fun bindItems(newsDataItems: NewsDataItems) {
            publisheddate.text = newsDataItems.published_date
            title.text = Html.fromHtml(newsDataItems.title)
            description.text = Html.fromHtml(newsDataItems.description)

            if(newsDataItems.imageURL!=""){
                Picasso.get().load(newsDataItems.imageURL).resize(itemView.resources.getDimension(R.dimen.thumbnailWidth)
                    .toInt(),itemView.resources.getDimension(R.dimen.thumbnailWidth).toInt() ).memoryPolicy(
                    MemoryPolicy.NO_CACHE
                ).networkPolicy(
                    NetworkPolicy.NO_CACHE
                ).into(thumbnail)
            }

            description.setOnClickListener { v ->
                intent = Intent(v.context, Mainframe::class.java)
                intent!!.putExtra("url", newsDataItems.url)
                v.context.startActivity(intent)
            }
            title.setOnClickListener { v ->
                intent = Intent(v.context, Mainframe::class.java)
                intent!!.putExtra("url", newsDataItems.url)
                v.context.startActivity(intent)
            }
            share.setOnClickListener { v ->
                val share = Intent(Intent.ACTION_SEND)
                share.type = "text/plain"
                share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET)

                // Add data to the intent, the receiving app will decide
                // what to do with it.
                share.putExtra(Intent.EXTRA_SUBJECT, newsDataItems.title)
                share.putExtra(Intent.EXTRA_TEXT, newsDataItems.description)
                share.putExtra(Intent.EXTRA_TEXT, newsDataItems.url)
                v.context.startActivity(Intent.createChooser(share, "Commodity News"))
            }
        }
    }

}