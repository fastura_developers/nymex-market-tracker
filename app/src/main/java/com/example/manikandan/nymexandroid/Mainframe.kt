@file:Suppress("DEPRECATION")

package com.example.manikandan.nymexandroid

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Intent
import android.database.Cursor
import android.database.SQLException
import android.database.sqlite.SQLiteDatabase
import android.graphics.Bitmap
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.net.Uri
import android.os.Bundle
import android.os.NetworkOnMainThreadException
import android.provider.Settings
import android.telephony.TelephonyManager
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_levels.*
import org.json.JSONException
import org.json.JSONObject
import org.jsoup.Jsoup
import java.io.*
import java.net.URLEncoder
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class Mainframe : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    lateinit var bundle: Bundle
    lateinit var cursor: Cursor
    var objects = ObjectsURLS()
    lateinit var name: TextView
    lateinit var email: TextView
    lateinit var mobile: TextView
    lateinit var validity: TextView
    lateinit var date: Date
    lateinit var simpleDateFormat2: SimpleDateFormat
    lateinit var time: SimpleDateFormat
    private lateinit var simpleDateFormat: String
    private lateinit var expiredDate: String
    lateinit var startDate: String
    lateinit var endDate: String
    lateinit var startTime: String
    lateinit var endTime: String
    private lateinit var editButton: ImageView
    private lateinit var deviceID: String
    lateinit var coordinatorLayout: CoordinatorLayout
    private lateinit var fragmentTransaction: FragmentTransaction
    lateinit var db: SQLiteDatabase
    private var connect: ConnectivityManager? = null
    lateinit var progressBar:ProgressBar

    @SuppressLint("SetTextI18n", "SimpleDateFormat", "HardwareIds")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //    getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_mainframe)

        date = Calendar.getInstance().time
        db = openOrCreateDatabase("NYMEXDB", MODE_PRIVATE, null)
        db.execSQL(objects.createUserTableIfNotExist)
        //OneSignal.startInit(this).init();
        bundle = Bundle()
        simpleDateFormat2 = SimpleDateFormat("yyyy-MM-dd")
        simpleDateFormat = simpleDateFormat2.format(date)
        progressBar = findViewById(R.id.progress)
        connect = getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        objects = ObjectsURLS()
        coordinatorLayout = findViewById<View>(R.id.mainframe) as CoordinatorLayout



        if (connect!!.getNetworkInfo(0)?.state == NetworkInfo.State.CONNECTED  || connect!!.getNetworkInfo(0)?.state == NetworkInfo.State.CONNECTING || connect!!.getNetworkInfo(1)?.state == NetworkInfo.State.CONNECTING || connect!!.getNetworkInfo(1)?.state == NetworkInfo.State.CONNECTED)
        {
            if(isConnectionIsSpeed()){
                Log.e(resources.getString(R.string.connectionSpeed),resources.getString(R.string.good))
            }else{
                objects.snackbarMessage(coordinatorLayout,resources.getColor(R.color.colorAccent),resources.getColor(R.color.white), resources.getString(R.string.connectionSpeed)+resources.getString(R.string.poor))
            }

            val cursor = db.rawQuery(objects.selectUser, null)
            cursor.moveToFirst()
            if (cursor.count > 0) {

                // Toast.makeText(getApplicationContext(),cursor.getString(3).toString(),Toast.LENGTH_LONG).show();
                val navView = findViewById<BottomNavigationView>(R.id.nav_view)
                navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
                deviceID = Settings.Secure.getString(applicationContext.contentResolver, Settings.Secure.ANDROID_ID)
                val toolbar = findViewById<Toolbar>(R.id.toolbar)
                setSupportActionBar(toolbar)
                val drawer = findViewById<DrawerLayout>(R.id.drawer_layout)
                val navigationView = findViewById<NavigationView>(R.id.nav_view1)
                val toggle = ActionBarDrawerToggle(
                        this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
                drawer.addDrawerListener(toggle)
                toggle.syncState()
                //                Log.d("Player id",OneSignal.getPermissionSubscriptionState().getSubscriptionStatus().getUserId());
                navigationView.setNavigationItemSelectedListener(this)
                val headerView = navigationView.getHeaderView(0)
                //sidebaraccount data
                name = headerView.findViewById<View>(R.id.username) as TextView
                email = headerView.findViewById<View>(R.id.emailid) as TextView
                mobile = headerView.findViewById<View>(R.id.mobileno) as TextView
                validity = headerView.findViewById<View>(R.id.validity) as TextView
                editButton = headerView.findViewById<View>(R.id.editbutton) as ImageView
                Account().execute(objects.accountmcx, (URLEncoder.encode("deviceid", "UTF-8") +"="+URLEncoder.encode(deviceID, "UTF-8")), objects.post)
                checkOffer()
                paymentCancel()
                cursor.close()
            } else {
                val intent = Intent(this@Mainframe, MainActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                startActivity(intent)
            }
        } else if (connect!!.getNetworkInfo(0)?.state == NetworkInfo.State.DISCONNECTED ||
                connect!!.getNetworkInfo(1)?.state == NetworkInfo.State.DISCONNECTED) {
            Snackbar.make(coordinatorLayout, "Network is disconnected", Snackbar.LENGTH_INDEFINITE).show()
        }
    }

    fun isConnectionIsSpeed():Boolean{
        if(connect!!.getNetworkInfo(0)?.getType() == ConnectivityManager.TYPE_WIFI){
            return true
        }
        else if (connect!!.getNetworkInfo(0)?.getType() == ConnectivityManager.TYPE_MOBILE) {
            // check NetworkInfo subtype
            when (connect!!.getNetworkInfo(0)?.subtype) {
                TelephonyManager.NETWORK_TYPE_GPRS -> {
                    return false
                    // Bandwidth between 100 kbps and below
                }
                TelephonyManager.NETWORK_TYPE_1xRTT -> {
                    return false
                    // Bandwidth between 100 kbps and below
                }
                TelephonyManager.NETWORK_TYPE_EDGE -> {
                    return false

                    // Bandwidth between 50-100 kbps
                }
                TelephonyManager.NETWORK_TYPE_CDMA -> {
                    return false

                    // Bandwidth between 50-100 kbps
                }
                TelephonyManager.NETWORK_TYPE_EVDO_0 -> {
                    return true
                    // Bandwidth between 400-1000 kbps
                }
                TelephonyManager.NETWORK_TYPE_EVDO_A -> {
                    return true
                    // Bandwidth between 600-1400 kbps
                }
                TelephonyManager.NETWORK_TYPE_EVDO_A -> {
                    return true
                    // Bandwidth between 600-1400 kbps
                }
                TelephonyManager.NETWORK_TYPE_LTE -> {
                    return true
                    // Bandwidth between 600-1400 kbps
                }
                TelephonyManager.NETWORK_TYPE_IDEN -> {
                    return false
                    // Bandwidth between 600-1400 kbps
                }
                TelephonyManager.NETWORK_TYPE_UNKNOWN -> {
                    return false
                    // Bandwidth between 600-1400 kbps
                }
                TelephonyManager.NETWORK_TYPE_HSPAP -> {
                    return true
                    // Bandwidth between 600-1400 kbps
                }
                TelephonyManager.NETWORK_TYPE_EVDO_B -> {
                    return true
                    // Bandwidth between 600-1400 kbps
                }
                TelephonyManager.NETWORK_TYPE_EHRPD -> {
                    return true
                    // Bandwidth between 600-1400 kbps
                }
                TelephonyManager.NETWORK_TYPE_HSUPA -> {
                    return true
                // Bandwidth between 600-1400 kbps
                }
                TelephonyManager.NETWORK_TYPE_UMTS -> {
                    return true

                    // Bandwidth between 600-1400 kbps
                }
                TelephonyManager.NETWORK_TYPE_HSDPA -> {
                    return true
                    // Bandwidth between 600-1400 kbps
                }
            }
        }
        return false
    }

    private fun checkOffer() {
        //check if offer is checked
        db!!.execSQL(objects.createOfferTableIfNotExist)
        val cursor = db!!.rawQuery(objects.selectOffer, null)
        cursor.moveToFirst()
        if (cursor.count > 0) {
            simpleDateFormat2 = SimpleDateFormat("yyyy-MM-dd")
            val dateBefore = simpleDateFormat2.parse(simpleDateFormat)
            val lastSeen = simpleDateFormat2.parse(cursor.getString(1))
            Log.e("Last seen",cursor.getString(1))
            Log.e("date",dateBefore.toString())
                try {
                    if (dateBefore.after(lastSeen)) {
                        db.execSQL("Drop table offer;")
                        OfferClass().execute(objects.offer, "", objects.post)
                    }else{
                        Log.e("Is user seen a offer?", objects.yes)
                        priyarityToRun()
                    }
                }catch (e:Exception){
                    Log.e("offer exception",e.toString())
                }
            cursor.close()
        }else{
            Log.e("Is user seen a offer?",objects.no)
            OfferClass().execute(objects.offer, "", objects.post)
            cursor.close()
        }
    }

    //Payment message
    private fun paymentCancel() = try {
        intent = intent
        bundle = intent.extras!!
        val msg = bundle.getString("payment_result")
        if (msg!!.isNotEmpty()) {
            objects.snackbarMessage(coordinatorLayout,resources.getColor(R.color.colorAccent),resources.getColor(R.color.white), msg)
        } else {
            //Nothing to show
        }
    } catch (e: NullPointerException) {
    }

    @SuppressLint("Recycle")
    private fun priyarityToRun() {
        fragmentTransaction = supportFragmentManager.beginTransaction()
        intent = intent
        if (intent.hasExtra("url")) {
            bundle = intent.extras!!
            bundle.putString("urllink", bundle.getString("url"))
            val categories = OnlinePageViewer()
            categories.arguments = bundle
            fragmentTransaction.replace(R.id.frames, categories)
            fragmentTransaction.commit()
        } else if (intent.hasExtra("amount")) {
            bundle = intent.extras!!
            cursor = db.rawQuery("SELECT name,email,mobileno FROM NYMEX", null)
            cursor.moveToFirst()
            if (cursor.getCount() > 0 && cursor.getCount() == 1) {
                val userName: String
                val emailId: String
                val mobileNo: String
                userName = cursor.getString(0)
                emailId = cursor.getString(1)
                mobileNo = cursor.getString(2)
                intent = Intent(this@Mainframe, PaymentStage::class.java)
                intent!!.putExtra("name", userName)
                intent!!.putExtra("email", emailId)
                intent!!.putExtra("mobileno", mobileNo)
                intent!!.putExtra("amount", bundle.getString("amount"))
                intent!!.putExtra("planname", bundle.getString("planname"))
                intent!!.putExtra("currencytype", bundle.getString("currencytype"))
                intent!!.putExtra("dollarrate", bundle.getString("dollarrate"))
                intent!!.putExtra("deviceID", deviceID)
                intent!!.putExtra("days", bundle.getString("days"))
                intent!!.putExtra("gateWay", objects.payPal)

                cursor.close()
                startActivity(intent)
            } else {
                objects.snackbarMessage(coordinatorLayout,  resources.getColor(R.color.colorAccent), resources.getColor(R.color.white),resources.getString(R.string.datasDidNotReceived))
            }
        } else if (intent.hasExtra("offer_plan")) {
            bundle = intent.extras!!
            val fragmentTransaction = supportFragmentManager.beginTransaction()
            cursor = db.rawQuery(objects.selectUser, null)
            cursor.moveToFirst()
            if (cursor.getCount() > 0) {
                title = resources.getString(R.string.plans)
                val plans = Plans()
                bundle.putString("expired_date", cursor.getString(3))
                plans.arguments = bundle
                fragmentTransaction.replace(R.id.frames, plans)
                fragmentTransaction.commit()
                cursor.close()
            } else {
                title = resources.getString(R.string.signals)
                val categories = Signals()
                fragmentTransaction.replace(R.id.frames, categories)
                fragmentTransaction.commit()
            }
        } else if (intent.hasExtra("notification")) {
            cursor = db.rawQuery("SELECT expired_date FROM NYMEX", null)
            cursor.moveToFirst()
            if (cursor.getCount() > 0 && cursor.getCount() == 1) {
                try {
                    val dateBefore = simpleDateFormat2.parse(simpleDateFormat)
                    val accountdate = simpleDateFormat2.parse(cursor.getString(0))
                    if (dateBefore.after(accountdate)) {
                        title = resources.getString(R.string.plans)
                        val plans = Plans()
                        bundle!!.putString("expired_date", cursor.getString(0))
                        plans.arguments = bundle
                        fragmentTransaction.replace(R.id.frames, plans)
                        fragmentTransaction.commit()
                    } else {
                        title = resources.getString(R.string.title_notifications)
                        val notifications = Notifications()
                        fragmentTransaction.replace(R.id.frames, notifications)
                        fragmentTransaction.commit()
                    }
                } catch (e: NullPointerException) {
                    objects = ObjectsURLS()
                    Snackbar.make(coordinatorLayout, objects.error, Snackbar.LENGTH_INDEFINITE).show()
                } catch (e: Exception) {
                }
                cursor.close()
            }
        } else {
            val fragmentTransaction = supportFragmentManager.beginTransaction()
            val db = openOrCreateDatabase("NYMEXDB", MODE_PRIVATE, null)
            db.execSQL(objects.createUserTableIfNotExist)
            cursor = db.rawQuery("SELECT expired_date FROM NYMEX", null)
            cursor.moveToFirst()
            if (cursor.getCount() > 0) {
                try {
                    simpleDateFormat2 = SimpleDateFormat("yyyy-MM-dd")
                    simpleDateFormat = simpleDateFormat2.format(date)
                    val dateBefore = simpleDateFormat2.parse(simpleDateFormat)
                    val accountdate = simpleDateFormat2.parse(cursor.getString(0))
                    if (dateBefore.after(accountdate)) {
                        title = resources.getString(R.string.plans)
                        val plans = Plans()
                        bundle!!.putString("expired_date", cursor.getString(0))
                        plans.arguments = bundle
                        fragmentTransaction.replace(R.id.frames, plans)
                        fragmentTransaction.commit()
                    } else {
                        title = resources.getString(R.string.signals)
                        val categories = Signals()
                        fragmentTransaction.replace(R.id.frames, categories)
                        fragmentTransaction.commit()
                    }
                } catch (e: NullPointerException) {
                    objects = ObjectsURLS()
                    Snackbar.make(coordinatorLayout!!, e.toString(), Snackbar.LENGTH_INDEFINITE).show()
                } catch (e: Exception) {
                }
                cursor.close()
            }
        }
        editButton.setOnClickListener {
            val fragmentTransaction = supportFragmentManager.beginTransaction()
            title = resources.getString(R.string.useraccount)
            val useraccount = UserAccount()
            fragmentTransaction.replace(R.id.frames, useraccount)
            fragmentTransaction.commit()
            val drawer = findViewById<DrawerLayout>(R.id.drawer_layout)
            drawer.closeDrawer(GravityCompat.START)
        }

        Thread(Runnable {
            var latestVersion: String =""
            try {
                latestVersion = Jsoup.connect("https://play.google.com/store/apps/details?id=com.fastura.nymex&hl=en")
                    .timeout(30000)
                    .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                    .referrer("http://www.google.com")
                    .get()
                    .select("div.hAyfc:nth-child(4) > span:nth-child(2) > div:nth-child(1) > span:nth-child(1)")
                    .first()
                    .ownText();
            Log.e("DocumentText", latestVersion.toString())
                val versionName=BuildConfig.VERSION_NAME
                Log.e("VersionName", versionName.toString())
                
                if(versionName!=latestVersion){
                    val snackbar = Snackbar
                        .make(coordinatorLayout, resources.getText(R.string.updateMessage), Snackbar.LENGTH_INDEFINITE)
                        .setAction(resources.getString(R.string.updateText)) {
                            val intent = Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.fastura.nymex"))
                            startActivity(intent)
                        }
                    snackbar.show()
                }
            }catch (e: NetworkOnMainThreadException){
            Log.e("NetworkOnMainThread", e.toString())
        }
            runOnUiThread { Log.e("DocumentText", latestVersion.toString()) }
        }).start()
    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        val db = openOrCreateDatabase("NYMEXDB", MODE_PRIVATE, null)
        fragmentTransaction = supportFragmentManager.beginTransaction()
        objects = ObjectsURLS()
        bundle = Bundle()
        when (item.itemId) {
            R.id.navigation_calls -> {
                cursor = db.rawQuery("SELECT expired_date FROM NYMEX", null)
                cursor.moveToFirst()
                if (cursor.getCount() > 0) {
                    try {
                        val dateBefore = simpleDateFormat2!!.parse(simpleDateFormat)
                        val accountdate = simpleDateFormat2!!.parse(cursor.getString(0))
                        if (dateBefore.after(accountdate)) {
                            title = resources.getString(R.string.plans)
                            val plans = Plans()
                            bundle!!.putString("expired_date", cursor.getString(0))
                            plans.arguments = bundle
                            fragmentTransaction.replace(R.id.frames, plans)
                            fragmentTransaction.commit()
                        } else {
                            title = resources.getString(R.string.signals)
                            val categories = Signals()
                            fragmentTransaction.replace(R.id.frames, categories)
                            fragmentTransaction.commit()
                        }
                    } catch (e: Exception) {
                        objects = ObjectsURLS()
                        Snackbar.make(coordinatorLayout, e.toString(), Snackbar.LENGTH_INDEFINITE).show()
                    }
                    cursor.close()
                }
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_levels -> {
                cursor = db.rawQuery("SELECT expired_date FROM NYMEX", null)
                cursor.moveToFirst()
                if (cursor.getCount() > 0 && cursor.getCount() == 1) {
                    try {
                        val dateBefore = simpleDateFormat2.parse(simpleDateFormat)
                        val accountdate = simpleDateFormat2.parse(cursor.getString(0))
                        if (dateBefore.after(accountdate) == true) {
                            title = resources.getString(R.string.plans)
                            val plans = Plans()
                            bundle!!.putString("expired_date", cursor.getString(0))
                            plans.arguments = bundle
                            fragmentTransaction.replace(R.id.frames, plans)
                            fragmentTransaction.commit()
                        } else {
                            title = resources.getString(R.string.pivotpoints)
                            val levels = PivotPoints()
                            fragmentTransaction.replace(R.id.frames, levels)
                            fragmentTransaction.commit()
                        }
                    } catch (e: NullPointerException) {
                        objects = ObjectsURLS()
                        Snackbar.make(coordinatorLayout, objects.error, Snackbar.LENGTH_INDEFINITE).show()
                    } catch (e: Exception) {
                        Snackbar.make(coordinatorLayout, objects.none, Snackbar.LENGTH_INDEFINITE).show()
                    }
                    cursor.close()
                }
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_news -> {
                title = resources.getString(R.string.news)
                val mcxnews = NYMEXNews()
                fragmentTransaction.replace(R.id.frames, mcxnews)
                fragmentTransaction.commit()
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_performance -> {
                title = resources.getString(R.string.performance)
                bundle.putString("url", objects.performance)
                val performance = MonthlySignals()
                performance.arguments = bundle
                fragmentTransaction.replace(R.id.frames, performance)
                fragmentTransaction.commit()
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_notification -> {
                cursor = db.rawQuery("SELECT expired_date FROM NYMEX", null)
                cursor.moveToFirst()
                if (cursor.getCount() > 0 && cursor.getCount() == 1) {
                    try {
                        val dateBefore = simpleDateFormat2.parse(simpleDateFormat)
                        val accountdate = simpleDateFormat2.parse(cursor.getString(0))
                        if (dateBefore.after(accountdate)) {
                            title = resources.getString(R.string.plans)
                            val plans = Plans()
                            bundle!!.putString("expired_date", cursor.getString(0))
                            plans.arguments = bundle
                            fragmentTransaction.replace(R.id.frames, plans)
                            fragmentTransaction.commit()
                        } else {
                            title = resources.getString(R.string.title_notifications)
                            val notifications = Notifications()
                            fragmentTransaction.replace(R.id.frames, notifications)
                            fragmentTransaction.commit()
                        }
                    } catch (e: NullPointerException) {
                        objects = ObjectsURLS()
                        Snackbar.make(coordinatorLayout!!, objects!!.error, Snackbar.LENGTH_INDEFINITE).show()
                    } catch (e: Exception) {
                    }
                    cursor.close()
                }
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onBackPressed() {
        val drawer = findViewById<DrawerLayout>(R.id.drawer_layout)
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.mainframe, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId
        if (id == R.id.action_exit) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        val id = item.itemId
        bundle = Bundle()
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        if (id == R.id.nav_home) {
            db = openOrCreateDatabase("NYMEXDB", MODE_PRIVATE, null)
            cursor = db.rawQuery("SELECT expired_date FROM NYMEX", null)
            cursor.moveToFirst()
            if (cursor.getCount() > 0) {
                try {
                    val dateBefore = simpleDateFormat2.parse(simpleDateFormat)
                    val accountdate = simpleDateFormat2.parse(cursor.getString(0))
                    if (dateBefore.after(accountdate)) {
                        title = resources.getString(R.string.plans)
                        val plans = Plans()
                        bundle.putString("expired_date", cursor.getString(0))
                        plans.arguments = bundle
                        fragmentTransaction.replace(R.id.frames, plans)
                    } else {
                        title = resources.getString(R.string.signals)
                        val categories = Signals()
                        fragmentTransaction.replace(R.id.frames, categories)
                    }
                } catch (e: NullPointerException) {
                    objects.snackbarMessage(coordinatorLayout, resources.getColor(R.color.colorAccent), resources.getColor(R.color.white), objects.error)
                } catch (e: Exception) {
                }
                cursor.close()
            }
        } else if (id == R.id.nav_gallery) {
            title = resources.getString(R.string.videos)
            val categories = NYMEXYouTube()
            fragmentTransaction.replace(R.id.frames, categories)
        } else if (id == R.id.nav_plans) {
            cursor = db.rawQuery("SELECT expired_date FROM NYMEX", null)
            cursor.moveToFirst()
            if (cursor.count > 0) {
                expiredDate = cursor.getString(0)
            }
            title = resources.getString(R.string.plans)
            val plans = Plans()
            bundle.putString("expired_date", expiredDate)
            plans.arguments = bundle
            fragmentTransaction.replace(R.id.frames, plans)
        } else if (id == R.id.nav_tools) {
            title = resources.getString(R.string.calendar)
            val holidays = Holidays()
            fragmentTransaction.replace(R.id.frames, holidays)
        } else if (id == R.id.nav_share) {
            val share = Intent(Intent.ACTION_SEND)
            share.type = "text/plain"
            share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET)
            share.putExtra(Intent.EXTRA_SUBJECT, "Commodity Market Tracker")
            share.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=com.fastura.nymex")
            startActivity(Intent.createChooser(share, "Commodity Market Tracker"))
        } else if (id == R.id.nav_support) {
            title = resources.getString(R.string.support)
            val support = Support()
            support.arguments = bundle
            fragmentTransaction.replace(R.id.frames, support)
        } else if (id == R.id.nav_about) {
            title = resources.getString(R.string.aboutus)
            val aboutus = AboutUs()
            fragmentTransaction.replace(R.id.frames, aboutus)
        } else if (id == R.id.nav_ratme) {
            //  setTitle(getResources().getString(R.string.rateme));
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.fastura.nymex"))
            startActivity(intent)
        }
        fragmentTransaction.commit()
        val drawer = findViewById<DrawerLayout>(R.id.drawer_layout)
        drawer.closeDrawer(GravityCompat.START)
        return true
    }

    inner class Account : com.example.manikandan.nymexandroid.AsyncTask.AsyncTaskClass() {
        var alertDialog: AlertDialog? = null
        var bitmap: Bitmap? = null
        private var result: String = ""

        override fun onPreExecute() {

            //    progressDialog = ProgressDialog.show(context, "", "Please wait...", true);
            alertDialog = AlertDialog.Builder(applicationContext).create()
        }

        override fun onPostExecute(result: String?) {
            //check if exist or not
            try {
                if (result!!.contains(objects.code)) {
                    var jsonObject = JSONObject(result)

                    //Log.e("Register",jsonObject.getJSONArray( feeditems.result).toString())

                    //Log.e("Register",jsonObject.getString(objects.code).toString())
                    if ((jsonObject.getString(objects.code).equals(this@Mainframe.objects.success)) && jsonObject.has(objects.result)) {

                        if (jsonObject.getString(objects.code).equals(objects.success)) {
                            Log.e("Register", jsonObject.getJSONArray(this@Mainframe.objects.result).toString())
                        }

                        if ((jsonObject.getString(objects.code).equals(this@Mainframe.objects.success)) && jsonObject.has(objects.result)) {

                            if (jsonObject.has(this@Mainframe.objects.message)) {
                                objects.snackbarMessage(coordinatorLayout, resources.getColor(R.color.colorPrimary), resources.getColor(R.color.white), jsonObject.getString("message"))
                            }

                            val date = Calendar.getInstance().time
                            val simpleDateFormat = simpleDateFormat2.format(date)
                            try {
                                val dateBefore = simpleDateFormat2.parse(simpleDateFormat)
                                val jsonArray = jsonObject.getJSONArray(this@Mainframe.objects.result)
                                val jsonObject = jsonArray.getJSONObject(0)
                                val name = jsonObject.getString("UserName").substring(0)
                                val userEmailId = jsonObject.getString("UserEmail")
                                val userMobileNo = jsonObject.getString("UserMobileNo")
                                val expiredDate = jsonObject.getString("AppExipiredDate")
                                this@Mainframe.name.text = name
                                email.text = userEmailId
                                mobile.text = userMobileNo
                                if (simpleDateFormat2.parse(jsonObject.getString("AppExipiredDate")).before(dateBefore) == true) {
                                    validity.text = "${this@Mainframe.objects.accountExpiredOn}+$expiredDate"
                                    Log.e("Account expire message",validity.text.toString())
                                    validity.setTextColor(resources.getColor(R.color.colorAccent))
                                } else {
                                    validity.text = "${this@Mainframe.objects.expiredOn}$expiredDate"
                                    validity.setTextColor(resources.getColor(R.color.lightgreen))
                                }
                                db = openOrCreateDatabase("NYMEXDB", MODE_PRIVATE, null)
                                db.execSQL(this@Mainframe.objects.createUserTableIfNotExist)
                                db.execSQL("Update NYMEX set name='$name',mobileno=$userMobileNo,email='$userEmailId',expired_date='$expiredDate';")
                            } catch (e: JSONException) {
                            } catch (e: NullPointerException) {
                                this@Mainframe.objects = ObjectsURLS()
                                objects.snackbarMessage(coordinatorLayout, resources.getColor(R.color.colorAccent), resources.getColor(R.color.white), this@Mainframe.objects!!.error)
                            } catch (e: Exception) {
                            }
                        }
                    } else if (jsonObject.getString(objects.code).equals(objects.fail)) {
                        if (jsonObject.has(this@Mainframe.objects.message)) {
                            objects.snackbarMessage(coordinatorLayout, resources.getColor(R.color.colorAccent), resources.getColor(R.color.white), jsonObject.getString("message"))
                        }
                        db = openOrCreateDatabase("NYMEXDB", MODE_PRIVATE, null)
                        db.execSQL("Drop table NYMEX")
                        db.close()
                        val intent = Intent(applicationContext, MainActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                        applicationContext.startActivity(intent)
                    }
                }
            } catch (e: JSONException) {
                Snackbar.make((coordinatorLayout), this@Mainframe.objects.error, Snackbar.LENGTH_SHORT).show()
            } catch (e: SQLException) {
                Snackbar.make((coordinatorLayout), this@Mainframe.objects.error, Snackbar.LENGTH_SHORT).show()
            } catch (e: NullPointerException) {
                Snackbar.make((coordinatorLayout), this@Mainframe.objects.error, Snackbar.LENGTH_SHORT).show()
            }
        }
    }


        //offers
        inner class OfferClass : com.example.manikandan.nymexandroid.AsyncTask.AsyncTaskClass() {
            private var result: String = ""
            private var dialog: ProgressDialog? = null

            override fun onPreExecute() {
                progressBar.visibility=View.VISIBLE
            }

            @SuppressLint("SimpleDateFormat", "Recycle")
            override fun onPostExecute(result: String?) {
                progressBar.visibility=View.GONE
                //  Snackbar.make(coordinatorLayout, result, Snackbar.LENGTH_SHORT).show();
                //  Toast.makeText(getApplicationContext(),result,Toast.LENGTH_LONG).show();
                try {
                    var i: Int
                    val jsonObject=JSONObject(result)
                    if(jsonObject.has(objects.code)) {
                        if (jsonObject.getString(objects.code).equals(objects.success)) {
                            if (jsonObject.has(objects.message)) {
                                objects.snackbarMessage(
                                    levelspage,
                                    resources.getColor(R.color.colorPrimary),
                                    resources.getColor(R.color.white),
                                    jsonObject.getString("message")
                                )
                            }

                            val jsonArray = jsonObject.getJSONArray(objects.result)
                            val jsonObject1 = jsonArray.getJSONObject(0)
                            startDate = jsonObject1.getString("startdate")
                            endDate = jsonObject1.getString("enddate")
                            startTime = jsonObject1.getString("starttime")
                            endTime = jsonObject1.getString("endtime")
                            // string to time conversion
                            time = SimpleDateFormat("hh.mm")
                            val start = time.parse(startTime)
                            val end = time.parse(endTime)
                            val current = time.parse(time.format(Date()))
                            //offer shows time
                            if (current.after(start) && current.before(end)) {   //&& end.before(current)
                                //    Toast.makeText(getApplicationContext(),"before",Toast.LENGTH_LONG).show();
                                db.execSQL(objects.createOfferTableIfNotExist)
                                cursor = db.rawQuery(objects.selectOffer, null)
                                cursor.moveToFirst()
                                if (cursor.getCount() > 0) {
                                    Log.e("offer table have a data",cursor.count.toString())
                                } else {
                                    Log.e("offer havenot a data",cursor.count.toString())

                                    val intent = Intent(this@Mainframe, Offer::class.java)
                                    intent.putExtra("url", jsonObject1.getString("url"))
                                    intent.putExtra("offername", jsonObject1.getString("offername"))
                                    startActivity(intent)
                                }
                            } else {
                                db.execSQL(objects.createOfferTableIfNotExist)
                                cursor = db.rawQuery(objects.selectOffer, null)
                                cursor.moveToFirst()
                                if (cursor.getCount() > 0) {
                                    db.execSQL("Drop table offer;")
                                    cursor.close()
                                }
                                priyarityToRun()
                            }
                        }
                        else if (jsonObject.getString(objects.code).equals(objects.fail)) {
                            if (jsonObject.has(objects.message)) {
                                objects.snackbarMessage(
                                    coordinatorLayout,
                                    resources.getColor(R.color.colorAccent),
                                    resources.getColor(R.color.white),
                                    jsonObject.getString("message")
                                )
                            }
                            priyarityToRun()
                        }
                    }
                } catch (e: JSONException) {
                    Log.e("JSONException",e.toString())
                } catch (e: NullPointerException) {
                    Snackbar.make(coordinatorLayout, this@Mainframe.objects.error, Snackbar.LENGTH_SHORT).show()
                } catch (e: ParseException) {
                    e.printStackTrace()
                }
            }
        }
    }
