@file:Suppress("DEPRECATION")

package com.example.manikandan.nymexandroid

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.database.sqlite.SQLiteDatabase
import android.graphics.Color
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import org.json.JSONException
import org.json.JSONObject
import java.net.URLEncoder

/**
 * A simple [Fragment] subclass.
 */
class UserAccount : Fragment() {
    lateinit var name: EditText
    lateinit var email: EditText
    lateinit var mobileNo: EditText
    lateinit var variables: ObjectsURLS
    private lateinit var deviceId: String
    lateinit var coordinatorLayout: CoordinatorLayout
    lateinit var update: Button
    @SuppressLint("HardwareIds")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val v = inflater.inflate(R.layout.fragment_useraccount, container, false)
        variables = ObjectsURLS()
        this.name = v.findViewById(R.id.name)
        this.email = v.findViewById(R.id.mail)
        mobileNo = v.findViewById(R.id.mobileno)
        deviceId = Settings.Secure.getString(context!!.contentResolver, Settings.Secure.ANDROID_ID)
        coordinatorLayout = v.findViewById<View>(R.id.useraccount) as CoordinatorLayout
        update = v.findViewById<View>(R.id.update) as Button
        AlreadyRegistered().execute(variables.accountmcx, (URLEncoder.encode("deviceid", "UTF-8") + "=" + URLEncoder.encode(deviceId, "UTF-8")),variables.post)
        //AlreadyRegistered.execute(variables.accountmcx,deviceid,name,email,mobileno);
        validation()
        return v
    }

    @SuppressLint("HardwareIds")
    private fun validation() {
        deviceId = Settings.Secure.getString(context!!.contentResolver, Settings.Secure.ANDROID_ID)
        this.update.setOnClickListener {
            if ((name.text.toString().length >= 3) && (mobileNo.text.length > 9) && (email.text.isNotEmpty())) {
                AlreadyRegistered().execute(variables.updateaccount, (URLEncoder.encode("deviceid", "UTF-8") + "=" + URLEncoder.encode(deviceId, "UTF-8") + "&" + URLEncoder.encode("name", "UTF-8") + "=" + URLEncoder.encode(name.text.toString(), "UTF-8") + "&" + URLEncoder.encode("mail", "UTF-8") + "=" + URLEncoder.encode(email.text.toString(), "UTF-8") + "&" + URLEncoder.encode("phone", "UTF-8") + "=" + URLEncoder.encode(mobileNo.text.toString(), "UTF-8")),variables.post)
            } else {
                if (name.text.toString().isEmpty()) {
                    name.hint = "Name is empty"
                    name.setHintTextColor(Color.RED)
                }
                if (name.text.toString().isNotEmpty() && name.text.toString().length < 3) {
                    Toast.makeText(context, "Name is Invalid", Toast.LENGTH_SHORT).show()
                }
                if (mobileNo.text.toString().isEmpty()) {
                    mobileNo.hint = "Mobile Number is empty"
                    mobileNo.setHintTextColor(Color.RED)
                }
                if (mobileNo.text.toString().length in 1..9) {
                    Toast.makeText(context, "Mobile Number is Invalid", Toast.LENGTH_SHORT).show()
                }
                if (email.text.toString().isEmpty()) {
                    email.hint = "EmailID is empty"
                    email.setHintTextColor(Color.RED)
                }
                val emailtext = email.text.toString().trim { it <= ' ' }
                val emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"

// onClick of button perform this simplest code.
                if (!emailtext.equals(emailPattern)) {
                    Toast.makeText(context, "Invalid Email address", Toast.LENGTH_LONG).show()
                }
            }
        }
    }

     @SuppressLint("StaticFieldLeak")
     inner class AlreadyRegistered: com.example.manikandan.nymexandroid.AsyncTask.AsyncTaskClass() {
        private lateinit var db: SQLiteDatabase
        private lateinit var userName: String
        private lateinit var userMobileNo: String
        private lateinit var userEmailId: String
         private var result: String = ""
         private lateinit var dialog: ProgressDialog

        override fun onPreExecute() {
            dialog = ProgressDialog(context)
            dialog.setMessage("Processing...")
            dialog.setCancelable(false)
            dialog.setInverseBackgroundForced(false)
            dialog.show()
        }

        override fun onPostExecute(result: String?) {
            dialog.dismiss()
            try {
                var jsonObject = JSONObject(result)
                if(jsonObject.has(objects.code)) {
                    if (jsonObject.getString(objects.code).equals(objects.success)) {
                        if (jsonObject.has(objects.message)) {
                            objects.snackbarMessage(coordinatorLayout, resources.getColor(R.color.colorPrimary), resources.getColor(R.color.white), jsonObject.getString("message"))
                        }
                        if(jsonObject.has(objects.result)) {
                            val jsonArray = jsonObject.getJSONArray(variables.result)
                            val jsonObject = jsonArray.getJSONObject(0)
                            userName = jsonObject.getString("UserName")
                            name.setText(jsonObject.getString("UserName"))
                            userMobileNo = jsonObject.getString("UserMobileNo")
                            mobileNo.setText(jsonObject.getString("UserMobileNo"))
                            userEmailId = jsonObject.getString("UserEmail")
                            email.setText(jsonObject.getString("UserEmail"))
                            if (result!!.contains("true")) {
                                val no = userMobileNo.toLong()
                                db = context!!.openOrCreateDatabase(
                                    "NYMEXDB",
                                    Context.MODE_PRIVATE,
                                    null
                                )
                                db.execSQL("CREATE TABLE IF NOT EXISTS NYMEX(name text,mobileno BIGINT(15) primary key,email text,expired_date text);")
                                db.execSQL("Update NYMEX set name='$userName',mobileno=$no,email='$userEmailId';")
                                Toast.makeText(
                                    context,
                                    "Account details are updated",
                                    Toast.LENGTH_LONG
                                )
                                    .show()
                                val intent = Intent(context, Mainframe::class.java)
                                startActivity(intent)
                            }
                        }
                    } else if (jsonObject.getString(objects.code).equals(objects.fail)) {
                        Log.e("201",jsonObject.toString())
                        if (jsonObject.has(objects.message)) {
                            objects.snackbarMessage(
                                coordinatorLayout,
                                resources.getColor(R.color.colorAccent),
                                resources.getColor(R.color.white),
                                jsonObject.getString("message")
                            )
                        }
                    }
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            } catch (e: NullPointerException) {
                Snackbar.make((coordinatorLayout), "Details not found. please try again!..", Snackbar.LENGTH_INDEFINITE).show()
            }
        }
    }
}