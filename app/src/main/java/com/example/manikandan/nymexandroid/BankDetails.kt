package com.example.manikandan.nymexandroid

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class BankDetails (val bankDataItems: List<BankDataItems>) : RecyclerView.Adapter<BankDetails.MyHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.activity_bankdetails, parent, false)
        return MyHolder(view)
    }

    override fun onBindViewHolder(holder: MyHolder, position: Int) {
        holder.bindItems(bankDataItems[position])
    }

    // return total item from List
    public override fun getItemCount(): Int {
        return bankDataItems.size
    }

    class MyHolder constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(bankDataItems:BankDataItems) {
            val objects = ObjectsURLS()

            // create constructor to get widget reference
            var userName = itemView.findViewById(R.id.username) as TextView
            var bankName = itemView.findViewById(R.id.bankname) as TextView

            var accountNo = itemView.findViewById(R.id.accountno) as TextView
            var branch = itemView.findViewById(R.id.branch) as TextView
            var ifscCode = itemView.findViewById(R.id.ifsccode) as TextView

            // Get current position of item in recyclerview to bind data and assign values from list
            userName.text = bankDataItems.username
            bankName.text = bankDataItems.bankName
            accountNo.text = bankDataItems.account_no
            branch.text = bankDataItems.description
            ifscCode.text = bankDataItems.ifsc_code
    
        }
    }
}