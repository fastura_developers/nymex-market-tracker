package com.example.manikandan.nymexandroid

import android.content.Context
import android.content.res.Resources
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.coordinatorlayout.widget.CoordinatorLayout
import com.google.android.material.snackbar.Snackbar

class ObjectsURLS {
    var type: String? = null
    var at_price: String? = null
    var contract_month: String? = null
    var additional_commodity_string: String? = null
    var price_type: String? = null
    var subscribe_plan: String? = null
    var target1: String? = null
    var target1_achieved: String? = null
    var error = "Server busy please try again later!..."
    var none = "Some problem occured!..."
    var target1message = "Target 1 Achieved. Book Profit"
    var target2message = "Target 2 Achieved. Book Profit"
    var target3message = "Target 3 Achieved. Book Profit"
    var stoplosstext = "Stop Loss Hit"
    var exittext = "Exit at"
    var target2: String? = null
    var bankdetails = "http://karthikfastura.com/nymex/API/Support/bankdetails"
    var target2_achieved: String? = null
    var target3: String? = null
    var target3_achieved: String? = null
    var stop_loss: String? = null
    var stop_loss_met: String? = null
    var exit_price: String? = null
    var exit_call: String? = null
    var follow_up: String? = null
    var profit_loss: String? = null
    var expiredcote = "Account has expired on"
    var ensure = "Please ensure your account status"
    var last_sms: String? = null
    var description: String? = null
    var offer = "http://karthikfastura.com/nymex/API/Plan/offer"
    var aboutus = "http://karthikfastura.com/nymex/API/Support/about"
    var levels = "http://karthikfastura.com/nymex/API/Levels/levels"
    var calls = "http://karthikfastura.com/nymex/API/Calls/calls"
    var overallcalls = "http://karthikfastura.com/nymex/API/Calls/overallcalls"
    var plans = "http://karthikfastura.com/nymex/API/Plan/plan"
    var updateaccount = "http://karthikfastura.com/nymex/API/User/updateAccount"
    var paymentUpdate = "http://karthikfastura.com/nymex/API/Payment/success"
    var accountrecovery = "http://karthikfastura.com/nymex/API/User/accountRecovery"
    var alreadyRegistered  = "http://karthikfastura.com/nymex/API/User/alreadyRegistered"
    var register  = "http://karthikfastura.com/nymex/API/User/register"
    var holidays = "http://karthikfastura.com/nymex/API/Support/holiday"
    var notification = "http://karthikfastura.com/nymex/API/Calls/notification"
    var url: String? = null
    var mcxnews = "http://karthikfastura.com/nymex/API/Support/newsURL"
    var commodities: String? = null
    var crudenewsvideos = "https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&maxResults=50&playlistId=PLOW-SYMVXDRsMaBw4OLgbNH1EA818q5zB&key=AIzaSyChol_OGmMgEOJAhQbM-sEtdaruIa_9zJ4"
    var commoditynews = "https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&maxResults=50&playlistId=PLOW-SYMVXDRsncLjXVw55-GIMGx6yRqbr&key=AIzaSyChol_OGmMgEOJAhQbM-sEtdaruIa_9zJ4"
    var title: String? = null
    var performance = "http://fastura.net/nymex/nymex/mcxperformance.html"
    var accountmcx = "http://karthikfastura.com/nymex/API/User/oldUser"
    var support = "http://karthikfastura.com/nymex/API/Support/support"
    var imagelink: String? = null
    var videolink: String? = null
    var published_date: String? = null
    var t1: String? = null
    var t2: String? = null
    var t3: String? = null
    var s1: String? = null
    var s2: String? = null
    var s3: String? = null
    var cname: String? = null
    var tv: String? = null
    var updated_on: String? = null
    var plan: String? = null
    var days: String? = null
    var amount: String? = null
    var planname: String? = null
    var currencytype: String? = null
    var dollarrate = "1"
    val clientservice="NYMEX MARKET TRACKER"
    val authkey="REST-API"
    val servermaintenance="Fastura Technologies"
    val clientserviceObject="Client-Service"
    val authkeyObject="Auth-Key"
    val servermaintenanceObject="Server-Maintenance"
    val post="POST"
    val get="GET"
    val code="code"
    val success = "200"
    val message = "message"
    val fail="201"
    val result="result"
    val createUserTableIfNotExist="CREATE TABLE IF NOT EXISTS NYMEX(id INTEGER primary key AUTOINCREMENT,name text,mobileno text,email text,expired_date text);"
//    val createUserTableIfNotExist="CREATE TABLE IF NOT EXISTS NYMEX(name text,mobileno BIGINT primary key,email text,expired_date text);"
    val createOfferTableIfNotExist="CREATE TABLE IF NOT EXISTS offer(id Integer primary key AUTOINCREMENT,message TEXT,lastSeen TEXT);"
    val selectUser="SELECT name,email,mobileno,expired_date FROM NYMEX"
    val selectOffer="SELECT message,lastSeen FROM offer"
    val accountExpiredOn= "Account has expired on "
    val expiredOn = "Expired on "
    val exit="exit"
    val yes="yes"
    val no="no"
    val target="target"
    val stoploss = "stop_loss"
    val updatedOn="Updated_on"
    val processEngaged="Application process is engaged!..."
    val deviceNotProcess="Device cannot being process"
    val emailEmpty="Email ID is empty"
    val oneSignalId="76addc60-997c-4b91-9ee1-d90fa591dede"
    val PAYPAL_CLIENT_ID = "AbuErP2qUEvTyiS2m7r_33ogVW6h32-bpvpP_8tXj7QYl1MFAUGXaNXyhDQw0c1r0-rp2_Kq4d5dpIyH"
    //test key = "AZRj8DcC_y0bz5igb7ESRKndTBEaFR4xXZRtEbZadeRXmznkeYtqjwoitUzWeYis6yIdg8fapF6QFT7_"
    val payPal = "PayPal"


    companion object{var firebaseID:String=""}

    fun snackbarMessage(coordinatorLayout: CoordinatorLayout,backgroundColor:Int,textColor:Int,message: String){
        val snackbar= Snackbar.make(coordinatorLayout, message, 3000)
        val view=snackbar.getView()
        view?.setBackgroundColor(backgroundColor.toString().toInt())//Resources.getSystem().getColor(R.color.colorAccent))
        val  message = view.findViewById(com.google.android.material.R.id.snackbar_text) as TextView
        message.setTextColor(textColor)
        snackbar.show()
    }

    fun toastMessage(context:Context, message: String){
        val toast= Toast.makeText(context, message, Toast.LENGTH_LONG)
        val view=toast.getView()
        view?.setBackgroundColor(Resources.getSystem().getColor(R.color.colorAccent))
        val message = view!!.findViewById<View>(android.R.id.message) as TextView
        message.setTextColor(Resources.getSystem().getColor(R.color.white))
        toast.show()
    }
}