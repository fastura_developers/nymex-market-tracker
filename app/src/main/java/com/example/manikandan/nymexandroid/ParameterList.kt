package com.example.manikandan.nymexandroid

data class SignalDataItems(var created_on:String, var at_price:String, var target1:String, var target2:String, var target3:String,
                           var target1_achieved:String, var target2_achieved:String, var target3_achieved:String,
                           var follow_up:String, var additional_commodity_string:String,
                           var contract_month:String, var exit_call:String, var price_type:String, var subscribe_plan:String,
                           var commodities:String, var profit_loss:String, var stop_loss:String,
                           var stop_loss_met:String, var type:String, var exit_price:String,
                           var last_sms:String, var url:String)

data class LevelsDataItems(var TV:String,var SN:String,var R1:String,var R2:String,var R3:String,var S1:String,var S2:String,var S3:String,var Updated_on:String)

data class PlanDataItems(var plan:String,var days:String,var amount:String,var planname:String,var dollarrate:String,var currencytype:String)

data class BankDataItems(var username:String,var bankName:String,var account_no:String,var ifsc_code:String,var description:String)

data class VideoDataItems(var publishedAt:String,var title:String,var videoId:String,var imageURL:String)

data class NewsDataItems(var title:String,var description:String,var published_date:String,var imageURL:String,var url:String)

data class HolidayDataItems(var Date:String,var Days:String,var Particulars:String,var Ms:String,var Es:String)