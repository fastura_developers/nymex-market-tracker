package com.example.manikandan.nymexandroid

import android.annotation.TargetApi
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.*
import android.widget.Toast
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar

/**
 * A simple [Fragment] subclass.
 */
class OnlinePageViewer constructor() : Fragment() {
    lateinit var intent: Intent
    lateinit var webView: WebView
    lateinit var onlinePageViewer: CoordinatorLayout
    var extras: Bundle? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                                     savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val v: View = inflater.inflate(R.layout.fragment_onlinepageviewer, container, false)
        webView = v.findViewById<View>(R.id.online) as WebView
        val webSettings: WebSettings = webView!!.getSettings()
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true)
        webView!!.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH)
        webView!!.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK)
        webView!!.getSettings().setAppCacheEnabled(true)
        webView!!.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH)
        webView!!.setWebChromeClient(WebChromeClient())
        webSettings.setDomStorageEnabled(true)
        webSettings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS)
        webSettings.setUseWideViewPort(true)
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true)
        webSettings.setJavaScriptEnabled(true)
        onlinePageViewer = v.findViewById<View>(R.id.onlinepageviewer) as CoordinatorLayout
        //     webView.setWebViewClient(new MyWebViewClient());
        loadurl()
        return v
    }

    private inner class MyWebViewClient constructor() : WebViewClient() {
        public override fun onPageStarted(view: WebView, url: String, favicon: Bitmap) {
            // TODO Auto-generated method stub
            Toast.makeText(getContext(), "fdf", Toast.LENGTH_LONG).show()
            super.onPageStarted(view, url, favicon)
        }

        public override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
            val uri: Uri = Uri.parse(url)
            return true
        }

        @TargetApi(Build.VERSION_CODES.N)
        public override fun shouldOverrideUrlLoading(view: WebView, request: WebResourceRequest): Boolean {
            view.loadUrl(request.getUrl().toString())
            return true
        }

        public override fun onReceivedError(view: WebView, request: WebResourceRequest, error: WebResourceError) {
            Toast.makeText(getContext(), error.toString(), Toast.LENGTH_LONG).show()
            super.onReceivedError(view, request, error)
        }
    }

    fun loadurl() {
        val url: String? = getArguments()!!.getString("url")
        if (url!!.length > 0) {
            webView!!.loadUrl(url)
            webView!!.getSettings().setBuiltInZoomControls(true)
            webView!!.getSettings().setDisplayZoomControls(true)
            Snackbar.make((onlinePageViewer)!!, "Loading...", Snackbar.LENGTH_LONG).show()
            webView!!.setWebViewClient(object : WebViewClient() {
                override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                    // do your handling codes here, which url is the requested url
                    // probably you need to open that url rather than redirect:
                    view.loadUrl(url)
                    return false // then it is not handled by default action
                }
            })
        } else {
            Snackbar.make((onlinePageViewer)!!, "Please try again!...", Snackbar.LENGTH_INDEFINITE).show()
        }
    }

    companion object {
        fun getMimeType(url: String?): String? {
            var type: String? = null
            val extension: String? = MimeTypeMap.getFileExtensionFromUrl(url)
            if (extension != null) {
                type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension)
            }
            return type
        }
    }
}