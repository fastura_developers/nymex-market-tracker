package com.example.manikandan.nymexandroid

import android.app.AlertDialog
import android.app.ProgressDialog
import android.graphics.Bitmap
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import org.json.JSONException
import org.json.JSONObject
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class Notifications constructor() : Fragment() {
    lateinit var variables: ObjectsURLS
    lateinit var recyclerView: RecyclerView
    lateinit var coordinatorLayout: CoordinatorLayout
    lateinit var notify: NotificationList
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                                     savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val v: View = inflater.inflate(R.layout.fragment_notifications, container, false)
        recyclerView = v.findViewById(R.id.notifications)
        coordinatorLayout = v.findViewById(R.id.notification)
        variables = ObjectsURLS()
        NotificationListView().execute(variables!!.notification,"", variables!!.post)
        return v
    }

    inner class NotificationListView : com.example.manikandan.nymexandroid.AsyncTask.AsyncTaskClass() {
        var progressDialog: ProgressDialog? = null
        var alertDialog: AlertDialog? = null
        var bitmap: Bitmap? = null
        override fun onPreExecute() {
            progressDialog = ProgressDialog.show(context, "", "Please wait...", true)
            alertDialog = AlertDialog.Builder(context).create()
        }

        /*override fun doInBackground(vararg params: String?): String? {
            val loginUrl= params[0]
            try {
                val url: URL = URL(loginUrl)
                val httpURLConnection = url.openConnection() as HttpURLConnection
                httpURLConnection.requestMethod = "POST"
                httpURLConnection.doOutput = true
                httpURLConnection.doInput = true
                val inputStream: InputStream = httpURLConnection.inputStream
                val bufferedReader = BufferedReader(InputStreamReader(inputStream, "iso-8859-1"))
                var result = ""
                var line = ""
                while ((bufferedReader.readLine().also { line = it }) != null) {
                    result = result + line + "\n"
                }
                bufferedReader.close()
                inputStream.close()
                httpURLConnection.disconnect()
                return result
            } catch (e: MalformedURLException) {
                e.printStackTrace()
            } catch (e: IOException) {
                e.printStackTrace()
            }
            //}
            return null
        }*/

        override fun onPostExecute(result: String?) {
            progressDialog!!.dismiss()
            alertDialog!!.dismiss()
            //check if exist or not
            try {
                var i: Int
                val jsonObject=JSONObject(result)
                if(jsonObject.has(objects.code)) {
                    if (jsonObject.getString(objects.code).equals(objects.success)) {
                        Log.e("Notification",result.toString())

                        if (jsonObject.has(variables!!.message)) {
                            objects.snackbarMessage(coordinatorLayout,resources.getColor(R.color.colorAccent),resources.getColor(R.color.white),jsonObject.getString("message"))
                        }
                        val data: MutableList<ObjectsURLS> = ArrayList()
                        i = 0
                        val jsonArray = jsonObject.getJSONArray(variables!!.result)
                        while (i < jsonArray.length()) {
                            val obj: JSONObject = jsonArray.getJSONObject(i)
                            variables = ObjectsURLS()
                            variables!!.published_date = obj.getString("created_on")
                            variables!!.description = obj.getString("content")
                            data.add(variables!!)
                            i++
                        }
                        notify = NotificationList(super@Notifications.getContext(), data)
                        recyclerView!!.setAdapter(notify)
                        recyclerView!!.setLayoutManager(LinearLayoutManager(super@Notifications.getContext()))
                    }else if (jsonObject.getString(objects.code).equals(objects.fail)) {
                        if (result != null) {
                            Log.e("Fail",result)
                        }

                        if (jsonObject.has(variables!!.message)) {
                            objects.snackbarMessage(coordinatorLayout,resources.getColor(R.color.colorAccent),resources.getColor(R.color.white),jsonObject.getString("message"))
                        }
                    }
                }
            } catch (e: JSONException) {
            } catch (e: NullPointerException) {
                variables = ObjectsURLS()
                Snackbar.make((coordinatorLayout)!!, variables!!.error, Snackbar.LENGTH_INDEFINITE).show()
            }
            progressDialog!!.dismiss()
            alertDialog!!.dismiss()
        }
    }
}