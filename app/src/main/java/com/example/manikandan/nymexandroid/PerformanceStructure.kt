package com.example.manikandan.nymexandroid

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import java.util.*

class PerformanceStructure (val signalDataItems: List<SignalDataItems>) : RecyclerView.Adapter<PerformanceStructure.MyHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.activity_performancestructure, parent, false)
        return MyHolder(view)
    }

    override fun onBindViewHolder(holder: MyHolder, position: Int) {
        holder.bindItems(signalDataItems[position])
    }

    // return total item from List
    public override fun getItemCount(): Int {
        return signalDataItems.size
    }

    class MyHolder constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(signalDataItems: SignalDataItems) {
            val objects = ObjectsURLS()
            var date = itemView.findViewById<View>(R.id.date) as TextView
            var commodity = itemView.findViewById<View>(R.id.commodity) as TextView
            var status = itemView.findViewById<View>(R.id.status) as TextView
            val firstSubString: String

            if (signalDataItems.last_sms!!.length > 0 && (signalDataItems.last_sms!!.contains("AM") || signalDataItems.last_sms!!.contains("PM"))) {
                val split: Array<String> = signalDataItems.last_sms!!.split(" ".toRegex()).toTypedArray()
                firstSubString = split.get(0)
            } else {
                firstSubString = "..."
            }
            date.text = signalDataItems.created_on + "\n" + firstSubString
            commodity.text = signalDataItems.commodities
            status.text = signalDataItems.profit_loss
            val target = "target"
            val yes = "yes"
            val exit = "exit"
            val stop_loss = "stop_loss"
            if (signalDataItems.follow_up.equals(objects.target)!!) {
                val targettext: String = target.substring(0, 1).toUpperCase(Locale.ROOT) + target.substring(1).toLowerCase() + " " + "at"
                if (signalDataItems.target1_achieved.equals(objects.yes)!!) {
                    status.setText(targettext + " " + signalDataItems.target1)
                    date.setTextColor(itemView.context!!.getResources().getColor(R.color.lightgreen))
                    status.setTextColor(itemView.context!!.getResources().getColor(R.color.lightgreen))
                    commodity.setTextColor(itemView.context!!.getResources().getColor(R.color.lightgreen))
                } else if (signalDataItems.target2_achieved.equals(objects.yes)) {
                    status.setText(targettext + " " + signalDataItems.target2)
                    date.setTextColor(itemView.context!!.resources.getColor(R.color.lightgreen))
                    status.setTextColor(itemView.context!!.resources.getColor(R.color.lightgreen))
                    commodity.setTextColor(itemView.context!!.resources.getColor(R.color.lightgreen))
                } else if (signalDataItems.target3_achieved.equals(objects.yes)) {
                    status.setText(targettext + " " + signalDataItems.target3)
                    date.setTextColor(itemView.context!!.resources.getColor(R.color.lightgreen))
                    status.setTextColor(itemView.context!!.resources.getColor(R.color.lightgreen))
                    commodity.setTextColor(itemView.context!!.resources.getColor(R.color.lightgreen))
                } else {
                    date.setTextColor(itemView.context!!.resources.getColor(R.color.calls))
                    status.setTextColor(itemView.context!!.resources.getColor(R.color.calls))
                    commodity.setTextColor(itemView.context!!.resources.getColor(R.color.calls))
                }
            }
            if (signalDataItems.follow_up.equals(objects.exit)) {
                if (signalDataItems.exit_call.equals(objects.yes)) {
                    status.setText(objects.exittext + " " + signalDataItems.exit_price)
                }
                date.setTextColor(itemView.context!!.resources.getColor(R.color.gold1))
                status.setTextColor(itemView.context!!.resources.getColor(R.color.gold1))
                commodity.setTextColor(itemView.context!!.resources.getColor(R.color.gold1))
            }
            if (signalDataItems.follow_up.equals(signalDataItems.stop_loss)) {
                if (signalDataItems.profit_loss!!.contains("-")) {
                } else {
                    var PL: Double = signalDataItems.profit_loss!!.toDouble()
                    PL = PL * (-1)
                    status.setText(PL.toString().toString())
                }
                if (signalDataItems.stop_loss_met.equals(objects.yes)) {
                    status.setText(objects.stoplosstext)
                }
                date.setTextColor(itemView.context!!.resources.getColor(R.color.stoploss))
                status.setTextColor(itemView.context!!.resources.getColor(R.color.stoploss))
                commodity.setTextColor(itemView.context!!.resources.getColor(R.color.stoploss))
            }
        }
    }
}

    /*@SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        // Get callsDataItems position of item in recyclerview to bind data and assign values from list
        val  MyHolder = holder as MyHolder
        val callsDataItems: feeditems = data.get(position)
        val firstSubString: String
        if (callsDataItems.last_sms!!.length > 0 && (callsDataItems.last_sms!!.contains("AM") || callsDataItems.last_sms!!.contains("PM"))) {
            val split: Array<String> = callsDataItems.last_sms!!.split(" ".toRegex()).toTypedArray()
            firstSubString = split.get(0)
        } else {
            firstSubString = "..."
        }
        date.text = callsDataItems.published_date + "\n" + firstSubString
        commodity.text = callsDataItems.commodities
        status.text = callsDataItems.profit_loss
        val target = "target"
        val yes = "yes"
        val exit = "exit"
        val stop_loss = "stop_loss"
        if (callsDataItems.follow_up.equals(callsDataItems.target)!!) {
            val targettext: String = target.substring(0, 1).toUpperCase(Locale.ROOT) + target.substring(1).toLowerCase() + " " + "at"
            if (callsDataItems.target1_achieved.equals(callsDataItems.yes)!!) {
                status.setText(targettext + " " + callsDataItems.target1)
                date.setTextColor(itemView.context!!.getResources().getColor(R.color.target))
                status.setTextColor(itemView.context!!.getResources().getColor(R.color.target))
                commodity.setTextColor(itemView.context!!.getResources().getColor(R.color.target))
            } else if (callsDataItems.target2_achieved.equals(callsDataItems.yes)) {
                status.setText(targettext + " " + callsDataItems.target2)
                date.setTextColor(itemView.context!!.resources.getColor(R.color.target))
                status.setTextColor(itemView.context!!.resources.getColor(R.color.target))
                commodity.setTextColor(itemView.context!!.resources.getColor(R.color.target))
            } else if (callsDataItems.target3_achieved.equals(callsDataItems.yes)) {
                status.setText(targettext + " " + callsDataItems.target3)
                date.setTextColor(itemView.context!!.resources.getColor(R.color.target))
                status.setTextColor(itemView.context!!.resources.getColor(R.color.target))
                commodity.setTextColor(itemView.context!!.resources.getColor(R.color.target))
            } else {
                date.setTextColor(itemView.context!!.resources.getColor(R.color.calls))
                status.setTextColor(itemView.context!!.resources.getColor(R.color.calls))
                commodity.setTextColor(itemView.context!!.resources.getColor(R.color.calls))
            }
        }
        if (callsDataItems.follow_up.equals(callsDataItems.exit)) {
            if (callsDataItems.exit_call.equals(callsDataItems.yes)) {
                status.setText(callsDataItems.exittext + " " + callsDataItems.exit_price)
            }
            date.setTextColor(itemView.context!!.resources.getColor(R.color.gold1))
            status.setTextColor(itemView.context!!.resources.getColor(R.color.gold1))
            commodity.setTextColor(itemView.context!!.resources.getColor(R.color.gold1))
        }
        if (callsDataItems.follow_up.equals(callsDataItems.stop_loss)) {
            if (callsDataItems.profit_loss!!.contains("-")) {
            } else {
                var PL: Double = callsDataItems.profit_loss!!.toDouble()
                PL = PL * (-1)
                status.setText(PL.toString().toString())
            }
            if (callsDataItems.stop_loss_met.equals(callsDataItems.yes)) {
                status.setText(callsDataItems.stoplosstext)
            }
            date.setTextColor(itemView.context!!.resources.getColor(R.color.stoploss))
            status.setTextColor(itemView.context!!.resources.getColor(R.color.stoploss))
            commodity.setTextColor(itemView.context!!.resources.getColor(R.color.stoploss))
        }
    }

    // return total item from List
    override fun getItemCount(): Int {
        return data.size
    }

    internal inner class MyHolder constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var date: TextView
        var commodity: TextView
        var status: TextView

        // create constructor to get widget reference
        init {
            date = itemView.findViewById<View>(R.id.date) as TextView
            commodity = itemView.findViewById<View>(R.id.commodity) as TextView
            status = itemView.findViewById<View>(R.id.status) as TextView
        }
    }

    init {
        inflater = LayoutInflater.from(itemView.context)
        this.data = data
    }
}*/