package com.example.manikandan.nymexandroid

import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class HolidayList(var holidayDataItems: List<HolidayDataItems>) : RecyclerView.Adapter<HolidayList.MyHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.activity_holidaylist, parent, false)
        return MyHolder(view)
    }

    override fun onBindViewHolder(holder: MyHolder, position: Int) {
        holder.bindItems(holidayDataItems[position])
    }

    // return total item from List
    public override fun getItemCount(): Int {
        return holidayDataItems.size
    }

    class MyHolder constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(holidayDataItems: HolidayDataItems) {
            val objects = ObjectsURLS()
            var date = itemView.findViewById<View>(R.id.date) as TextView
            var days: TextView? = null
            var particulars = itemView.findViewById<View>(R.id.particulars) as TextView
            var es = itemView.findViewById<View>(R.id.es) as TextView
            var ms = itemView.findViewById<View>(R.id.ms) as TextView
            date.text = """
            ${holidayDataItems.Date}
            ${holidayDataItems.Days}
            """.trimIndent()
            particulars.text = holidayDataItems.Particulars
            ms.text = Html.fromHtml(holidayDataItems.Ms)
            es.text = Html.fromHtml(holidayDataItems.Es)
        }

    }
}