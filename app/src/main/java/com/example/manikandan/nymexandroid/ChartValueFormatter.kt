package com.example.manikandan.nymexandroid

import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.formatter.IValueFormatter
import com.github.mikephil.charting.formatter.ValueFormatter
import com.github.mikephil.charting.utils.ViewPortHandler
import java.text.DecimalFormat

class ChartValueFormatter constructor() : ValueFormatter(), IValueFormatter {
    private val mFormat: DecimalFormat
    public override fun getFormattedValue(value: Float, entry: Entry, dataSetIndex: Int, viewPortHandler: ViewPortHandler): String {
        // write your logic here
        return mFormat.format(value.toDouble()) + " $" // e.g. append a dollar-sign
    }

    init {
        mFormat = DecimalFormat("###,###,##0.0") // use one decimal
    }
}