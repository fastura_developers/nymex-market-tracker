package com.example.manikandan.nymexandroid

import android.app.AlertDialog
import android.app.ProgressDialog
import android.graphics.Bitmap
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.google.android.material.snackbar.Snackbar
import org.json.JSONException
import org.json.JSONObject
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class NYMEXNews : Fragment() {
    lateinit var coordinatorLayout: CoordinatorLayout
    lateinit var newsList: NewsList
    lateinit var news: RecyclerView
    var variables= ObjectsURLS()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val v = inflater.inflate(R.layout.fragment_mcxnews, container, false)
        news = v.findViewById(R.id.mcxnews)
        news.layoutManager=LinearLayoutManager(context,RecyclerView.VERTICAL,false)
        coordinatorLayout = v.findViewById<View>(R.id.mcxnewslist) as CoordinatorLayout
        variables = ObjectsURLS()
        GetNEWSUrl().execute(variables!!.mcxnews,"", variables!!.post)
        return v
    }

    inner class GetNEWSUrl:com.example.manikandan.nymexandroid.AsyncTask.AsyncTaskClass() {
        var progressDialog: ProgressDialog? = null
        var alertDialog: AlertDialog? = null
        var bitmap: Bitmap? = null
        override fun onPreExecute() {
            progressDialog = ProgressDialog.show(context, "", "Please wait...", true)
            alertDialog = AlertDialog.Builder(context).create()
        }

        override fun onPostExecute(result: String?) {
            progressDialog!!.dismiss()
            alertDialog!!.dismiss()

            //check if exist or not
            // Toast.makeText(getContext(),result,Toast.LENGTH_SHORT).show();
            try {
                var i: Int
                val jsonObject=JSONObject(result)
                if(jsonObject.has(objects.code)) {
                    if(jsonObject.getString(objects.code).equals(objects.success)) {
                        val jsonArray = jsonObject.getJSONArray(objects.result)
                        i = 0
                        val obj = jsonArray.getJSONObject(i)
                        if(obj.has("newsurl")) {
                            if(obj.getString("newsurl")!="") {
                                mcxtrackernews(obj.getString("newsurl"))
                            }else{
                                objects.snackbarMessage(coordinatorLayout, resources.getColor(R.color.colorAccent), resources.getColor(R.color.white), getString(R.string.datasDidNotReceived))
                            }
                        }else{
                            objects.snackbarMessage(coordinatorLayout, resources.getColor(R.color.colorAccent), resources.getColor(R.color.white), getString(R.string.resourceEmpty))
                        }
/*                            variables!!.published_date =
                            variables!!.at_price =
                            variables!!.target1 =
                            variables!!.target2 =
                            variables!!.target3 =
                            variables!!.target1_achieved =
                            variables!!.target2_achieved =
                            variables!!.target3_achieved =
                            variables!!.follow_up =
                            variables!!.additional_commodity_string =
                            variables!!.contract_month =
                            variables!!.exit_call =
                            variables!!.price_type =
                            variables!!.subscribe_plan =
                            variables!!.commodities =
                            variables!!.profit_loss =
                            variables!!.stop_loss =
                            variables!!.stop_loss_met =
                            variables!!.type =
                            variables!!.exit_price =
                            variables!!.last_sms =*/
                    }else if (jsonObject.getString(objects.code).equals(objects.fail)) {
                        if (jsonObject.has(variables?.message)) {
                            objects.snackbarMessage(coordinatorLayout, resources.getColor(R.color.colorAccent), resources.getColor(R.color.white), jsonObject.getString("message"))
                        }
                    }
                }
            } catch (e: JSONException) {
            } catch (e: NullPointerException) {
                variables = ObjectsURLS()
                Snackbar.make((coordinatorLayout)!!, variables!!.error, Snackbar.LENGTH_INDEFINITE).show()
            }
            progressDialog!!.dismiss()
            alertDialog!!.dismiss()
        }

    }

    fun mcxtrackernews(url: String?) {
        val requestQueue = Volley.newRequestQueue(context)
        val jsonObjectRequest = JsonObjectRequest(Request.Method.GET, url, null, object : Response.Listener<JSONObject> {
            var progressDialog = ProgressDialog.show(context, "", "Please wait...", true)
            var alertDialog = AlertDialog.Builder(context).create()
            override fun onResponse(response: JSONObject) {
                try {
                    var i: Int
                    var totalvideos: Int
                    val newsList:ArrayList<NewsDataItems> = ArrayList()
                    val totalNews=NewsList(newsList)
                    val jsonArray = response.getJSONArray("items")
                    i = 0
                    while (i < jsonArray.length()) {
                        val variables = ObjectsURLS()
                        val obj = jsonArray.getJSONObject(i)
                        newsList.add(NewsDataItems(obj.getString("title"),obj.getString("description"),obj.getString("pubDate"),obj.getString("thumbnail"),obj.getString("link")))
                        i++
                    }
                    news!!.adapter = totalNews
                    progressDialog.dismiss()
                    alertDialog.dismiss()
                } catch (e: JSONException) {
                } catch (e: NullPointerException) {
                    variables = ObjectsURLS()
                    Snackbar.make(coordinatorLayout!!, variables!!.error, Snackbar.LENGTH_INDEFINITE).show()
                }
                //  findViewById(R.id.progressBar).setVisibility(View.GONE);
            }
        }, Response.ErrorListener {
            //   txtShowTextResult.setText("An Error occured while making the request");
        })
        requestQueue.add(jsonObjectRequest)
    }
}