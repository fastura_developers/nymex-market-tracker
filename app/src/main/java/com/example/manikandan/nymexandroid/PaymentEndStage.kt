package com.example.manikandan.nymexandroid

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Intent
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.graphics.Bitmap
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.coordinatorlayout.widget.CoordinatorLayout
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_monthlycalls.*
import org.json.JSONException
import org.json.JSONObject
import java.io.*
import java.net.URLEncoder
import java.text.SimpleDateFormat
import java.util.*

class PaymentEndStage : AppCompatActivity() {
    lateinit var bundle: Bundle
    lateinit var objects: ObjectsURLS
    lateinit var simpleDateFormat2: SimpleDateFormat
    lateinit var dateString: String
    lateinit var expiryDate: String
    lateinit var db: SQLiteDatabase
    lateinit var date: Date
    private lateinit var toast: Toast
    private var lastBackPressTime: Long = 0
    lateinit var cursor: Cursor
    lateinit var coordinatorLayout: CoordinatorLayout
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_paymentendstage)
        coordinatorLayout = findViewById<View>(R.id.paymentend) as CoordinatorLayout
        allowAccount()
    }

    fun allowAccount() {
        db = openOrCreateDatabase("NYMEXDB", MODE_PRIVATE, null)
        cursor = db.rawQuery("SELECT expired_date FROM NYMEX", null)
        cursor.moveToFirst()
        if (cursor.getCount() > 0) {
            //Toast.makeText(getApplicationContext(),cursor.getString(0),Toast.LENGTH_LONG).show();
            try {
                date = Calendar.getInstance().getTime()
                simpleDateFormat2 = SimpleDateFormat("yyyy-MM-dd")
                dateString = simpleDateFormat2!!.format(date)
                //Toast.makeText(getApplicationContext(),dateString,Toast.LENGTH_LONG).show();
                val dateBefore: Date = simpleDateFormat2!!.parse(dateString)
                val accountdate: Date = simpleDateFormat2!!.parse(cursor.getString(0))
                if (dateBefore.after(accountdate)) {
                    //current date
                    expiryDate = dateString
                } else {
                    //account date
                    expiryDate = cursor.getString(0)
                }
            } catch (e: NullPointerException) {
                objects = ObjectsURLS()
                Snackbar.make((coordinatorLayout), objects.none, Snackbar.LENGTH_INDEFINITE).show()
            } catch (e: Exception) {
            }
            cursor.close()
        }
        intent = intent
        bundle = Bundle()
        bundle = if (intent.extras != null) intent.extras!! else throw NullPointerException("Expression 'intent.extras' must not be null")
        if (bundle != null && expiryDate.length > 0) {
            objects = ObjectsURLS()


            Account().execute(objects.paymentUpdate,(URLEncoder.encode("deviceID", "UTF-8") + "=" + URLEncoder.encode(bundle.getString("deviceID"), "UTF-8") + "&" +
                    URLEncoder.encode("emailID", "UTF-8") + "=" + URLEncoder.encode(bundle.getString("email"), "UTF-8") + "&" +
                    URLEncoder.encode("mobileno", "UTF-8") + "=" + URLEncoder.encode(bundle.getString("phone"), "UTF-8") + "&" +
                    URLEncoder.encode("plan_name", "UTF-8") + "=" + URLEncoder.encode(bundle.getString("plan_name"), "UTF-8") + "&" +
                    URLEncoder.encode("amount", "UTF-8") + "=" + URLEncoder.encode(bundle.getString("payment"), "UTF-8") + "&" +
                    URLEncoder.encode("days", "UTF-8") + "=" + URLEncoder.encode(bundle.getString("days"), "UTF-8") + "&" +
                    URLEncoder.encode("status", "UTF-8") + "=" + URLEncoder.encode(bundle.getString("status"), "UTF-8") + "&" +
                    URLEncoder.encode("payment_state", "UTF-8") + "=" + URLEncoder.encode(bundle.getString("payment_state"), "UTF-8") + "&" +
                    URLEncoder.encode("date", "UTF-8") + "=" + URLEncoder.encode(expiryDate, "UTF-8") + "&" +
                    URLEncoder.encode("transaction_id", "UTF-8") + "=" + URLEncoder.encode(bundle.getString("transaction_id"), "UTF-8")),objects.post)
        } else {
            Snackbar.make((coordinatorLayout), objects.none, Snackbar.LENGTH_INDEFINITE).show()
            val intent = Intent(this, Mainframe::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
            intent.putExtra("payment_result", objects.ensure)
        }
    }

    @SuppressLint("StaticFieldLeak")
    inner class Account: com.example.manikandan.nymexandroid.AsyncTask.AsyncTaskClass(){
        var alertDialog: AlertDialog? = null
        var bitmap: Bitmap? = null
        private var result: String = ""

        override fun onPreExecute() {
            //    progressDialog = ProgressDialog.show(context, "", "Please wait...", true);
            alertDialog = AlertDialog.Builder(applicationContext).create()
        }

        override fun onPostExecute(result: String?) {
            alertDialog!!.dismiss()
            //check if exist or not
            Log.e("Payment end result",result.toString())
            var message:String=""
            try {
                val jsonObject = JSONObject(result)
                if (jsonObject.has(objects.code)) {
                    if (jsonObject.getString(objects.code).equals(objects.success)) {
                        objects.snackbarMessage(
                            coordinatorLayout,
                            resources.getColor(R.color.colorPrimaryDark),
                            resources.getColor(R.color.white),
                            jsonObject.getString("message")
                        )
                        message = jsonObject.getString(objects.message)
                    } else if (jsonObject.getString(objects.code).equals(objects.fail)) {
                        if (jsonObject.has(objects.message)) {
                            objects.snackbarMessage(
                                coordinatorLayout,
                                resources.getColor(R.color.colorAccent),
                                resources.getColor(R.color.white),
                                jsonObject.getString("message")
                            )
                            message = jsonObject.getString(objects.message)
                        }
                    }
                }
            }catch (e: JSONException) {
            } catch (e: NullPointerException) {
                Snackbar.make((coordinatorLayout)!!, objects.error, Snackbar.LENGTH_INDEFINITE).show()
            }
            val intent = Intent(this@PaymentEndStage, Mainframe::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
            intent.putExtra("payment_result", message)
            startActivity(intent)
        }
    }

    override fun onBackPressed() {
        /*  AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle(R.string.app_name);
        builder.setIcon(R.mipmap.ic_launcher);
        builder.setMessage("Do you want to exit?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();

        new AlertDialog.Builder(this)
                .setMessage("Are you sure you want to exit?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        PaymentEndStage.super.onBackPressed();
                    }
                })
                .setNegativeButton("No", null)
                .show();
        */

        /*override fun doInBackground(vararg params: String?): String? {
            val loginUrl= params[0]
            val emailID=params[2]
            val mobileNo=params[1]
            val planName=params[4]
            val amount=params[3]
            val days=params[8]
            val deviceID=params[5]
            val status=params[7]
            val transactionId=params[6]
            val date=params[9]
            try {
                val url = URL(loginUrl)
                val httpURLConnection: HttpURLConnection = url.openConnection() as HttpURLConnection
                httpURLConnection.requestMethod = "POST"
                httpURLConnection.doOutput = true
                httpURLConnection.doInput = true
                val outputStream: OutputStream = httpURLConnection.outputStream
                val bufferedWriter = BufferedWriter(OutputStreamWriter(outputStream, "UTF-8"))
                val postdata: String = (URLEncoder.encode("deviceID", "UTF-8") + "=" + URLEncoder.encode(deviceID, "UTF-8") + "&" +
                        URLEncoder.encode("emailID", "UTF-8") + "=" + URLEncoder.encode(emailID, "UTF-8") + "&" +
                        URLEncoder.encode("mobileno", "UTF-8") + "=" + URLEncoder.encode(mobileNo, "UTF-8") + "&" +
                        URLEncoder.encode("plan_name", "UTF-8") + "=" + URLEncoder.encode(planName, "UTF-8") + "&" +
                        URLEncoder.encode("amount", "UTF-8") + "=" + URLEncoder.encode(amount, "UTF-8") + "&" +
                        URLEncoder.encode("days", "UTF-8") + "=" + URLEncoder.encode(days, "UTF-8") + "&" +
                        URLEncoder.encode("status", "UTF-8") + "=" + URLEncoder.encode(status, "UTF-8") + "&" +
                        URLEncoder.encode("date", "UTF-8") + "=" + URLEncoder.encode(date, "UTF-8") + "&" +
                        URLEncoder.encode("transaction_id", "UTF-8") + "=" + URLEncoder.encode(transactionId, "UTF-8"))
                bufferedWriter.write(postdata)
                bufferedWriter.flush()
                bufferedWriter.close()
                outputStream.close()
                val responseCode: Int = httpURLConnection.responseCode
                if (responseCode == 200) {
                    result = StreamReader().streamToString(httpURLConnection.inputStream)
                }

                return result

            }catch (ex: SSLHandshakeException){
                Log.e("SSLHandshakeException",ex.toString());
            }
            catch (ex: GooglePlayServicesRepairableException) {
                Log.e("GPRepairableException",ex.toString());
            } catch (ex: GooglePlayServicesNotAvailableException) {
                Log.e("SecurityException", "Google Play Services not available.");
            }
            return  result
        }*/
        if (lastBackPressTime < System.currentTimeMillis() - 4000) {
            toast = Toast.makeText(this, R.string.paymentprocessinterruptmessage, Toast.LENGTH_LONG)
            toast.show()
            lastBackPressTime = System.currentTimeMillis()
        } else {
            toast.cancel()
            super.onBackPressed()
        }
    }
}