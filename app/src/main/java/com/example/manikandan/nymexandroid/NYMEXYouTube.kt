package com.example.manikandan.nymexandroid

import android.app.AlertDialog
import android.app.ProgressDialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.tabs.TabItem
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayout.OnTabSelectedListener
import org.json.JSONException
import org.json.JSONObject
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class NYMEXYouTube : Fragment() {
    lateinit var variables: ObjectsURLS
    lateinit var videos: RecyclerView
    lateinit var coordinatorLayout: CoordinatorLayout
    lateinit var commodity: TabItem
    lateinit var crudeoil: TabItem
    lateinit var tabLayout: TabLayout
    lateinit var mRequestQueue: RequestQueue
    lateinit var mStringRequest: StringRequest
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val v = inflater.inflate(R.layout.fragment_mcxyoutube, container, false)
        videos = v.findViewById<View>(R.id.videolist) as RecyclerView
        videos.layoutManager=LinearLayoutManager(context,RecyclerView.VERTICAL,false)
        variables = ObjectsURLS()
        coordinatorLayout = v.findViewById<View>(R.id.youtube) as CoordinatorLayout
        //commodity = v.findViewById(R.id.commodity)
        //crudeoil = v.findViewById(R.id.crudeoil)
        tabLayout = v.findViewById(R.id.maintablayout) as TabLayout
        tabValidations()
        return v
    }

    fun tabValidations() {
        variables = ObjectsURLS()
        videoFromCommodity(variables!!.commoditynews)
        tabLayout!!.addOnTabSelectedListener(object : OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                if (tab.position == 0) {
                    //videoFromCommodity(variables!!.commoditynews)
                } else if (tab.position == 1) {
                    //videoFromCommodity(variables!!.crudenewsvideos)
                } else {
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {}
            override fun onTabReselected(tab: TabLayout.Tab) {}
        })
    }

    fun videoFromCommodity(url: String?) {
        val requestQueue = Volley.newRequestQueue(context)
        val jsonObjectRequest = JsonObjectRequest(Request.Method.GET, url, null, object : Response.Listener<JSONObject> {
            var progressDialog = ProgressDialog.show(context, "", "Please wait...", true)
            var alertDialog = AlertDialog.Builder(context).create()
            override fun onResponse(response: JSONObject) {
                try {
                    var i: Int
                    val totalvideos: Int
                    val videoDataItems:ArrayList<VideoDataItems> = ArrayList()
                    val videoList=YouTubeVideoList(videoDataItems)
                    val jsonArray = response.getJSONArray("items")
                    totalvideos = if (jsonArray.length() > 25) {
                        25
                    } else {
                        jsonArray.length()
                    }
                    i = 0
                    while (i < totalvideos) {
                        val obj = jsonArray.getJSONObject(i)
                        Log.e("Image",obj.getJSONObject("snippet").getJSONObject("thumbnails").getJSONObject("high").getString("url"))
                        val snippet = obj.getJSONObject("snippet")
                        val resourceId = snippet.getJSONObject("resourceId")
                        videoDataItems.add(VideoDataItems(snippet.getString("publishedAt"),snippet.getString("title"),
                            resourceId.getString("videoId"),obj.getJSONObject("snippet").getJSONObject("thumbnails").getJSONObject("high").getString("url")))
                        i++
                    }
                    videos!!.adapter = videoList
                    progressDialog.dismiss()
                    alertDialog.dismiss()
                } catch (e: JSONException) {
                } catch (e: NullPointerException) {
                    variables = ObjectsURLS()
                    Snackbar.make(coordinatorLayout!!, variables!!.error, Snackbar.LENGTH_INDEFINITE).show()
                }
                //  findViewById(R.id.progressBar).setVisibility(View.GONE);
            }
        }, Response.ErrorListener {
            //   txtShowTextResult.setText("An Error occured while making the request");
        })
        requestQueue.add(jsonObjectRequest)
    }
}