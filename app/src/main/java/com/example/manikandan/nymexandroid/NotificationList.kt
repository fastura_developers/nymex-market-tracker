package com.example.manikandan.nymexandroid

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class NotificationList constructor(var context: Context?, data: List<ObjectsURLS>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    lateinit var intent: Intent
    private val inflater: LayoutInflater
    var data: List<ObjectsURLS> = emptyList()
    lateinit var current: ObjectsURLS
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view: View = inflater.inflate(R.layout.activity_notificationlist, parent, false)
        val holder: MyHolder = MyHolder(view)
        return holder
    }

    public override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        // Get current position of item in recyclerview to bind data and assign values from list
        val myHolder: MyHolder = holder as MyHolder
        val current: ObjectsURLS = data.get(position)
        myHolder.createdOn.text = current.published_date
        myHolder.content.text = current.description
    }

    // return total item from List
    public override fun getItemCount(): Int {
        return data.size
    }

    internal inner class MyHolder constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var createdOn: TextView
        var content: TextView

        // create constructor to get widget reference
        init {
            createdOn = itemView.findViewById<View>(R.id.n_date) as TextView
            content = itemView.findViewById<View>(R.id.content) as TextView
        }
    }

    init {
        inflater = LayoutInflater.from(context)
        this.data = data
    }
}