@file:Suppress("DEPRECATION")

package com.example.manikandan.nymexandroid

import android.Manifest
import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.IntentSender.SendIntentException
import android.content.pm.PackageManager
import android.database.Cursor
import android.database.SQLException
import android.database.sqlite.SQLiteDatabase
import android.graphics.Color
import android.location.*
import android.location.LocationListener
import android.os.Build
import android.os.Build.VERSION
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.PendingResult
import com.google.android.gms.common.api.Status
import com.google.android.gms.location.*
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.iid.FirebaseInstanceId
import org.json.JSONException
import org.json.JSONObject
import java.io.*
import java.net.URLEncoder
import java.util.*

class MainActivity : AppCompatActivity(), LocationListener {
    private lateinit var mGoogleApiClient: GoogleApiClient
    lateinit var name: EditText
    private lateinit var mobileNo: EditText
    private lateinit var emailText: EditText
    private lateinit var deviceID: String
    private lateinit var deviceName: String
    private lateinit var deviceVersion: String
    private var userAddress: String = "Permission Denied"
    lateinit var view: TextView
    private lateinit var renew: TextView
    lateinit var check: CheckBox
    private lateinit var locationManager: LocationManager
    lateinit var ObjectsURLS: ObjectsURLS
    lateinit var coordinatorLayout: CoordinatorLayout
    lateinit var button: Button
    private var gpsStatus: Boolean=false
    val objects=ObjectsURLS()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        check = (findViewById<View>(R.id.check) as CheckBox?)!!
        ObjectsURLS = ObjectsURLS()
        com.example.manikandan.nymexandroid.ObjectsURLS.firebaseID = FirebaseInstanceId.getInstance().getToken().toString()
        Log.e("senderId", com.example.manikandan.nymexandroid.ObjectsURLS.firebaseID)
        check()
        locationManager = (getSystemService(LOCATION_SERVICE) as LocationManager?)!!
        gpsStatus = locationManager!!.isProviderEnabled(LocationManager.GPS_PROVIDER)
        if (gpsStatus == true) {
        } else {
        }
        initGoogleAPIClient() //Init Google API Client
        checkPermissions()
        location
        validation()
    }

    //
    private fun initGoogleAPIClient() {
        //Without Google API Client Auto Location Dialog will not work
        mGoogleApiClient = GoogleApiClient.Builder(this@MainActivity)
                .addApi(LocationServices.API)
                .build()
        mGoogleApiClient.connect()
    }

    /* Check Location Permission for Marshmallow Devices */
    private fun checkPermissions() {
        if (VERSION.SDK_INT >= 23) {
            if ((ContextCompat.checkSelfPermission(this@MainActivity,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            != PackageManager.PERMISSION_GRANTED)) requestLocationPermission() else showSettingDialog()
        } else showSettingDialog()
    }

    /*  Show Popup to access User Permission  */
    private fun requestLocationPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this@MainActivity, Manifest.permission.ACCESS_FINE_LOCATION)) {
            ActivityCompat.requestPermissions(this@MainActivity, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    ACCESS_FINE_LOCATION_INTENT_ID)
        } else {
            ActivityCompat.requestPermissions(this@MainActivity, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    ACCESS_FINE_LOCATION_INTENT_ID)
        }
    }

    /* Show Location Access Dialog */
    private fun showSettingDialog() {
        val locationRequest: LocationRequest = LocationRequest.create()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY //Setting priotity of Location request to high
        locationRequest.interval = 30 * 1000.toLong()
        locationRequest.fastestInterval = 5 * 1000.toLong() //5 sec Time interval for location update
        val builder: LocationSettingsRequest.Builder = LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest)
        builder.setAlwaysShow(true) //this is the key ingredient to show dialog always when GPS is off
        val result: PendingResult<LocationSettingsResult> = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build())
        result.setResultCallback { result ->
            val status: Status = result.status
            result.getLocationSettingsStates()
            when (status.getStatusCode()) {
                LocationSettingsStatusCodes.SUCCESS ->                         // All location settings are satisfied. The client can initialize location
                    // requests here.
                    updateGPSStatus()
                LocationSettingsStatusCodes.RESOLUTION_REQUIRED ->                         // Location settings are not satisfied. But could be fixed by showing the user
                    // a dialog.
                    try {
                        // Show the dialog by calling startResolutionForResult(),
                        // and check the result in onActivityResult().
                        status.startResolutionForResult(this@MainActivity, REQUEST_CHECK_SETTINGS)
                    } catch (e: SendIntentException) {
                        e.printStackTrace()
                        // Ignore the error.
                    }
                LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            REQUEST_CHECK_SETTINGS -> when (resultCode) {
                RESULT_OK -> {
                    Log.e("Settings", "Result OK")
                    updateGPSStatus()
                }
                RESULT_CANCELED -> {
                    Log.e("Settings", "Result Cancel")
                    updateGPSStatus()
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        registerReceiver(gpsLocationReceiver, IntentFilter(BROADCAST_ACTION)) //Register broadcast receiver to check the status of GPS
    }

    override fun onDestroy() {
        super.onDestroy()
        //Unregister receiver on destroy
        if (gpsLocationReceiver != null) unregisterReceiver(gpsLocationReceiver)
    }

    //Run on UI
    private val sendUpdatesToUI: Runnable = Runnable { showSettingDialog() }

    /* Broadcast receiver to check status of GPS */
    private val gpsLocationReceiver: BroadcastReceiver? = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {

            //If Action is Location
            if (intent.action?.matches(BROADCAST_ACTION.toRegex())?.equals(true)!!) {
                val locationManager: LocationManager = context.getSystemService(LOCATION_SERVICE) as LocationManager
                //Check if GPS is turned ON or OFF
                if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    Log.e("About GPS", "GPS is Enabled in your device")
                    updateGPSStatus()
                } else {
                    //If GPS turned OFF show Location Dialog
                    Handler().postDelayed(sendUpdatesToUI, 10)
                    // showSettingDialog();
                    updateGPSStatus()
                    Log.e("About GPS", "GPS is Disabled in your device")
                }
            }
        }
    }

    //Method to update GPS status text
    private fun updateGPSStatus() {}

    /* On Request permission method to check the permisison is granted or not for Marshmallow+ Devices  */
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            ACCESS_FINE_LOCATION_INTENT_ID -> {

                // If request is cancelled, the result arrays are empty.
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {

                    //If permission granted show location dialog if APIClient is not null
                    if (mGoogleApiClient == null) {
                        initGoogleAPIClient()
                        showSettingDialog()
                    } else showSettingDialog()
                } else {
                    updateGPSStatus()
                    Toast.makeText(this@MainActivity, "Location Permission denied.", Toast.LENGTH_SHORT).show()
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return
            }
        }
    }

    //
    fun check() {
        check.setOnClickListener {
            if (check.isChecked) {
                ObjectsURLS.snackbarMessage(coordinatorLayout,resources.getColor(R.color.colorAccent),resources.getColor(R.color.white),resources.getString(R.string.termsandconditionscontent))
            } else {
                ObjectsURLS.snackbarMessage(coordinatorLayout,resources.getColor(R.color.colorAccent), resources.getColor(R.color.white), resources.getString(R.string.nottermschecked))
            }
        }
    }

    private val location: Unit
        get() {
            try {
                val locationManager: LocationManager = getSystemService(LOCATION_SERVICE) as LocationManager
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 5f, this)
            } catch (e: SecurityException) {
                e.printStackTrace()
            }
        }

    override fun onLocationChanged(location: Location) {
        userAddress = ("Latitude: " + location.latitude + "\n Longitude: " + location.longitude)
        try {
            val geoCoder = Geocoder(this, Locale.getDefault())
            val addresses: List<Address> = geoCoder.getFromLocation(location.latitude, location.longitude, 1)
            userAddress = (userAddress + "\n" + addresses[0].getAddressLine(0))
            //  Toast.makeText(getApplicationContext(),useraddress,Toast.LENGTH_LONG).show();
        } catch (e: Exception) {
        }
    }

    override fun onProviderDisabled(provider: String) {
        //Toast.makeText(MainActivity.this, "Please Enable GPS and Internet", Toast.LENGTH_SHORT).show();
        userAddress = "Permission Denied"
    }

    override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {}
    override fun onProviderEnabled(provider: String) {}

    @SuppressLint("HardwareIds")
    private fun validation() {
        name = findViewById<View>(R.id.name) as EditText
        mobileNo = findViewById<View>(R.id.mobileno) as EditText
        emailText = findViewById<View>(R.id.emailid) as EditText
        button = findViewById(R.id.register)
        renew = findViewById(R.id.renew)
        deviceVersion = VERSION.RELEASE
        deviceName = Build.MODEL
        deviceID = Settings.Secure.getString(applicationContext.contentResolver, Settings.Secure.ANDROID_ID)
        coordinatorLayout = findViewById(R.id.container)
        //  Snackbar.make(coordinatorLayout,deviceID,Snackbar.LENGTH_INDEFINITE).show();
        Log.e("DeviceID",deviceID)
        // Toast.makeText(getApplicationContext(),deviceversion+deviceID+devicename,Toast.LENGTH_LONG).show();

        //Temp. suspended for firebase ID creation
        /*AlreadyRegistered().execute(ObjectsURLS.alreadyRegistered,(URLEncoder.encode("deviceid", "UTF-8") + "=" + URLEncoder.encode(deviceID, "UTF-8") +
                "&" + URLEncoder.encode("deviceversion", "UTF-8") + "=" + URLEncoder.encode(deviceVersion, "UTF-8") + "&" +
                URLEncoder.encode("devicename", "UTF-8") + "=" + URLEncoder.encode(deviceName, "UTF-8")+
                "&" + URLEncoder.encode("senderid", "UTF-8") + "=" + URLEncoder.encode(com.example.manikandan.ncdexandroid.ObjectsURLS.firebaseID, "UTF-8")),"POST")*/

        button.setOnClickListener {
            if ((name.text.toString().length >= 3) && (mobileNo.text.length > 9) && (emailText.text.isNotEmpty()) && check.isChecked) {
                Register().execute(ObjectsURLS.register,(URLEncoder.encode("name", "UTF-8") + "=" + URLEncoder.encode(name.text.toString(), "UTF-8") +
                        "&" + URLEncoder.encode("phone", "UTF-8") + "=" + URLEncoder.encode(mobileNo.text.toString(), "UTF-8")+"&"+
                        URLEncoder.encode("mail", "UTF-8") + "=" + URLEncoder.encode(emailText.text.toString(), "UTF-8") + "&"
                        + URLEncoder.encode("deviceid", "UTF-8") +"="+URLEncoder.encode(deviceID, "UTF-8")+ "&" +
                        URLEncoder.encode("deviceversion", "UTF-8") + "=" + URLEncoder.encode(deviceVersion, "UTF-8") + "&" +
                        URLEncoder.encode("devicename", "UTF-8") + "=" + URLEncoder.encode(deviceName, "UTF-8") +
                        "&" + URLEncoder.encode("useraddress", "UTF-8") + "=" + URLEncoder.encode(userAddress, "UTF-8")+
                        "&" + URLEncoder.encode("senderid", "UTF-8") + "=" + URLEncoder.encode(com.example.manikandan.nymexandroid.ObjectsURLS.firebaseID, "UTF-8")),"POST")
            } else {
                if (!check.isChecked) {
                    objects.snackbarMessage(coordinatorLayout,resources.getColor(R.color.colorAccent),resources.getColor(R.color.white), resources.getString(R.string.nottermschecked))
                }
                if (name.text.toString().isEmpty()) {
                    name.hint = "Name is empty"
                    name.setHintTextColor(Color.RED)
                }
                if (name.text.toString().isNotEmpty() && name.text.toString().length < 3) {
                    Toast.makeText(applicationContext, "Name is Invalid", Toast.LENGTH_SHORT).show()
                }
                if (mobileNo.text.toString().isEmpty()) {
                    mobileNo.hint = "Mobile Number is empty"
                    mobileNo.setHintTextColor(Color.RED)
                }
                if (mobileNo.text.toString().isNotEmpty() && mobileNo.text.toString().length <= 9) {
                    Toast.makeText(applicationContext, "Mobile Number is Invalid", Toast.LENGTH_SHORT).show()
                }
                if (emailText.text.toString().isEmpty()) {
                    emailText.hint = "EmailID is empty"
                    emailText.setHintTextColor(Color.RED)
                }
                val email: String = emailText.text.toString().trim { it <= ' ' }
                val emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"

// onClick of button perform this simplest code.
                if (email.matches(emailPattern.toRegex())?.equals( false)!!) {
                    Toast.makeText(applicationContext, "Invalid Email address", Toast.LENGTH_LONG).show()
                }
            }
        }
        renew.setOnClickListener {
            val intent = Intent(this@MainActivity, NewDeviceoIdUser::class.java)
            intent.putExtra("deviceID", deviceID)
            intent.putExtra("deviceaddress", userAddress)
            intent.putExtra("deviceversion", deviceVersion)
            intent.putExtra("devicename", deviceName)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
            startActivity(intent)
        }
    }

    @SuppressLint("StaticFieldLeak")
    internal inner class Register : com.example.manikandan.nymexandroid.AsyncTask.AsyncTaskClass(){
        private lateinit var db: SQLiteDatabase
        private lateinit var userName: String
        private lateinit var userMobileNo: String
        private lateinit var userEmailId: String
        private lateinit var expiredDate: String
        private var result: String = ""
        private lateinit var dialog: ProgressDialog

        override fun onPreExecute() {
            dialog = ProgressDialog(this@MainActivity)
            dialog.setMessage("Processing...")
            dialog.setCancelable(false)
            dialog.setInverseBackgroundForced(false)
            dialog.show()
        }

        override fun onPostExecute(result: String?) {
            dialog.dismiss()
            try {
                if (result!!.contains(objects.code)) {
                    var jsonObject = JSONObject(result)

                    //Log.e("Register",jsonObject.getJSONArray( feeditems.result).toString())

                    //Log.e("Register",jsonObject.getString(objects.code).toString())
                    if (jsonObject.getString(objects.code).equals(objects.success)) {
                        Log.e("Register",jsonObject.getJSONArray( ObjectsURLS.result).toString())
                    }

                        if ((jsonObject.getString(objects.code).equals(ObjectsURLS.success)) && jsonObject.has(objects.result)) {

                        if (jsonObject.has(ObjectsURLS.message)) {
                            objects.snackbarMessage(coordinatorLayout,resources.getColor(R.color.colorPrimary),resources.getColor(R.color.white),jsonObject.getString("message"))
                        }
                        Log.e("Register","inner jsonObject.getJSONArray( innerfeeditems.result).toString()")

                        Log.e("Register",jsonObject.getJSONArray(ObjectsURLS.result).toString())

                        val jsonArray = jsonObject.getJSONArray(ObjectsURLS.result)
                        Log.e("Register",jsonArray.length().toString())

                        Log.e("Json Array",jsonArray.toString())
                        val jsonObject: JSONObject = jsonArray.getJSONObject(0)
                            //Log.e("Json row",jsonObject.getString("UserName"))
                            //Log.e("Json row",jsonObject.getString("UserMobileNo"))
                            //Log.e("Json row",jsonObject.getString("UserEmail"))
                            //Log.e("Json row",jsonObject.getString("AppExipiredDate"))

                            userName = jsonObject.getString("UserName")
                        userMobileNo = (jsonObject.getString("UserMobileNo"))
                        userEmailId = (jsonObject.getString("UserEmail"))
                        expiredDate = (jsonObject.getString("AppExipiredDate"))

                        if ((userName != null) && (userEmailId != null) && (userMobileNo != null)) {
                            val no: Long = userMobileNo.toLong()
                            db = openOrCreateDatabase("NYMEXDB", MODE_PRIVATE, null)
                            db.execSQL(objects.createUserTableIfNotExist)
                            db.execSQL("INSERT INTO NYMEX (name,mobileno,email,expired_date)VALUES('" + userName + "', " + no + " ,'" + userEmailId + "','" + expiredDate + "');")
                            val cursor: Cursor = db.rawQuery("SELECT * FROM NYMEX where mobileno = '" + userMobileNo + "'", null)
                            cursor.moveToFirst()
                            if (cursor.count > 0) {
                                val intent = Intent(applicationContext, Mainframe::class.java)
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                                startActivity(intent)
                                cursor.close()
                            } else {
                                objects.snackbarMessage(coordinatorLayout,resources.getColor(R.color.colorAccent),resources.getColor(R.color.white),"Application process is engaged!...")
                            }
                        } else {
                            objects.snackbarMessage(coordinatorLayout,resources.getColor(R.color.colorAccent),resources.getColor(R.color.white),"If you have an account, you can recover the details.")
                        }
                    }else if (jsonObject.getString(objects.code).equals(objects.fail)) {
                        if (jsonObject.has(ObjectsURLS.message)) {
                            objects.snackbarMessage(coordinatorLayout,resources.getColor(R.color.colorAccent),resources.getColor(R.color.white),jsonObject.getString("message"))
                        }
                    }
                }
            } catch (e: JSONException) {
                objects.snackbarMessage(coordinatorLayout,resources.getColor(R.color.colorAccent),resources.getColor(R.color.white),ObjectsURLS.error)
            } catch (e: SQLException) {
                objects.snackbarMessage(coordinatorLayout,resources.getColor(R.color.colorAccent),resources.getColor(R.color.white),ObjectsURLS.error)
            }catch (e: NullPointerException) {
                objects.snackbarMessage(coordinatorLayout,resources.getColor(R.color.colorAccent),resources.getColor(R.color.white),ObjectsURLS.error)
            }
        }
    }


    @SuppressLint("StaticFieldLeak")
    internal inner class AlreadyRegistered: com.example.manikandan.nymexandroid.AsyncTask.AsyncTaskClass(){
        private lateinit var db: SQLiteDatabase
        private lateinit var userName: String
        private lateinit var userMobileNo: String
        private lateinit var userEmailId: String
        private lateinit var expiredDate: String
        private var result: String = ""
        private lateinit var dialog: ProgressDialog

        override fun onPreExecute() {
            dialog = ProgressDialog(this@MainActivity)
            dialog.setMessage("Processing...")
            dialog.setCancelable(false)
            dialog.setInverseBackgroundForced(false)
            dialog.show()
        }

        override fun onPostExecute(result: String?) {
            dialog.dismiss()
            try {
                if (result!!.contains(objects.code)) {
                    var jsonObject = JSONObject(result)
                    Log.e("code",jsonObject.getString(objects.code))

                    if (jsonObject.getString(objects.code).equals(ObjectsURLS.success)) {
                        Log.e("Success",result)
                        if (jsonObject.has(ObjectsURLS.message)) {
                            objects.snackbarMessage(coordinatorLayout,resources.getColor(R.color.colorAccent),resources.getColor(R.color.white),jsonObject.getString("message"))
                          }
                        val jsonArray = jsonObject.getJSONArray("result")
                        val jsonObject: JSONObject = jsonArray.getJSONObject(0)
                        userName = jsonObject.getString("UserName")
                        userMobileNo = (jsonObject.getString("UserMobileNo"))
                        userEmailId = (jsonObject.getString("UserEmail"))
                        expiredDate = (jsonObject.getString("AppExipiredDate"))

                        if ((userName != null) && (userEmailId != null) && (userMobileNo != null)) {
                            db = openOrCreateDatabase("NYMEXDB", MODE_PRIVATE, null)
                            db.execSQL(objects.createUserTableIfNotExist)
                            db.execSQL("INSERT INTO NYMEX (name,mobileno,email,expired_date)VALUES('$userName', '$userMobileNo','$userEmailId','$expiredDate');")
                            val cursor: Cursor = db.rawQuery("SELECT * FROM NYMEX where mobileno = '" + userMobileNo + "'", null)
                            cursor.moveToFirst()
                            if (cursor.count > 0) {
                                val intent = Intent(applicationContext, Mainframe::class.java)
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                                startActivity(intent)
                                cursor.close()
                            } else {
                                objects.snackbarMessage(coordinatorLayout,resources.getColor(R.color.colorAccent),resources.getColor(R.color.white),"Application process is engaged!...")
                            }
                        } else {
                            objects.snackbarMessage(coordinatorLayout,resources.getColor(R.color.colorAccent),resources.getColor(R.color.white),"If you have an account, you can recover the details.")
                        }
                    }else if (jsonObject.getString(objects.code).equals(objects.fail)) {
                        Log.e("Fail",result)

                        if (jsonObject.has(ObjectsURLS.message)) {
                            objects.snackbarMessage(coordinatorLayout,resources.getColor(R.color.colorAccent),resources.getColor(R.color.white),jsonObject.getString("message"))
                        }
                        }
                }
            } catch (e: JSONException) {
                objects.snackbarMessage(coordinatorLayout,resources.getColor(R.color.colorAccent),resources.getColor(R.color.white),ObjectsURLS.error)
            } catch (e: NullPointerException) {
                objects.snackbarMessage(coordinatorLayout,resources.getColor(R.color.colorAccent),resources.getColor(R.color.white),ObjectsURLS.error)
            }
        }
    }

    companion object {
        private val REQUEST_CHECK_SETTINGS: Int = 0x1
        private val ACCESS_FINE_LOCATION_INTENT_ID: Int = 3
        private val BROADCAST_ACTION: String = "android.location.PROVIDERS_CHANGED"
        const val MY_PERMISSIONS_REQUEST_LOCATION: Int = 99
    }
}




