package com.example.manikandan.nymexandroid

import android.app.AlertDialog
import android.app.ProgressDialog
import android.graphics.Bitmap
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.android.material.snackbar.Snackbar
import org.json.JSONException
import org.json.JSONObject
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
internal class Signals constructor() : Fragment() {
    lateinit var refresh: SwipeRefreshLayout
    lateinit var calls: RecyclerView
    lateinit var variables: ObjectsURLS
    lateinit var coordinatorLayout: CoordinatorLayout

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                                     savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val v: View = inflater.inflate(R.layout.fragment_mcxcalls, container, false)
        coordinatorLayout = v.findViewById(R.id.mcxcalls)
        refresh = v.findViewById(R.id.refresh)
        calls = v.findViewById<View>(R.id.calls) as RecyclerView
        calls.layoutManager=LinearLayoutManager(context,RecyclerView.VERTICAL,false)
        variables = ObjectsURLS()
        Calls().execute(variables!!.calls, "",variables!!.post)
        refresh.setOnRefreshListener {
            Calls().execute(variables!!.calls, "",variables!!.post)
            refresh.isRefreshing=false
        }
        return v
    }

    inner class Calls:com.example.manikandan.nymexandroid.AsyncTask.AsyncTaskClass() {
        var progressDialog: ProgressDialog? = null
        var alertDialog: AlertDialog? = null
        var bitmap: Bitmap? = null
        override fun onPreExecute() {
            progressDialog = ProgressDialog.show(context, "", "Please wait...", true)
            alertDialog = AlertDialog.Builder(context).create()
        }

        override fun onPostExecute(result: String?) {
            progressDialog!!.dismiss()
            alertDialog!!.dismiss()

            //check if exist or not
            // Toast.makeText(getContext(),result,Toast.LENGTH_SHORT).show();
            try {
                var i: Int
                val jsonObject=JSONObject(result)
                if(jsonObject.has(objects.code)) {
                    if(jsonObject.getString(objects.code).equals(objects.success)) {
                        val jsonArray = jsonObject.getJSONArray(objects.result)
                        val signalList:ArrayList<SignalDataItems> = ArrayList()
                        val totalCalls=SignalsList(signalList)

                        i = 0
                        while (i < 20) {
                            val obj = jsonArray.getJSONObject(i)
                            variables = ObjectsURLS()
                            signalList.add(SignalDataItems(obj.getString("created_on"),obj.getString("at_price"),
                                    obj.getString("target1"),obj.getString("target2"),obj.getString("target3"),
                                    obj.getString("target1_achieved"),obj.getString("target2_achieved"),
                                    obj.getString("target3_achieved"),obj.getString("follow_up"),obj.getString("additional_commodity_string"),
                                    obj.getString("contract_month"),obj.getString("exit_call"),obj.getString("price_type"),
                                    obj.getString("subscribe_plan"),obj.getString("commodities"),obj.getString("profit_loss"),
                                    obj.getString("stop_loss"),obj.getString("stop_loss_met"),obj.getString("type"),
                                    obj.getString("exit_price"),obj.getString("last_sms"),obj.getString("url")))
/*                            variables!!.published_date =
                            variables!!.at_price =
                            variables!!.target1 =
                            variables!!.target2 =
                            variables!!.target3 =
                            variables!!.target1_achieved =
                            variables!!.target2_achieved =
                            variables!!.target3_achieved =
                            variables!!.follow_up =
                            variables!!.additional_commodity_string =
                            variables!!.contract_month =
                            variables!!.exit_call =
                            variables!!.price_type =
                            variables!!.subscribe_plan =
                            variables!!.commodities =
                            variables!!.profit_loss =
                            variables!!.stop_loss =
                            variables!!.stop_loss_met =
                            variables!!.type =
                            variables!!.exit_price =
                            variables!!.last_sms =*/
                            i++
                        }
                        calls.adapter=totalCalls
                    }else if (jsonObject.getString(objects.code).equals(objects.fail)) {
                        if (jsonObject.has(variables.message)) {
                            objects.snackbarMessage(coordinatorLayout, resources.getColor(R.color.colorAccent), resources.getColor(R.color.white), jsonObject.getString("message"))
                        }
                    }
                }
            } catch (e: JSONException) {
            } catch (e: NullPointerException) {
                variables = ObjectsURLS()
                Snackbar.make((coordinatorLayout)!!, variables!!.error, Snackbar.LENGTH_INDEFINITE).show()
            }
            progressDialog!!.dismiss()
            alertDialog!!.dismiss()
        }

    }
}