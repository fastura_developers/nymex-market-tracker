package com.example.manikandan.nymexandroid

import android.content.Intent
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class PlanList(val planDataItems: List<PlanDataItems>) : RecyclerView.Adapter<PlanList.MyHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.activity_planlist, parent, false)
        return MyHolder(view)
    }

    override fun onBindViewHolder(holder: MyHolder, position: Int) {
        holder.bindItems(planDataItems[position])
    }

    // return total item from List
    override fun getItemCount(): Int {
        return planDataItems.size
    }

    class MyHolder constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(planDataItems: PlanDataItems) {
            val objects = ObjectsURLS()
            var payNow: Button= itemView.findViewById(R.id.paynow)
            var plan: TextView= itemView.findViewById<View>(R.id.plans) as TextView
            var amount: TextView= itemView.findViewById<View>(R.id.amount) as TextView
            //var pay: Button

            plan.text = Html.fromHtml(planDataItems.plan)
            if (planDataItems.currencytype.equals(itemView.context!!.resources.getString(R.string.india))) {
                amount.text = itemView.context!!.resources.getString(R.string.rupeesign) + " " + planDataItems.amount
                Log.e("41PlanName",planDataItems.amount)
            } else if (planDataItems.currencytype.equals(itemView.context!!.resources.getString(R.string.international))) {
                amount.text = itemView.context!!.resources.getString(R.string.dollarsign) + " " + planDataItems.amount
                Log.e("44PlanName",planDataItems.amount)
            } else {
                amount.text = itemView.context!!.resources.getString(R.string.dollarsign) + " " + planDataItems.amount
                Log.e("47PlanName",planDataItems.amount)
            }

            payNow.setOnClickListener { v ->
                val intent = Intent(v.context, Mainframe::class.java)
                intent.putExtra("amount", planDataItems.amount)
                intent.putExtra("planname", planDataItems.planname)
                intent.putExtra("days", planDataItems.days)
                intent.putExtra("dollarrate", planDataItems.dollarrate)
                intent.putExtra("currencytype", planDataItems.currencytype)
                v.context.startActivity(intent)
            }
        }
    }
}
