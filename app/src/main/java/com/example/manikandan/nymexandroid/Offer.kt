package com.example.manikandan.nymexandroid

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Intent
import android.database.sqlite.SQLiteDatabase
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.felipecsl.gifimageview.library.GifImageView
import java.io.BufferedInputStream
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.io.InputStream
import java.net.MalformedURLException
import java.net.URL
import java.text.SimpleDateFormat
import java.util.*
import javax.net.ssl.HttpsURLConnection

class Offer : AppCompatActivity() {
    var gifImageView: GifImageView? = null
    var bundle: Bundle? = null
    var db: SQLiteDatabase? = null
    lateinit var simpleDateFormat: SimpleDateFormat
    lateinit var offerName:String
    lateinit var seenDate:String
    lateinit var date: Date
    var objects=ObjectsURLS()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_offer)
        date = Calendar.getInstance().time
        bundle = Bundle()
        gifImageView = findViewById<View>(R.id.offergif) as GifImageView
        db = openOrCreateDatabase("NYMEXDB", MODE_PRIVATE, null)
        offerName =resources.getString(R.string.noOffer)
        checkurl()
    }

    fun checkurl() {
        if (intent.hasExtra("url")) {
            bundle = intent.extras
            if (bundle!!.getString("url")!!.length > 0) {
                Retrievebytearray().execute(bundle!!.getString("url"))
                offerName = bundle!!.getString("offername").toString()
                gifImageView!!.startAnimation()
            }
        }
        gifImageView!!.setOnClickListener {
            simpleDateFormat = SimpleDateFormat("yyyy-MM-dd")
            seenDate=simpleDateFormat.format(date)
            Log.e("date",seenDate.toString())
            db!!.execSQL(objects.createOfferTableIfNotExist)
            db!!.execSQL("INSERT INTO offer(message,lastSeen)VALUES('USER HAD SEEN','$seenDate');")
            val cursor = db!!.rawQuery(objects.selectOffer, null)
            cursor.moveToFirst()
            if (cursor.count > 0) {
                val intent = Intent(this@Offer, Mainframe::class.java)
                intent.putExtra("offer_plan", offerName)
                startActivity(intent)
                cursor.close()
            } else {
                val intent = Intent(this@Offer, Mainframe::class.java)
                startActivity(intent)
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    internal inner class Retrievebytearray : AsyncTask<String?, Void?, ByteArray?>() {
        var dialog: ProgressDialog? = null
        override fun onPreExecute() {
            dialog = ProgressDialog(this@Offer)
            dialog!!.setMessage("Processing...")
            dialog!!.setCancelable(false)
            dialog!!.setInverseBackgroundForced(false)
            dialog!!.show()
        }

        override fun doInBackground(vararg params: String?): ByteArray? {
            try {
                val url = URL(params[0])
                val httpsURLConnection = url.openConnection() as HttpsURLConnection
                if (httpsURLConnection.responseCode == 200) {
                    val inputStream: InputStream = BufferedInputStream(httpsURLConnection.inputStream)
                    val outputStream = ByteArrayOutputStream()
                    var read: Int
                    val data = ByteArray(15240)
                    while (inputStream.read(data, 0, data.size).also { read = it } != -1) outputStream.write(data, 0, read)
                    outputStream.flush()
                    return outputStream.toByteArray()
                }
            } catch (e: MalformedURLException) {
            } catch (e: IOException) {
            }
            return null
        }

        override fun onPostExecute(bytes: ByteArray?) {
            dialog!!.dismiss()
            super.onPostExecute(bytes)
            gifImageView!!.setBytes(bytes)
        }
    }
}