package com.example.manikandan.nymexandroid

import android.util.Log
import java.io.BufferedReader
import java.io.InputStream
import java.io.InputStreamReader
import javax.net.ssl.SSLProtocolException

class StreamReader {
    fun streamToString(inputStream: InputStream): String {

        val bufferReader = BufferedReader(InputStreamReader(inputStream))
        var line: String
        var result = ""

        try {
            do {
                line = bufferReader.readLine()
                if (line != null) {
                    result += line
                }
            } while (line != null)
            inputStream.close()
        } catch (ex: Exception) {

        }
        catch (ex: SSLProtocolException) {
            Log.e("ssl error",ex.toString())
        }
        return result
    }
}