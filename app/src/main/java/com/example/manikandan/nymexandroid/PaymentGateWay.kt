package com.example.manikandan.nymexandroid

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity

class PaymentGateWay : AppCompatActivity() {
    lateinit var razorpay: Button
    lateinit var paypal: Button
    lateinit var close: ImageView
    var objects=ObjectsURLS()
    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.payment_dialog)
        paymentGateWaySelection()
    }

    private fun paymentGateWaySelection() {
        intent = Intent(this, PaymentStage::class.java)
        //       razorpay=findViewById(R.id.razorpay);
        paypal = findViewById(R.id.paypal)
        close = findViewById(R.id.close)
        if (getIntent().hasExtra("amount") && getIntent().hasExtra("planname") && getIntent().hasExtra(
                "days"
            )
        ) {
            val bundle = getIntent().extras
            intent!!.putExtra("name", bundle!!.getString("name"))
            intent!!.putExtra("email", bundle.getString("email"))
            intent!!.putExtra("mobileno", bundle.getString("mobileno"))
            intent!!.putExtra("amount", bundle.getString("amount"))
            //Toast.makeText(getApplicationContext(),"Amount at PG"+bundle.getString("amount"),Toast.LENGTH_SHORT).show();
            intent!!.putExtra("planname", bundle.getString("planname"))
            intent!!.putExtra("days", bundle.getString("days"))
            intent!!.putExtra("dollarrate", bundle.getString("dollarrate"))
            intent!!.putExtra("deviceID", bundle.getString("deviceID"))
        }
        /* Temporary stopped
        razorpay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent.putExtra("gateWay",LinksAndStrings.razorpay);
                startActivity(intent);
            }
        });*/paypal.setOnClickListener(View.OnClickListener {
            intent.putExtra("gateWay", objects.payPal)
            startActivity(intent)
        })
        close.setOnClickListener(View.OnClickListener { super@PaymentGateWay.onBackPressed() })
    }
}