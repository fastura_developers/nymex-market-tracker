package com.example.manikandan.nymexandroid

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class PivotPointsList(var levelsDataItems: List<LevelsDataItems>) : RecyclerView.Adapter<PivotPointsList.MyHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):MyHolder
    {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.activity_pivotpointslist, parent, false)
        return MyHolder(view)
    }

    override fun onBindViewHolder(holder: MyHolder, position: Int) {
        holder.bindItems(levelsDataItems[position])
    }

    // return total item from List
    public override fun getItemCount(): Int {
        return levelsDataItems.size
    }

    class MyHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(levelsDataItems: LevelsDataItems) {
            val objects = ObjectsURLS()

            var t1 = itemView.findViewById<View>(R.id.t1) as TextView
            var t2 = itemView.findViewById<View>(R.id.t2) as TextView
            var t3 = itemView.findViewById<View>(R.id.t3) as TextView
            var s1 = itemView.findViewById<View>(R.id.s1) as TextView
            var s2 = itemView.findViewById<View>(R.id.s2) as TextView
            var s3 = itemView.findViewById<View>(R.id.s3) as TextView
            var cname = itemView.findViewById<View>(R.id.cname) as TextView
            var tv = itemView.findViewById<View>(R.id.tv) as TextView
            
            
            
            //Pivot points list
            // Get levelsDataItems position of item in recyclerview to bind data and assign values from list


            var decimalDigits = ""
            var digitsLength = 0
            var decimalPoints = ""
            if (levelsDataItems.R1.substring(
                    levelsDataItems.R1.indexOf(
                        itemView.context.getResources().getString(R.string.dot)
                    ) + 1
                ) !== "" && levelsDataItems.R1.substring(
                    levelsDataItems.R1.indexOf(
                        itemView.context.getResources().getString(R.string.dot)
                    ) + 1
                ).toInt() > 0
            ) {
                decimalDigits = levelsDataItems.R1.substring(
                    levelsDataItems.R1.indexOf(
                        itemView.context.getResources().getString(R.string.dot)
                    ) + 1
                )
                digitsLength = decimalDigits.length
                do {
                    if (digitsLength > 0) {
                        digitsLength = digitsLength - 1
                    }
                    if (decimalDigits[digitsLength].toString().toInt() > 0) {
                        for (i in 0..digitsLength) {
                            decimalPoints = decimalPoints + decimalDigits[i].toString()
                        }
                        t1.setText(
                            levelsDataItems.R1.substring(
                                0,
                                levelsDataItems.R1.lastIndexOf(
                                    itemView.context.getResources().getString(R.string.dot)
                                ) + 1
                            ).toString() + decimalPoints
                        )
                        //Toast.makeText(itemView.context,decimalPoints,Toast.LENGTH_SHORT).show();
                        digitsLength = 0
                    }
                } while (digitsLength > 0)
                decimalPoints = ""
            } else t1.setText(
                levelsDataItems.R1.substring(
                    0,
                    levelsDataItems.R1.lastIndexOf(itemView.context.getResources().getString(R.string.dot))
                )
            )

            if (levelsDataItems.R2.substring(
                    levelsDataItems.R2.indexOf(
                        itemView.context.getResources().getString(R.string.dot)
                    ) + 1
                ) !== "" && levelsDataItems.R2.substring(
                    levelsDataItems.R2.indexOf(
                        itemView.context.getResources().getString(R.string.dot)
                    ) + 1
                ).toInt() > 0
            ) {
                decimalDigits = levelsDataItems.R2.substring(
                    levelsDataItems.R2.indexOf(
                        itemView.context.getResources().getString(R.string.dot)
                    ) + 1
                )
                digitsLength = decimalDigits.length
                do {
                    if (digitsLength > 0) {
                        digitsLength = digitsLength - 1
                    }
                    if (decimalDigits[digitsLength].toString().toInt() > 0) {
                        for (i in 0..digitsLength) {
                            decimalPoints = decimalPoints + decimalDigits[i].toString()
                        }
                        t2.setText(
                            levelsDataItems.R2.substring(
                                0,
                                levelsDataItems.R2.lastIndexOf(
                                    itemView.context.getResources().getString(R.string.dot)
                                ) + 1
                            ).toString() + decimalPoints
                        )
                        //Toast.makeText(itemView.context,decimalPoints,Toast.LENGTH_SHORT).show();
                        digitsLength = 0
                    }
                } while (digitsLength > 0)
                decimalPoints = ""
            } else t2.setText(
                levelsDataItems.R2.substring(
                    0,
                    levelsDataItems.R2.lastIndexOf(itemView.context.getResources().getString(R.string.dot))
                )
            )
            if (levelsDataItems.R3.substring(
                    levelsDataItems.R3.indexOf(
                        itemView.context.getResources().getString(R.string.dot)
                    ) + 1
                ) !== "" && levelsDataItems.R3.substring(
                    levelsDataItems.R3.indexOf(
                        itemView.context.getResources().getString(R.string.dot)
                    ) + 1
                ).toInt() > 0
            ) {
                decimalDigits = levelsDataItems.R3.substring(
                    levelsDataItems.R3.indexOf(
                        itemView.context.getResources().getString(R.string.dot)
                    ) + 1
                )
                digitsLength = decimalDigits.length
                do {
                    if (digitsLength > 0) {
                        digitsLength = digitsLength - 1
                    }
                    if (decimalDigits[digitsLength].toString().toInt() > 0) {
                        for (i in 0..digitsLength) {
                            decimalPoints = decimalPoints + decimalDigits[i].toString()
                        }
                        t3.setText(
                            levelsDataItems.R3.substring(
                                0,
                                levelsDataItems.R3.lastIndexOf(
                                    itemView.context.getResources().getString(R.string.dot)
                                ) + 1
                            ).toString() + decimalPoints
                        )
                        //Toast.makeText(itemView.context,decimalPoints,Toast.LENGTH_SHORT).show();
                        digitsLength = 0
                    }
                } while (digitsLength > 0)
                decimalPoints = ""
            } else t3.setText(
                levelsDataItems.R3.substring(
                    0,
                    levelsDataItems.R3.lastIndexOf(itemView.context.getResources().getString(R.string.dot))
                )
            )
            if (levelsDataItems.S1.substring(
                    levelsDataItems.S1.indexOf(
                        itemView.context.getResources().getString(R.string.dot)
                    ) + 1
                ) !== "" && levelsDataItems.S1.substring(
                    levelsDataItems.S1.indexOf(
                        itemView.context.getResources().getString(R.string.dot)
                    ) + 1
                ).toInt() > 0
            ) {
                decimalDigits = levelsDataItems.S1.substring(
                    levelsDataItems.S1.indexOf(
                        itemView.context.getResources().getString(R.string.dot)
                    ) + 1
                )
                digitsLength = decimalDigits.length
                do {
                    if (digitsLength > 0) {
                        digitsLength = digitsLength - 1
                    }
                    if (decimalDigits[digitsLength].toString().toInt() > 0) {
                        for (i in 0..digitsLength) {
                            decimalPoints = decimalPoints + decimalDigits[i].toString()
                        }
                        s1.setText(
                            levelsDataItems.S1.substring(
                                0,
                                levelsDataItems.S1.lastIndexOf(
                                    itemView.context.getResources().getString(R.string.dot)
                                ) + 1
                            ).toString() + decimalPoints
                        )
                        //Toast.makeText(itemView.context,decimalPoints,Toast.LENGTH_SHORT).show();
                        digitsLength = 0
                    }
                } while (digitsLength > 0)
                decimalPoints = ""
            } else s1.setText(
                levelsDataItems.S1.substring(
                    0,
                    levelsDataItems.S1.lastIndexOf(itemView.context.getResources().getString(R.string.dot))
                )
            )
            if (levelsDataItems.S2.substring(
                    levelsDataItems.S2.indexOf(
                        itemView.context.getResources().getString(R.string.dot)
                    ) + 1
                ) !== "" && levelsDataItems.S2.substring(
                    levelsDataItems.S2.indexOf(
                        itemView.context.getResources().getString(R.string.dot)
                    ) + 1
                ).toInt() > 0
            ) {
                decimalDigits = levelsDataItems.S2.substring(
                    levelsDataItems.S2.indexOf(
                        itemView.context.getResources().getString(R.string.dot)
                    ) + 1
                )
                digitsLength = decimalDigits.length
                do {
                    if (digitsLength > 0) {
                        digitsLength = digitsLength - 1
                    }
                    if (decimalDigits[digitsLength].toString().toInt() > 0) {
                        for (i in 0..digitsLength) {
                            decimalPoints = decimalPoints + decimalDigits[i].toString()
                        }
                        s2.setText(
                            levelsDataItems.S2.substring(
                                0,
                                levelsDataItems.S2.lastIndexOf(
                                    itemView.context.getResources().getString(R.string.dot)
                                ) + 1
                            ).toString() + decimalPoints
                        )
                        //Toast.makeText(itemView.context,decimalPoints,Toast.LENGTH_SHORT).show();
                        digitsLength = 0
                    }
                } while (digitsLength > 0)
                decimalPoints = ""
            } else s2.setText(
                levelsDataItems.S2.substring(
                    0,
                    levelsDataItems.S2.lastIndexOf(itemView.context.getResources().getString(R.string.dot))
                )
            )
            if (levelsDataItems.S3.substring(
                    levelsDataItems.S3.indexOf(
                        itemView.context.getResources().getString(R.string.dot)
                    ) + 1
                ) !== "" && levelsDataItems.S3.substring(
                    levelsDataItems.S3.indexOf(
                        itemView.context.getResources().getString(R.string.dot)
                    ) + 1
                ).toInt() > 0
            ) {
                decimalDigits = levelsDataItems.S3.substring(
                    levelsDataItems.S3.indexOf(
                        itemView.context.getResources().getString(R.string.dot)
                    ) + 1
                )
                digitsLength = decimalDigits.length
                do {
                    if (digitsLength > 0) {
                        digitsLength = digitsLength - 1
                    }
                    if (decimalDigits[digitsLength].toString().toInt() > 0) {
                        for (i in 0..digitsLength) {
                            decimalPoints = decimalPoints + decimalDigits[i].toString()
                        }
                        s3.setText(
                            levelsDataItems.S3.substring(
                                0,
                                levelsDataItems.S3.lastIndexOf(
                                    itemView.context.getResources().getString(R.string.dot)
                                ) + 1
                            ).toString() + decimalPoints
                        )
                        //Toast.makeText(itemView.context,decimalPoints,Toast.LENGTH_SHORT).show();
                        digitsLength = 0
                    }
                } while (digitsLength > 0)
                decimalPoints = ""
            } else s3.setText(
                levelsDataItems.S3.substring(
                    0,
                    levelsDataItems.S3.lastIndexOf(itemView.context.getResources().getString(R.string.dot))
                )
            )
            cname.text = levelsDataItems.SN
            tv.text = levelsDataItems.TV
        }
    }
}