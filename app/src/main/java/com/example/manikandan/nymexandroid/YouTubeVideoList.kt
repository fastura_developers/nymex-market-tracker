package com.example.manikandan.nymexandroid

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.MemoryPolicy
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.Picasso

class YouTubeVideoList (val videoDataItems: List<VideoDataItems>) : RecyclerView.Adapter<YouTubeVideoList.MyHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.activity_youtubevideolist, parent, false)
        return MyHolder(view)
    }

    override fun onBindViewHolder(holder: MyHolder, position: Int) {
        holder.bindItems(videoDataItems[position])
    }

    // return total item from List
    public override fun getItemCount(): Int {
        return videoDataItems.size
    }

    class MyHolder constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(videoDataItems: VideoDataItems) {
            val objects = ObjectsURLS()
            var textSubject = itemView.findViewById<View>(R.id.title) as TextView
            var pubDate = itemView.findViewById<View>(R.id.pubdate) as TextView
            var relativeLayout = itemView.findViewById<View>(R.id.videocontainer) as RelativeLayout
            var networkImageView = itemView.findViewById<View>(R.id.image) as ImageView
            var intent:Intent
            textSubject.setText(videoDataItems.title)
            pubDate.setText(videoDataItems.publishedAt)
            relativeLayout.setOnClickListener(object : View.OnClickListener {
                public override fun onClick(v: View) {
                    val url: String? = videoDataItems.videoId
                    // intent=new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v="+url));
                    intent = Intent(v.getContext(), Mainframe::class.java)
                    intent!!.putExtra("url", "https://www.youtube.com/watch?v=" + url)
                    v.getContext().startActivity(intent)
                }
            })
            val url: String = videoDataItems.imageURL
            Picasso.get().load(url).resize(100, 100).memoryPolicy(
                MemoryPolicy.NO_CACHE
            ).networkPolicy(
                NetworkPolicy.NO_CACHE
            ).into(networkImageView)
        }

    }
}