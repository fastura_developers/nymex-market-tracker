package com.example.manikandan.nymexandroid

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class MyAppFirebaseMessagingService : FirebaseMessagingService() {
    val objects=ObjectsURLS()
    val url=ObjectsURLS()
    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: ${remoteMessage.from}")

        //AutoStartPermissionHelper.getInstance().getAutoStartPermission(applicationContext)

        // Check if message contains a data payload.
        if (remoteMessage.data.isNotEmpty()) {
            Log.d(TAG, "Message data payload: ${remoteMessage.data}")

            if (/* Check if data needs to be processed by long running job */ true) {
                // For long-running tasks (10 seconds or more) use WorkManager.
                //scheduleJob()
            } else {
                // Handle message within 10 seconds
                handleNow()
            }
        }

        // Check if message contains a notification payload.
        remoteMessage.notification?.let {
            Log.d(TAG, "Message Notification Body: ${it.body}")
            // Create an explicit intent for an Activity in your app
            var notificationId=Random()
//            toastMessage(it.body.toString())
            val builder: NotificationCompat.Builder = NotificationCompat.Builder(this)
                .setSmallIcon(android.R.drawable.ic_notification_overlay)
                .setContentTitle("Notifications Example")
                .setContentText("This is a test notification")

            val notificationIntent = Intent(this, Mainframe::class.java)
            val contentIntent = PendingIntent.getActivity(
                this, 0, notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT
            )
            builder.setContentIntent(contentIntent)

            // Add as notification

            // Add as notification
            val manager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
            manager.notify(notificationId.nextInt(), builder.build())

            var builder1 = NotificationCompat.Builder(this, resources.getString(R.string.default_notification_channel_id))
                .setSmallIcon(R.drawable.ic_account)
                .setContentTitle(resources.getString(R.string.app_name))
                .setContentText("Much longer text that cannot fit one line...")
                .setStyle(NotificationCompat.BigTextStyle()
                    .bigText(it.body))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            with(NotificationManagerCompat.from(this)) {
                // notificationId is a unique int for each notification that you must define
                //notify(notificationId.nextInt(), builder1.build())
            }
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }
    // [END receive_message]

    // [START on_new_token]
    /**
     * Called if the FCM registration token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the
     * FCM registration token is initially generated so this is where you would retrieve the token.
     */
    override fun onNewToken(token: String) {
        Log.d(TAG, "Refreshed token: $token")
        ObjectsURLS.firebaseID=token
        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // FCM registration token to your app server.
        sendRegistrationToServer(token)
    }
    // [END on_new_token]

    /**
     * Schedule async work using WorkManager.
     */
 /*   private fun scheduleJob() {
        // [START dispatch_job]
        val work = OneTimeWorkRequest.Builder(MyWorker::class.java).build()
        WorkManager.getInstance().beginWith(work).enqueue()
        // [END dispatch_job]
    }*/

    /**
     * Handle time allotted to BroadcastReceivers.
     */
    private fun handleNow() {
        Log.d(TAG, "Short lived task is done.")
    }

    /**
     * Persist token to third-party servers.
     *
     * Modify this method to associate the user's FCM registration token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private fun sendRegistrationToServer(token: String?) {
        // TODO: Implement this method to send token to your app server.
        Log.d(TAG, "sendRegistrationTokenToServer($token)")
        /*if(Variables.userID!="" && token!=null) {
            UpdateFirebaseSenderID().execute(
                url.userDeails,
                URLEncoder.encode(
                    objects.userID,
                    "UTF-8"
                ) + "=" + URLEncoder.encode(Variables.userID, "UTF-8")+"="+URLEncoder.encode(
                    objects.senderID,
                    "UTF-8"
                ) + "=" + URLEncoder.encode(token, "UTF-8")
            )
        }*/
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private fun sendNotification(messageBody: String) {
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
            PendingIntent.FLAG_ONE_SHOT)

        val channelId = getString(R.string.default_notification_channel_id)
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this, channelId)
            .setSmallIcon(R.drawable.textbackground)
            .setContentTitle(getString(R.string.fcm_message))
            .setContentText(messageBody)
            .setAutoCancel(true)
            .setSound(defaultSoundUri)
            .setContentIntent(pendingIntent)

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(channelId,
                "Channel human readable title",
                NotificationManager.IMPORTANCE_DEFAULT)
            notificationManager.createNotificationChannel(channel)
        }

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build())
    }

    companion object {

        const val TAG = "MyFirebaseMsgService"
    }

    inner class UpdateFirebaseSenderID : com.example.manikandan.nymexandroid.AsyncTask.AsyncTaskClass() {
        private var result = ""

        override fun onPreExecute() {
            //progressBar.visibility= View.VISIBLE
            Log.e("Firebase_SenderID", "Start to sharing")
        }

        override fun onPostExecute(result: String?) {
            Log.e("Firebase_SenderID", "Result was came")

            try {
                if (result != null) {
                    if (result.contains(objects.code)) {
                        var jsonObject = JSONObject(result)
                        if (jsonObject.getString(objects.code).equals(objects.success)) {
                            //Log.e("Result:",result.toString())
                            if (jsonObject.has(objects.message)) {
                                toastMessage(jsonObject.getString(objects.message))
                            }
                            if (jsonObject.has(objects.result)) {
                                val jsonArray = jsonObject.getJSONArray(objects.result)
                                //Log.e("result", jsonArray.toString())

                                if (jsonArray.length() == 1) {
                                    jsonObject = jsonArray.getJSONObject(0)

                                } else {
                                    Log.e(
                                        "Message",
                                        objects.error
                                    )
                                }
                            } else if (jsonObject.getString(objects.code).equals(objects.fail)) {
                                if (jsonObject.has(objects.message)) {
                                    Log.e("Message", jsonObject.getString(objects.message))
                                }
                                // progressBar.visibility=View.GONE
                            }
                        } else {
                            Log.e("Message", "result is null")
                        }
                    } else {
                        // progressBar.visibility=View.GONE
                    }
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            //progressBar.visibility= View.GONE

        }
    }
    fun toastMessage(message: String){
        val toast= Toast.makeText(applicationContext, message, Toast.LENGTH_LONG)
        val view=toast.getView()
        view?.setBackgroundColor(resources.getColor(R.color.colorAccent))
        val message = view!!.findViewById<View>(android.R.id.message) as TextView
        message.setTextColor(resources.getColor(R.color.white))
        toast.show()
    }

}