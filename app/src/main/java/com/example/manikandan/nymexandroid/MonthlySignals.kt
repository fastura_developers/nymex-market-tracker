package com.example.manikandan.nymexandroid

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.ProgressDialog
import android.graphics.Bitmap
import android.graphics.Typeface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TableRow
import android.widget.TextView
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.android.material.snackbar.Snackbar
import org.json.JSONException
import org.json.JSONObject
import java.net.URLEncoder
import java.text.SimpleDateFormat
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
@Suppress("DEPRECATION")
class MonthlySignals constructor() : Fragment() {
    var datePickerDialog: DatePickerDialog? = null
    lateinit var from: EditText
    lateinit var to: EditText
    lateinit var month3: TextView
    lateinit var month6: TextView
    lateinit var year1: TextView
    lateinit var custom: TextView
    lateinit var message: TextView
    lateinit var dateRow: TableRow
    var year: Int = 0
    var month: Int = 0
    var dayOfMonth: Int = 0
    lateinit var calendar: Calendar
    lateinit var date: Date
    lateinit var dynamicDate: Date
    lateinit var simpleDateFormat: SimpleDateFormat
    lateinit var PerformanceStructure: PerformanceStructure
    lateinit var calls: RecyclerView
    lateinit var refresh: SwipeRefreshLayout
    lateinit var variables: ObjectsURLS
    var timeperiod: Int = 1
    lateinit var coordinatorLayout: CoordinatorLayout
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                                     savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val v: View = inflater.inflate(R.layout.fragment_monthlycalls, container, false)
        coordinatorLayout = v.findViewById(R.id.mcxcalls)
        month3 = v.findViewById(R.id.last3months)
        month6 = v.findViewById(R.id.last6months)
        year1 = v.findViewById(R.id.year)
        date = Calendar.getInstance().time
        message = v.findViewById(R.id.message)
        simpleDateFormat = SimpleDateFormat("yyyy/MM/dd")
        custom = v.findViewById(R.id.custom)
        dateRow = v.findViewById(R.id.daterow)
        calls = v.findViewById(R.id.totalcalls) as RecyclerView
        calls.layoutManager=LinearLayoutManager(context,RecyclerView.VERTICAL,false)
        variables = ObjectsURLS()
        from = v.findViewById(R.id.from)
        to = v.findViewById(R.id.to)
        refresh = v.findViewById(R.id.refresh)
        refresh.setOnRefreshListener {
            decider()
            refresh.isRefreshing=false
        }
        setDatePickerDialog()
        //simpleDateFormat.format(date)
        return v
    }

    @SuppressLint("SetTextI18n")
    private fun setDatePickerDialog() {
        calendar = Calendar.getInstance()
        year = calendar.get(Calendar.YEAR)
        month = calendar.get(Calendar.MONTH)
        dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH)
        //default fixed
        year1.setTextColor(resources.getColor(R.color.black))
        year1.typeface = Typeface.DEFAULT
        month6.setTextColor(resources.getColor(R.color.black))
        month6.typeface = Typeface.DEFAULT
        month3.setTextColor(resources.getColor(R.color.black))
        month3.typeface = Typeface.DEFAULT_BOLD
        custom.setTextColor(resources.getColor(R.color.black))
        custom.typeface = Typeface.DEFAULT
        timeperiod = 1
        decider()


        //default
        from.setOnClickListener {
            Calendar.getInstance()
            val datePickerDialog = DatePickerDialog((context)!!,
                    { view, year, monthOfYear, dayOfMonth ->
                        if (from.text.toString().isNotEmpty() && to.text.toString().isNotEmpty()) {
                            OverAllCalls().execute(variables.overallcalls, (URLEncoder.encode("startdate", "UTF-8") + "=" + URLEncoder.encode(from.text.toString(), "UTF-8") +
                                    "&" + URLEncoder.encode("enddate", "UTF-8") + "=" + URLEncoder.encode(to.text.toString(), "UTF-8")),variables.post)
                        }
                        from.setText(year.toString() + "/" + (monthOfYear + 1) + "/" + dayOfMonth)
                    }, year, month, dayOfMonth)
            datePickerDialog.show()
        }
        to.setOnClickListener {
            Calendar.getInstance()
            val datePickerDialog: DatePickerDialog = DatePickerDialog((context)!!,
                    { view, year, monthOfYear, dayOfMonth ->
                        to.setText(year.toString() + "/" + (monthOfYear + 1) + "/" + dayOfMonth)
                        if (from.getText().toString().length > 0 && to.getText().toString().length > 0) {
                            OverAllCalls().execute(variables.overallcalls, (URLEncoder.encode("startdate", "UTF-8") + "=" + URLEncoder.encode(from.text.toString(), "UTF-8") +
                                    "&" + URLEncoder.encode("enddate", "UTF-8") + "=" + URLEncoder.encode(to.text.toString(), "UTF-8")),variables.post)
                        }
                    }, year, month, dayOfMonth)
            datePickerDialog.show()
        }
        month3.setOnClickListener {
            year1.setTextColor(getResources().getColor(R.color.black))
            year1.setTypeface(Typeface.DEFAULT)
            month6.setTextColor(getResources().getColor(R.color.black))
            month6.setTypeface(Typeface.DEFAULT)
            month3.setTextColor(getResources().getColor(R.color.black))
            month3.setTypeface(Typeface.DEFAULT_BOLD)
            custom.setTextColor(getResources().getColor(R.color.black))
            custom.setTypeface(Typeface.DEFAULT)
            from.setText("")
            to.setText("")
            timeperiod = 1
            decider()
        }
        month6.setOnClickListener {
            month6.setTextColor(getResources().getColor(R.color.black))
            month6.setTypeface(Typeface.DEFAULT_BOLD)
            month3.setTextColor(getResources().getColor(R.color.black))
            month3.setTypeface(Typeface.DEFAULT)
            year1.setTextColor(getResources().getColor(R.color.black))
            year1.setTypeface(Typeface.DEFAULT)
            custom.setTextColor(getResources().getColor(R.color.black))
            custom.setTypeface(Typeface.DEFAULT)
            from.setText("")
            to.setText("")
            timeperiod = 2
            decider()
        }
        year1.setOnClickListener {
            year1.setTextColor(getResources().getColor(R.color.black))
            custom.setTextColor(getResources().getColor(R.color.black))
            custom.setTypeface(Typeface.DEFAULT)
            month6.setTextColor(getResources().getColor(R.color.black))
            month6.setTypeface(Typeface.DEFAULT)
            month3.setTextColor(getResources().getColor(R.color.black))
            month3.setTypeface(Typeface.DEFAULT)
            year1.setTypeface(Typeface.DEFAULT_BOLD)
            from.setText("")
            to.setText("")
            timeperiod = 3
            decider()
        }
        //custom click
        custom.setOnClickListener {
            custom.setTextColor(getResources().getColor(R.color.black))
            custom.setTypeface(Typeface.DEFAULT_BOLD)
            year1.setTextColor(getResources().getColor(R.color.black))
            year1.setTypeface(Typeface.DEFAULT)
            month6.setTextColor(getResources().getColor(R.color.black))
            month6.setTypeface(Typeface.DEFAULT)
            month3.setTextColor(getResources().getColor(R.color.black))
            month3.setTypeface(Typeface.DEFAULT)
            timeperiod = 4
            dateRow.setVisibility(View.VISIBLE)
            decider()
        }
    }

    fun decider() {
        calendar = Calendar.getInstance()
        val fromdate: String
        val todate: String
        when (timeperiod) {
            1 -> {
                dateRow.setVisibility(View.GONE)
                dynamicDate = Calendar.getInstance().getTime()
                todate = simpleDateFormat.format(calendar.getTime())
                calendar.add(Calendar.MONTH, -3)
                fromdate = simpleDateFormat.format(calendar.getTime())
                OverAllCalls().execute(variables.overallcalls, (URLEncoder.encode("startdate", "UTF-8") + "=" + URLEncoder.encode(fromdate, "UTF-8") +
                        "&" + URLEncoder.encode("enddate", "UTF-8") + "=" + URLEncoder.encode(todate, "UTF-8")),variables.post)
            }
            2 -> {
                dateRow.setVisibility(View.GONE)
                dynamicDate = Calendar.getInstance().getTime()
                todate = simpleDateFormat.format(calendar.getTime())
                calendar.add(Calendar.MONTH, -6)
                fromdate = simpleDateFormat.format(calendar.getTime())
                OverAllCalls().execute(variables.overallcalls, (URLEncoder.encode("startdate", "UTF-8") + "=" + URLEncoder.encode(fromdate, "UTF-8") +
                        "&" + URLEncoder.encode("enddate", "UTF-8") + "=" + URLEncoder.encode(todate, "UTF-8")),variables.post)
            }
            3 -> {
                dateRow.setVisibility(View.GONE)
                dynamicDate = Calendar.getInstance().getTime()
                todate = simpleDateFormat.format(calendar.getTime())
                calendar.add(Calendar.YEAR, -1)
                fromdate = simpleDateFormat.format(calendar.getTime())
                OverAllCalls().execute(variables.overallcalls, (URLEncoder.encode("startdate", "UTF-8") + "=" + URLEncoder.encode(fromdate, "UTF-8") +
                        "&" + URLEncoder.encode("enddate", "UTF-8") + "=" + URLEncoder.encode(todate, "UTF-8")),variables.post)
            }
            4 -> {
                run({})
                run({})
            }
            else -> {
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    inner class OverAllCalls: com.example.manikandan.nymexandroid.AsyncTask.AsyncTaskClass() {
        var progressDialog: ProgressDialog? = null
        var alertDialog: AlertDialog? = null
        var bitmap: Bitmap? = null
        override fun onPreExecute() {
            progressDialog = ProgressDialog.show(context, "", "Please wait...", true)
            alertDialog = AlertDialog.Builder(context).create()
        }

        override fun onPostExecute(result: String?) {
            progressDialog!!.dismiss()
            alertDialog!!.dismiss()

            //check if exist or not
            try {
                var i: Int
                val jsonObject = JSONObject(result)
                when {
                    jsonObject.getString(variables.code).equals(variables.success)-> {
                            val jsonArray = jsonObject.getJSONArray(variables.result)
                            val signalList:ArrayList<SignalDataItems> = ArrayList()
                            val totalCalls=PerformanceStructure(signalList)
                            i = 0
                        while (i < jsonArray.length()) {
                            val obj = jsonArray.getJSONObject(i)
                            signalList.add(SignalDataItems(obj.getString("created_on"),obj.getString("at_price"),
                                obj.getString("target1"),obj.getString("target2"),obj.getString("target3"),
                                obj.getString("target1_achieved"),obj.getString("target2_achieved"),
                                obj.getString("target3_achieved"),obj.getString("follow_up"),obj.getString("additional_commodity_string"),
                                obj.getString("contract_month"),obj.getString("exit_call"),obj.getString("price_type"),
                                obj.getString("subscribe_plan"),obj.getString("commodities"),obj.getString("profit_loss"),
                                obj.getString("stop_loss"),obj.getString("stop_loss_met"),obj.getString("type"),
                                obj.getString("exit_price"),obj.getString("last_sms"),when(obj.has("url")) {true->obj.getString("url") false->""}))
                            i++
                        }
                        calls.setItemViewCacheSize(100)
                        calls.adapter=totalCalls
                    }

                    jsonObject.getString(variables.code).equals(variables.fail) -> {
                            if (jsonObject.has(variables.message)) {
                                objects.snackbarMessage(
                                    coordinatorLayout,
                                    resources.getColor(R.color.colorAccent),
                                    resources.getColor(R.color.white),
                                    jsonObject.getString("message")
                                )
                            }
                    }
                    else -> {
                        message.setVisibility(View.VISIBLE)
                        //   calls.setVisibility(View.GONE);
                        message.setText(getResources().getString(R.string.message))
                        message.setTextColor(context!!.getResources().getColor(R.color.stoploss))
                    }
                }
            } catch (e: JSONException) {
            } catch (e: NullPointerException) {
                variables = ObjectsURLS()
                Snackbar.make((coordinatorLayout)!!, variables.error, Snackbar.LENGTH_INDEFINITE).show()
            }
        }
    }
}