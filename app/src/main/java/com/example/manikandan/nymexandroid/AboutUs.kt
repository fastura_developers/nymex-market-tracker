package com.example.manikandan.nymexandroid

import android.app.AlertDialog
import android.app.ProgressDialog
import android.graphics.Bitmap
import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.fragment.app.Fragment
import org.json.JSONException
import org.json.JSONObject

/**
 * A simple [Fragment] subclass.
 */
class AboutUs : Fragment() {
    lateinit var objects: ObjectsURLS
    lateinit var aboutUs: TextView
    lateinit var coordinatorLayout: CoordinatorLayout
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val v = inflater.inflate(R.layout.fragment_aboutus, container, false)
        aboutUs = v.findViewById(R.id.aboutus)
        objects = ObjectsURLS()
        coordinatorLayout = v.findViewById<View>(R.id.aboutuspage) as CoordinatorLayout
        AboutUsView().execute(objects!!.aboutus,"",objects.post)
        return v
    }

    inner class AboutUsView : com.example.manikandan.nymexandroid.AsyncTask.AsyncTaskClass() {
        var progressDialog: ProgressDialog? = null
        var alertDialog: AlertDialog? = null
        var bitmap: Bitmap? = null
        override fun onPreExecute() {
            progressDialog = ProgressDialog.show(context, "", "Please wait...", true)
            alertDialog = AlertDialog.Builder(context).create()
        }

        override fun onPostExecute(result: String?) {
            progressDialog!!.dismiss()
            alertDialog!!.dismiss()
            try {
                var i: Int
                val jsonObject = JSONObject(result)
                if(jsonObject.has(objects.code)) {
                    if (jsonObject.getString(objects.code).equals(objects.success)) {
                        i = 0
                        val jsonArray = jsonObject.getJSONArray(objects.result)
                        while (i < jsonArray.length()) {
                            val obj = jsonArray.getJSONObject(i)
                            aboutUs!!.text = Html.fromHtml(obj.getString("aboutcompany"))
                            i++
                        }
                    }else if (jsonObject.getString(objects.code).equals(objects.fail)) {
                        if (jsonObject.has(objects.message)) {
                            objects.snackbarMessage(coordinatorLayout, resources.getColor(R.color.colorAccent), resources.getColor(R.color.white), jsonObject.getString("message"))
                        }
                    }
                }
            } catch (e: JSONException) {
            } catch (e: NullPointerException) {
                objects.snackbarMessage(coordinatorLayout!!,resources.getColor(R.color.colorAccent),resources.getColor(R.color.white),objects.error)
            }
            //  findViewById(R.id.progressBar).setVisibility(View.GONE);
        }
    }
}