package com.example.manikandan.nymexandroid

import android.R
import android.content.Context
import com.onesignal.OSNotificationReceivedEvent
import com.onesignal.OneSignal


class NotificationExtender: OneSignal.OSRemoteNotificationReceivedHandler {
    override fun remoteNotificationReceived(
        context: Context?,
        notificationReceivedEvent: OSNotificationReceivedEvent?
    ) {
        var notification = notificationReceivedEvent?.notification
        var mutableNotification= notification?.mutableCopy()
        if (mutableNotification != null) {
            with(mutableNotification) {
                mutableNotification.setExtender { builder ->
                    builder.setColor(
                        context!!.resources.getColor(R.color.background_light)
                    )
                }

                // If complete isn't call within a time period of 25 seconds, OneSignal internal logic will show the original notification
                // If null is passed to complete

                // If complete isn't call within a time period of 25 seconds, OneSignal internal logic will show the original notification
                // If null is passed to complete
                notificationReceivedEvent!!.complete(mutableNotification) }
        }
    }
}