package com.example.manikandan.nymexandroid

import android.app.Application
import android.util.Log
import com.onesignal.OSNotificationOpenedResult
import com.onesignal.OSNotificationReceivedEvent
import com.onesignal.OneSignal
import org.json.JSONObject

class notificationclass: Application() {
    val objects=ObjectsURLS()
    public override fun onCreate() {
        super.onCreate()
        OneSignal.initWithContext(this);
        OneSignal.setAppId(objects.oneSignalId);
        notificationHandler()
    }

    fun notificationHandler(){
        OneSignal.setNotificationWillShowInForegroundHandler {
            object : OneSignal.OSNotificationWillShowInForegroundHandler {
                override fun notificationWillShowInForeground(notificationReceivedEvent: OSNotificationReceivedEvent?) {
                    val data: JSONObject? = notificationReceivedEvent?.notification?.additionalData
                    if (data != null) {
                            // Complete with a notification means it will show
                            Log.e("Notification data",data.toString())
                        } else {
                            // Complete with null means don't show a notification.
                            notificationReceivedEvent?.complete(null)
                        }
                }

            }

            OneSignal.setNotificationOpenedHandler(OneSignal.OSNotificationOpenedHandler {
                result: OSNotificationOpenedResult? ->
                if (result != null) {
                    Log.e("Notification Title",result.notification.title)
                }
            })
        }
    }
}
