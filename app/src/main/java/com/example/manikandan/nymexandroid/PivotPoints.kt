package com.example.manikandan.nymexandroid

import android.app.AlertDialog
import android.app.ProgressDialog
import android.graphics.Bitmap
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_levels.*
import org.json.JSONException
import org.json.JSONObject
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class PivotPoints : Fragment() {
    lateinit var levelsView: RecyclerView
    lateinit var levelspages: CoordinatorLayout
    lateinit var ObjectsURLS: ObjectsURLS
    lateinit var date: TextView
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val v = inflater.inflate(R.layout.fragment_levels, container, false)
        levelsView = v.findViewById(R.id.levels)
        levelsView.layoutManager=LinearLayoutManager(context,RecyclerView.VERTICAL,false)
        levelspages = v.findViewById(R.id.levelspage)
        date = v.findViewById(R.id.updateddate)
        ObjectsURLS = ObjectsURLS()
        LevelsListClass().execute(ObjectsURLS!!.levels,"",ObjectsURLS.post)
        return v
    }

    inner class LevelsListClass : com.example.manikandan.nymexandroid.AsyncTask.AsyncTaskClass(){
        var progressDialog: ProgressDialog? = null
        var alertDialog: AlertDialog? = null
        var bitmap: Bitmap? = null
        override fun onPreExecute() {
            progressDialog = ProgressDialog.show(context, "", "Please wait...", true)
            alertDialog = AlertDialog.Builder(context).create()
        }

        override fun onPostExecute(result: String?) {
            progressDialog!!.dismiss()
            alertDialog!!.dismiss()
            //  Toast.makeText(getContext(), result.toString(), Toast.LENGTH_LONG).show();
            try {
                var i: Int
                val jsonObject= JSONObject(result)
                if(jsonObject.has(objects.code)) {
                    if (jsonObject.getString(objects.code).equals(objects.success)) {
                        if (jsonObject.has(objects.message)) {
                            objects.snackbarMessage(levelspage, resources.getColor(R.color.colorPrimary), resources.getColor(R.color.white), jsonObject.getString("message"))
                        }
                        val jsonArray = jsonObject.getJSONArray(objects.result)
                        val levelsList: ArrayList<LevelsDataItems> = ArrayList()
                        val levels = PivotPointsList(levelsList)
                        i = 0
                        while (i < jsonArray.length()) {
                            val variables = ObjectsURLS()
                            val obj = jsonArray.getJSONObject(i)
                            levelsList.add(
                                LevelsDataItems(obj.getString("TV"),
                                    obj.getString("SN")
                                    ,obj.getString("R1")
                                    ,obj.getString("R2")
                                    ,obj.getString("R3")
                                    ,obj.getString("S1")
                                    ,obj.getString("S2")
                                    ,obj.getString("S3")
                                    ,obj.getString("Updated_on")))
                            date!!.text = resources.getString(R.string.updatedon) + obj.getString(objects.updatedOn)
                            i++
                        }
                        levelsView.adapter=levels
                    }else if (jsonObject.getString(objects.code).equals(objects.fail)) {
                        if (jsonObject.has(objects.message)) {
                            objects.snackbarMessage(levelspage, resources.getColor(R.color.colorAccent), resources.getColor(R.color.white), jsonObject.getString("message"))
                        }
                    }
                }
            } catch (e: JSONException) {
            } catch (e: NullPointerException) {
                ObjectsURLS = ObjectsURLS()
                Snackbar.make(levelspages!!, ObjectsURLS!!.error, Snackbar.LENGTH_INDEFINITE).show()
            }
            //  findViewById(R.id.progressBar).setVisibility(View.GONE);
        }
    }
}