package com.example.manikandan.nymexandroid

import android.os.AsyncTask
import android.util.Log
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.common.GooglePlayServicesRepairableException
import java.io.BufferedWriter
import java.io.OutputStreamWriter
import java.net.HttpURLConnection
import java.net.URL
import java.util.concurrent.TimeoutException
import javax.net.ssl.SSLHandshakeException

class AsyncTask {

    @Suppress("DEPRECATION")
    abstract class AsyncTaskClass : AsyncTask<String?, Void?, String?>() {

        interface MyAsyncTaskListener {
            fun onPostExecution(result: String)
            fun onPreExecuteConcluded()
        }

        private var mListener: MyAsyncTaskListener? = null

        fun setListener(listener: MyAsyncTaskListener?) {
            mListener = listener
        }

        var i = 0
        private var result: String = ""
        val objects = ObjectsURLS()

        override fun doInBackground(vararg params: String?): String? {
            try {
                val url = URL(params[0])
                Log.e("result",params[0].toString())
                val httpURLConnection = url.openConnection() as HttpURLConnection
                if(params[2]=="POST")
                {
                    httpURLConnection.setRequestProperty(
                        objects.clientserviceObject,
                        objects.clientservice
                    )
                    httpURLConnection.setRequestProperty(objects.authkeyObject, objects.authkey)
                    httpURLConnection.setRequestProperty(
                        objects.servermaintenanceObject,
                        objects.servermaintenance
                    )
                    httpURLConnection.readTimeout = 8000
                    httpURLConnection.connectTimeout = 8000
                }
                httpURLConnection.requestMethod = params[2]
                if(params[2]=="POST") {
                    val outputStream = httpURLConnection.outputStream
                    val bufferedWriter = BufferedWriter(OutputStreamWriter(outputStream, "UTF-8"))
                    val postdata = params[1]
                    bufferedWriter.write(postdata)
                    bufferedWriter.flush()
                    bufferedWriter.close()
                    outputStream.close()
                }
                val responseCode: Int = httpURLConnection.responseCode
                if (responseCode == 200) {
                    result = StreamReader().streamToString(httpURLConnection.inputStream)
                    Log.e("result", result)
                }
                Log.e("Response code",responseCode.toString())
                return result
            }
            catch (ex:TimeoutException){
                    Log.e( "out of time", ex.toString())
                }
            catch (ex: SSLHandshakeException) {
            } catch (ex: GooglePlayServicesRepairableException) {
                Log.e("GPRepairableException", ex.toString());
            } catch (ex: GooglePlayServicesNotAvailableException) {
                Log.e("SecurityException", ex.toString());
            }
            return result
            super.cancel(true)
        }
    }
}