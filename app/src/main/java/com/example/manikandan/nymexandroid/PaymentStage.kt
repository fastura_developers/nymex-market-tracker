package com.example.manikandan.nymexandroid

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.webkit.*
import android.widget.ProgressBar
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatActivity
import androidx.coordinatorlayout.widget.CoordinatorLayout
import com.google.android.material.snackbar.Snackbar
import com.paypal.android.sdk.payments.*
import org.json.JSONException
import org.json.JSONObject
import java.lang.Double.*
import java.math.BigDecimal
import java.util.*

@Suppress("DEPRECATION")
class PaymentStage : AppCompatActivity() {
    lateinit var webView: WebView
    lateinit var activity: Context
    lateinit var coordinatorLayout: CoordinatorLayout
    /**
     * Order Id
     * To Request for Updating Payment Status if Payment Successfully Done
     */
    private val mTXNId // This will create below randomly
            : String? = null
    private var deviceID: String? = null
    private var mServiceId: String? = null
    private val mHash // This will create below randomly
            : String? = null
    private var mProductInfo // From Previous Activity
            : String? = null
    private var mFirstName // From Previous Activity
            : String? = null
    private var mEmailId // From Previous Activity
            : String? = null
    private var mAmount // From Previous Activity
            : Double? = null
    private var days: String? = null
    private var mPhone: String? = null
    private  var gateWay:kotlin.String? = null
    val objects=ObjectsURLS()
    var isFromOrder = false

    /**
     * Handler os handler
     */
    var mHandler = Handler()
    private val TAG = "User info"
    var progressBar: ProgressBar? = null


    /*paypal*/
    /*paypal*/
    /**
     * - Set to PayPalConfiguration.ENVIRONMENT_PRODUCTION to move real money.
     *
     * - Set to PayPalConfiguration.ENVIRONMENT_SANDBOX to use your test credentials
     * from https://developer.paypal.com
     *
     * - Set to PayPalConfiguration.ENVIRONMENT_NO_NETWORK to kick the tires
     * without communicating to PayPal's servers.
     */
    private val PAYPAL_REQUEST_CODE = 7777

    private val config: PayPalConfiguration = PayPalConfiguration()
        .environment(PayPalConfiguration.ENVIRONMENT_PRODUCTION)  //ENVIRONMENT_SANDBOX
        .clientId(objects.PAYPAL_CLIENT_ID)

    override fun onDestroy() {
        stopService(Intent(this, PayPalService::class.java))
        super.onDestroy()
    }
    /*paypal*/


    /*paypal*/
    /**
     * @param savedInstanceState
     */
    @SuppressLint("AddJavascriptInterface", "SetJavaScriptEnabled", "JavascriptInterface")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_paymentstage)
        progressBar = findViewById<View>(R.id.progress) as ProgressBar
        progressBar!!.visibility = View.VISIBLE
        coordinatorLayout = findViewById(R.id.paymentpage)
        webView = findViewById<View>(R.id.payumoney_webview) as WebView //for payumoney
        /**
         * Context Variable
         */
        activity = applicationContext
        /**
         * Getting Intent Variables...
         */
        var bundle = getIntent().extras
        if (bundle != null) {
            mFirstName = bundle.getString("name")
            mEmailId = bundle.getString("email")
            mProductInfo = bundle.getString("planname")
            mAmount = bundle.getString("amount")!!
                .toDouble() //in my case amount getting as String so i parse it double
            mPhone = bundle.getString("mobileno")
            deviceID = bundle.getString("deviceID")
            mServiceId = bundle.getString("service_id")
            isFromOrder = bundle.getBoolean("isFromOrder")
            days = bundle.getString("days")
            gateWay = bundle.getString("gateWay")
        }
        // Checkout.clearUserData(getApplicationContext());

        //Checkout.preload(getApplicationContext());
        try {
            if (gateWay !== "") {
                when (gateWay) {
                    objects.payPal -> payPal()
                    else -> {
                        // payPal();
                        Snackbar.make(
                            coordinatorLayout,
                            resources.getString(R.string.paymentgatewayerror),
                            Snackbar.LENGTH_LONG
                        ).show()
                        throw IllegalStateException("Unexpected value: $gateWay")
                    }
                }
            }
        } catch (e: Exception) {
            Snackbar.make(coordinatorLayout, R.string.paymentgatewayerror, Snackbar.LENGTH_LONG)
                .show()
        }
    }


    /*
    public void razorPay() {
        / **
         * Instantiate Checkout
         */
    /* Checkout checkout = new Checkout();

        / **
         * Set your logo here
         */
    /*checkout.setImage(R.mipmap.logo);

        / **
         * Reference to current activity
         */
    /*final Activity activity = this;

        / **
         * Pass your payment options to the Razorpay Checkout as a JSONObject
         */
    /* try {
            JSONObject options = new JSONObject();

            / **
             * Merchant Name
             * eg: ACME Corp || HasGeek etc.
             */
    /*   options.put("name", getResources().getString(R.string.app_name));

            / **
             * Description can be anything
             * eg: Reference No. #123123 - This order number is passed by you for your internal reference. This is not the `razorpay_order_id`.
             *     Invoice Payment
             *     etc.
             */
    /*   Random rand = new Random();
            String randomString = Integer.toString(rand.nextInt()) + (System.currentTimeMillis() / 1000L);
            mTXNId = hashCal("SHA-256", randomString).substring(0, 20);

            options.put("description", "Subscription Plan");
            //   options.put("order_id", "order_CuEzONfnOI86Ab");
            options.put("currency", "USD");
            / **
             * Amount is always passed in currency subunits
             * Eg: "500" = INR 5.00
             */
    /*    options.put("amount", mAmount * 100);
            options.put("prefill.name", mFirstName);
            options.put("prefill.contact", mPhone);
            options.put("prefill.email", mEmailId);

            checkout.open(activity, options);
        } catch (JSONException e) {
            Log.e(TAG, "Error in starting Razorpay Checkout", e);
        }
    }


        @Override
        public void onPaymentSuccess (String s){
        try {

            // Toast.makeText(getApplicationContext(), s, Toast.LENGTH_LONG).show();
            intent = new Intent(PaymentStage.this, PaymentEndStage.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            intent.putExtra("status", "success");
            intent.putExtra("transaction_id", s);
            intent.putExtra("deviceID", deviceID);
            intent.putExtra("days", days);
            intent.putExtra("name", mFirstName);
            intent.putExtra("email", mEmailId);
            intent.putExtra("phone", mPhone);
            intent.putExtra("payment", mAmount);
            intent.putExtra("plan_name", mProductInfo);
            //       Snackbar.make(coordinatorLayout,"mId"+mId+"mTXNID"+mTXNId+"device"+deviceID+"name"+mFirstName+"payment"+mAmount+"servicename"+mProductInfo,Snackbar.LENGTH_INDEFINITE).show();
            startActivity(intent);
            finish();
        }catch (Exception error){
            Snackbar.make(coordinatorLayout,error.toString(),Snackbar.LENGTH_INDEFINITE).show();
        }
    }

        @Override
        public void onPaymentError ( int i, String s){
        Snackbar.make(coordinatorLayout, s, Snackbar.LENGTH_INDEFINITE).show();
        //  Toast.makeText(getApplicationContext(), i + s, Toast.LENGTH_LONG).show();
          Intent intent = new Intent(PaymentStage.this, Mainframe.class);
        intent.putExtra("payment_result", s);
          startActivity(intent);
    }




    / **
     * Posting Data on PayUMoney Site with Form
     *
     * @param webView
     * @param url
     * @param postData
     */
    /* public void webViewClientPost(WebView webView, String url, Collection<Map.Entry<String, String>> postData) {
        StringBuilder sb = new StringBuilder();
        sb.append("<html><head></head>");
        sb.append("<body onload='form1.submit()'>");
        sb.append(String.format("<form id='form1' action='%s' method='%s'>", url, "post"));
        for (Map.Entry<String, String> item : postData) {
            sb.append(String.format("<input name='%s' type='hidden' value='%s' />", item.getKey(), item.getValue()));
        }
        sb.append("</form></body></html>");
        Log.d("TAG", "webViewClientPost called: " + sb.toString());
        webView.loadData(sb.toString(), "text/html", "utf-8");

    }


    / **
     * Hash Key Calculation
     *
     * @param type
     * @param str
     * @return
     */
    /*   public String hashCal(String type, String str) {
        byte[] hashSequence = str.getBytes();
        StringBuffer hexString = new StringBuffer();
        try {
            MessageDigest algorithm = MessageDigest.getInstance(type);
            algorithm.reset();
            algorithm.update(hashSequence);
            byte messageDigest[] = algorithm.digest();

            for (int i = 0; i < messageDigest.length; i++) {
                String hex = Integer.toHexString(0xFF & messageDigest[i]);
                if (hex.length() == 1)
                    hexString.append("0");
                hexString.append(hex);
            }
        } catch (NoSuchAlgorithmException NSAE) {
        }
        return hexString.toString();
    }*/
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onPressingBack()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        onPressingBack()
    }

    /**
     * On Pressing Back
     * Giving Alert...
     */
    private fun onPressingBack() {
        val intent: Intent
        intent = if (isFromOrder) Intent(this@PaymentStage, Mainframe::class.java) else Intent(
            this@PaymentStage,
            Mainframe::class.java
        )
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        val alertDialog = AlertDialog.Builder(this@PaymentStage)
        alertDialog.setTitle("Warning")
        intent.putExtra("payment_result", "Payment transaction canceled")
        alertDialog.setMessage("Do you cancel this transaction?")
        alertDialog.setPositiveButton(
            "Yes"
        ) { dialog, which ->
            finish()
            startActivity(intent)
        }
        alertDialog.setNegativeButton(
            "No"
        ) { dialog, which -> dialog.dismiss() }
        alertDialog.show()
    }

    private fun payPal() {
        val payPalPayment = PayPalPayment(
            BigDecimal(mAmount.toString()), "USD",
            "Purchase Goods", PayPalPayment.PAYMENT_INTENT_SALE
        )
        val intent = Intent(this, PaymentActivity::class.java)
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config)
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payPalPayment)
        startActivityForResult(intent,PAYPAL_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, @Nullable data: Intent?) {
        if (requestCode == PAYPAL_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                var confirmation: PaymentConfirmation? = data!!.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION)
                if (confirmation != null) {
                    try {
                        val paymentDetails = confirmation.toJSONObject().toString(4)
                        val jsonObject = JSONObject(paymentDetails)
                        Log.e("Payment json result",jsonObject.toString())
                        // Toast.makeText(activity,jsonObject.getJSONObject("response").getString("id"),Toast.LENGTH_LONG).show();
                        intent = Intent(this@PaymentStage, PaymentEndStage::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP)
                        intent.putExtra(
                            "payment_state",
                            jsonObject.getJSONObject("response").getString("state")
                        )
                        intent.putExtra(
                            "status",
                            "success"
                        )
                        intent.putExtra("transaction_id", jsonObject.getJSONObject("response").getString("id"))
                        intent.putExtra("deviceID", deviceID)
                        intent.putExtra("days", days)
                        intent.putExtra("name", mFirstName)
                        intent.putExtra("email", mEmailId)
                        intent.putExtra("phone", mPhone)
                        intent.putExtra("payment", toString(mAmount!!))
                        intent.putExtra("plan_name", mProductInfo)
                        Log.e("Payment Result",jsonObject.getJSONObject("response").getString("id")+"&"+deviceID+
                                "&"+days+
                                "&"+mFirstName+
                                "&"+mPhone+
                                "&"+mAmount+
                                "&"+mProductInfo)

                        Log.e("state",jsonObject.getJSONObject("response").getString("state"))
                        //      Snackbar.make(coordinatorLayout,"mId"+mId+"mTXNID"+mTXNId+"device"+deviceID+"name"+mFirstName+"payment"+mAmount+"servicename"+mProductInfo,Snackbar.LENGTH_INDEFINITE).show();
                        startActivity(intent)
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }
            } else if (resultCode == RESULT_CANCELED) {
                val intent = Intent(this@PaymentStage, Mainframe::class.java)
                intent.putExtra(
                    "payment_result",
                    resources.getString(R.string.paymenttransactioncanceled)
                )
                startActivity(intent)
            }
        } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
            val intent = Intent(this@PaymentStage, Mainframe::class.java)
            intent.putExtra(
                "payment_result",
                resources.getString(R.string.paymenttransactioninvalid)
            )
            startActivity(intent)
        }
    }
}


/* paypal , razerpay

package com.example.manikandan.mcxandroid;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.ProgressBar;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.google.android.material.snackbar.Snackbar;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;

//import com.razorpay.Checkout;
//import com.razorpay.PaymentResultListener;


public class PaymentStage extends AppCompatActivity {
    WebView webView;
    Context activity;
    CoordinatorLayout coordinatorLayout;
    Intent intent;
    /**
     * Order Id
     * To Request for Updating Payment Status if Payment Successfully Done
     */
    private String mTXNId; // This will create below randomly
    private String deviceID;
    private String mServiceId;
    private String mHash; // This will create below randomly
    private String mProductInfo; // From Previous Activity
    private String mFirstName; // From Previous Activity
    private String mEmailId; // From Previous Activity
    private Double mAmount; // From Previous Activity
    private String days;
    private String mPhone,gateWay; // From Previous Activity
    boolean isFromOrder;
    /**
     * Handler os handler
     */
    Handler mHandler = new Handler();
    private String TAG = "User info";
    ProgressBar progressBar;


    /*paypal*/
    /**
     * - Set to PayPalConfiguration.ENVIRONMENT_PRODUCTION to move real money.
     *
     * - Set to PayPalConfiguration.ENVIRONMENT_SANDBOX to use your test credentials
     * from https://developer.paypal.com
     *
     * - Set to PayPalConfiguration.ENVIRONMENT_NO_NETWORK to kick the tires
     * without communicating to PayPal's servers.
     */
    private static final int PAYPAL_REQUEST_CODE = 7777;

    private static PayPalConfiguration config = new PayPalConfiguration()
            .environment(PayPalConfiguration.ENVIRONMENT_SANDBOX)
            .clientId(LinksAndStrings.PAYPAL_CLIENT_ID);

    @Override
    protected void onDestroy() {
        stopService(new Intent(this,PayPalService.class));
        super.onDestroy();
    }
    /*paypal*/


    /**
     * @param savedInstanceState
     */
    @SuppressLint({"AddJavascriptInterface", "SetJavaScriptEnabled", "JavascriptInterface"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paymentstage);
        progressBar = (ProgressBar) findViewById(R.id.progress);
        progressBar.setVisibility(View.VISIBLE);
        coordinatorLayout = findViewById(R.id.paymentpage);

        webView = (WebView) findViewById(R.id.payumoney_webview);//for payumoney

        /**
         * Context Variable
         */
        activity = getApplicationContext();
        /**
         * Getting Intent Variables...
         */
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mFirstName = bundle.getString("name");
            mEmailId = bundle.getString("email");
            mProductInfo = bundle.getString("planname");
            mAmount = Double.parseDouble(bundle.getString("amount"));//in my case amount getting as String so i parse it double
            mPhone = bundle.getString("mobileno");
            deviceID = bundle.getString("deviceID");
            mServiceId = bundle.getString("service_id");
            isFromOrder = bundle.getBoolean("isFromOrder");
            days = bundle.getString("days");
            gateWay = bundle.getString("gateWay");
        }
       // Checkout.clearUserData(getApplicationContext());

        //Checkout.preload(getApplicationContext());
        try {
            if (gateWay != "") {
                switch (gateWay) {
                    case LinksAndStrings.payPal:
                        payPal();
                        break;
                    /*case LinksAndStrings.razorpay:
                        if (!Build.VERSION.RELEASE.matches("4.4.1") && !Build.VERSION.RELEASE.matches("4.4.2") &&
                                !Build.VERSION.RELEASE.matches("4.4.3") && !Build.VERSION.RELEASE.matches("4.4.4") &&
                                !Build.VERSION.RELEASE.matches("4.4") && !Build.VERSION.RELEASE.matches("5.0") &&
                                !Build.VERSION.RELEASE.matches("5.0.1") && !Build.VERSION.RELEASE.matches("5.0.2") &&
                                !Build.VERSION.RELEASE.matches("5.1") && !Build.VERSION.RELEASE.matches("5.1.1")) {
                            razorPay();
                        } else {
                            Intent intent = new Intent(PaymentStage.this, Mainframe.class);
                            intent.putExtra("payment_result", "Android version" + Build.VERSION.RELEASE + "is not support");
                            startActivity(intent);
                            Toast.makeText(getApplicationContext(), "Android version" + Build.VERSION.RELEASE + "" + "is not support" + "\n" + "You can try in rupees", Toast.LENGTH_LONG).show();
                        }
                        break;*/
                    default:
                       // payPal();
                        Snackbar.make(coordinatorLayout,getResources().getString(R.string.paymentgatewayerror),Snackbar.LENGTH_LONG).show();
                        throw new IllegalStateException("Unexpected value: " + gateWay);
                }
            }
        }catch (Exception e){
            Snackbar.make(coordinatorLayout,R.string.paymentgatewayerror,Snackbar.LENGTH_LONG).show();
        }

    }

    /*
    public void razorPay() {
        /**
         * Instantiate Checkout
         */
       /* Checkout checkout = new Checkout();

        /**
         * Set your logo here
         */
        /*checkout.setImage(R.mipmap.logo);

        /**
         * Reference to current activity
         */
        /*final Activity activity = this;

        /**
         * Pass your payment options to the Razorpay Checkout as a JSONObject
         */
       /* try {
            JSONObject options = new JSONObject();

            /**
             * Merchant Name
             * eg: ACME Corp || HasGeek etc.
             */
         /*   options.put("name", getResources().getString(R.string.app_name));

            /**
             * Description can be anything
             * eg: Reference No. #123123 - This order number is passed by you for your internal reference. This is not the `razorpay_order_id`.
             *     Invoice Payment
             *     etc.
             */
         /*   Random rand = new Random();
            String randomString = Integer.toString(rand.nextInt()) + (System.currentTimeMillis() / 1000L);
            mTXNId = hashCal("SHA-256", randomString).substring(0, 20);

            options.put("description", "Subscription Plan");
            //   options.put("order_id", "order_CuEzONfnOI86Ab");
            options.put("currency", "USD");
            /**
             * Amount is always passed in currency subunits
             * Eg: "500" = INR 5.00
             */
        /*    options.put("amount", mAmount * 100);
            options.put("prefill.name", mFirstName);
            options.put("prefill.contact", mPhone);
            options.put("prefill.email", mEmailId);

            checkout.open(activity, options);
        } catch (JSONException e) {
            Log.e(TAG, "Error in starting Razorpay Checkout", e);
        }
    }


        @Override
        public void onPaymentSuccess (String s){
        try {

            // Toast.makeText(getApplicationContext(), s, Toast.LENGTH_LONG).show();
            intent = new Intent(PaymentStage.this, PaymentEndStage.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            intent.putExtra("status", "success");
            intent.putExtra("transaction_id", s);
            intent.putExtra("deviceID", deviceID);
            intent.putExtra("days", days);
            intent.putExtra("name", mFirstName);
            intent.putExtra("email", mEmailId);
            intent.putExtra("phone", mPhone);
            intent.putExtra("payment", mAmount);
            intent.putExtra("plan_name", mProductInfo);
            //       Snackbar.make(coordinatorLayout,"mId"+mId+"mTXNID"+mTXNId+"device"+deviceID+"name"+mFirstName+"payment"+mAmount+"servicename"+mProductInfo,Snackbar.LENGTH_INDEFINITE).show();
            startActivity(intent);
            finish();
        }catch (Exception error){
            Snackbar.make(coordinatorLayout,error.toString(),Snackbar.LENGTH_INDEFINITE).show();
        }
    }

        @Override
        public void onPaymentError ( int i, String s){
        Snackbar.make(coordinatorLayout, s, Snackbar.LENGTH_INDEFINITE).show();
        //  Toast.makeText(getApplicationContext(), i + s, Toast.LENGTH_LONG).show();
          Intent intent = new Intent(PaymentStage.this, Mainframe.class);
        intent.putExtra("payment_result", s);
          startActivity(intent);
    }




    /**
     * Posting Data on PayUMoney Site with Form
     *
     * @param webView
     * @param url
     * @param postData
     */
   /* public void webViewClientPost(WebView webView, String url, Collection<Map.Entry<String, String>> postData) {
        StringBuilder sb = new StringBuilder();
        sb.append("<html><head></head>");
        sb.append("<body onload='form1.submit()'>");
        sb.append(String.format("<form id='form1' action='%s' method='%s'>", url, "post"));
        for (Map.Entry<String, String> item : postData) {
            sb.append(String.format("<input name='%s' type='hidden' value='%s' />", item.getKey(), item.getValue()));
        }
        sb.append("</form></body></html>");
        Log.d("TAG", "webViewClientPost called: " + sb.toString());
        webView.loadData(sb.toString(), "text/html", "utf-8");

    }


    /**
     * Hash Key Calculation
     *
     * @param type
     * @param str
     * @return
     */
 /*   public String hashCal(String type, String str) {
        byte[] hashSequence = str.getBytes();
        StringBuffer hexString = new StringBuffer();
        try {
            MessageDigest algorithm = MessageDigest.getInstance(type);
            algorithm.reset();
            algorithm.update(hashSequence);
            byte messageDigest[] = algorithm.digest();

            for (int i = 0; i < messageDigest.length; i++) {
                String hex = Integer.toHexString(0xFF & messageDigest[i]);
                if (hex.length() == 1)
                    hexString.append("0");
                hexString.append(hex);
            }
        } catch (NoSuchAlgorithmException NSAE) {
        }
        return hexString.toString();
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onPressingBack();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        onPressingBack();
    }

    /**
     * On Pressing Back
     * Giving Alert...
     */
    private void onPressingBack() {
        final Intent intent;
        if (isFromOrder)
            intent = new Intent(PaymentStage.this, Mainframe.class);
        else
            intent = new Intent(PaymentStage.this, Mainframe.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(PaymentStage.this);
        alertDialog.setTitle("Warning");
        intent.putExtra("payment_result", "Payment transaction canceled");
        alertDialog.setMessage("Do you cancel this transaction?");
        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                finish();
                startActivity(intent);
            }
        });
        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alertDialog.show();
    }

    /*public class PayUJavaScriptInterface {
        Context mContext;

        PayUJavaScriptInterface(Context c) {
            mContext = c;
        }

        public void success(long id, final String paymentId) {

            mHandler.post(new Runnable() {

                public void run() {

                    mHandler = null;

                    Intent intent = new Intent(PaymentStage.this, PaymentEndStage.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    intent.putExtra("status", "success");
                    intent.putExtra("transaction_id", mTXNId);
                    intent.putExtra("deviceID", deviceID);
                    intent.putExtra("days", days);
                    intent.putExtra("name", mFirstName);
                    intent.putExtra("email", mEmailId);
                    intent.putExtra("phone", mPhone);
                    intent.putExtra("payment", Double.toString(mAmount ));
                    intent.putExtra("plan_name", mProductInfo);
                    //       Snackbar.make(coordinatorLayout,"mId"+mId+"mTXNID"+mTXNId+"device"+deviceID+"name"+mFirstName+"payment"+mAmount+"servicename"+mProductInfo,Snackbar.LENGTH_INDEFINITE).show();
                    startActivity(intent);
                    finish();

                }

            });
        }
    }*/
    private void payPal() {
        PayPalPayment payPalPayment = new PayPalPayment(new BigDecimal(String.valueOf(mAmount)),"USD",
                "Purchase Goods",PayPalPayment.PAYMENT_INTENT_SALE);
        Intent intent = new Intent(this, PaymentActivity.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION,config);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT,payPalPayment);
        startActivityForResult(intent,PAYPAL_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == PAYPAL_REQUEST_CODE){
            if (resultCode == RESULT_OK){
                PaymentConfirmation confirmation = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirmation != null){
                    try {
                        String paymentDetails = confirmation.toJSONObject().toString(4);

                        JSONObject jsonObject=new JSONObject(paymentDetails);
                       // Toast.makeText(activity,jsonObject.getJSONObject("response").getString("id"),Toast.LENGTH_LONG).show();
                        intent = new Intent(PaymentStage.this, PaymentEndStage.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        intent.putExtra("status", jsonObject.getJSONObject("response").getString("state"));
                        intent.putExtra("transaction_id",jsonObject.getJSONObject("response").getString("id"));
                        intent.putExtra("deviceID", deviceID);
                        intent.putExtra("days", days);
                        intent.putExtra("name", mFirstName);
                        intent.putExtra("email", mEmailId);
                        intent.putExtra("phone", mPhone);
                        intent.putExtra("payment", Double.toString(mAmount));
                        intent.putExtra("plan_name", mProductInfo);
                        //      Snackbar.make(coordinatorLayout,"mId"+mId+"mTXNID"+mTXNId+"device"+deviceID+"name"+mFirstName+"payment"+mAmount+"servicename"+mProductInfo,Snackbar.LENGTH_INDEFINITE).show();
                        startActivity(intent);
                    } catch (JSONException e){
                        e.printStackTrace();
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED)
            {
                Intent intent=new Intent(PaymentStage.this,Mainframe.class);
                intent.putExtra("payment_result",getResources().getString(R.string.paymenttransactioncanceled));
                startActivity(intent);
            }
        } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
            Intent intent=new Intent(PaymentStage.this,Mainframe.class);
            intent.putExtra("payment_result",getResources().getString(R.string.paymenttransactioninvalid));
            startActivity(intent);
        }
    }
}
 */