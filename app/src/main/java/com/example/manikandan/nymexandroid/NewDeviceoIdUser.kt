package com.example.manikandan.nymexandroid

import android.app.ProgressDialog
import android.content.Intent
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.coordinatorlayout.widget.CoordinatorLayout
import org.json.JSONException
import org.json.JSONObject
import java.io.*
import java.net.URLEncoder

class NewDeviceoIdUser: AppCompatActivity() {
    lateinit var emailId: EditText
    lateinit var next: Button
    lateinit var objects: ObjectsURLS
    lateinit var bundle: Bundle
    lateinit var back: ImageView
    lateinit var coordinatorLayout: CoordinatorLayout
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_newdeviceolduser)
        emailId = findViewById<View>(R.id.email) as EditText
        coordinatorLayout = findViewById<View>(R.id.olduser) as CoordinatorLayout
        next = findViewById<View>(R.id.next) as Button
        objects = ObjectsURLS()
        bundle = Bundle()
        back = findViewById<View>(R.id.back) as ImageView
        recover()
        controls()
    }

    fun controls() {
        back!!.setOnClickListener(object : View.OnClickListener {
            public override fun onClick(v: View) {
                val intent: Intent = Intent(getApplicationContext(), MainActivity::class.java)
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
            }
        })
    }

    fun recover() {
        next!!.setOnClickListener {
            if (emailId.getText().toString().isNotEmpty()) {
                intent = intent
                bundle = intent.getExtras()!!
                if (bundle != null) {
                    val deviceID = bundle.getString("deviceID")
                    if (deviceID!!.isNotEmpty()) {
                        RecoverUser().execute(objects.accountrecovery,(URLEncoder.encode("mail", "UTF-8") + "=" + URLEncoder.encode(emailId.getText().toString(), "UTF-8") + "&" +
                                URLEncoder.encode("deviceid", "UTF-8") + "=" + URLEncoder.encode(bundle.getString("deviceID"), "UTF-8") +
                                "&" + URLEncoder.encode("deviceversion", "UTF-8") + "=" + URLEncoder.encode(bundle.getString("deviceversion"), "UTF-8") + "&" +
                                URLEncoder.encode("devicename", "UTF-8") + "=" + URLEncoder.encode(bundle.getString("devicename"), "UTF-8")+
                                "&" + URLEncoder.encode("senderid", "UTF-8") + "=" + URLEncoder.encode(com.example.manikandan.nymexandroid.ObjectsURLS.firebaseID, "UTF-8")),objects.post)
                        //  Snackbar.make(coordinatorLayout,bundle.getString("deviceaddress")+bundle.getString("deviceversion"),Snackbar.LENGTH_INDEFINITE).show();
                    } else {
                        objects.snackbarMessage(coordinatorLayout, resources.getColor(R.color.colorAccent),resources.getColor(R.color.white),objects.deviceNotProcess)
                    }
                } else {
                    objects.snackbarMessage(coordinatorLayout, resources.getColor(R.color.colorAccent),resources.getColor(R.color.white),objects.deviceNotProcess)
                }
            } else {
                if (emailId.text.toString().isEmpty()) {
                    emailId.hint = resources.getString(R.string.emailEmpty)
                    emailId.setHintTextColor(Color.RED)
                }
            }
        }
    }

    internal inner class RecoverUser : com.example.manikandan.nymexandroid.AsyncTask.AsyncTaskClass() {
        lateinit var db: SQLiteDatabase
        lateinit var userName: String
        var userMobileNo: String? = null
        var userEmailId: String? = null
        var expiredDate: String? = null
        private var dialog: ProgressDialog? = null

        override fun onPreExecute() {
            dialog = ProgressDialog(this@NewDeviceoIdUser)
            dialog!!.setMessage(resources.getString(R.string.process))
            dialog!!.setCancelable(false)
            dialog!!.setInverseBackgroundForced(false)
            dialog!!.show()
        }

        override fun onPostExecute(result: String?) {
            dialog!!.dismiss()
            try {
                if (result != null) {
                    Log.e("old user result",result)
                }
                var i: Int = 0
                val jsonObject= JSONObject(result)
                if(jsonObject.has(objects.code)) {
                    if (jsonObject.getString(objects.code).equals(objects.success)) {
                        if (jsonObject.has(objects.message)) {
                            objects.snackbarMessage(
                                coordinatorLayout,
                                resources.getColor(R.color.colorAccent),
                                resources.getColor(R.color.white),
                                jsonObject.getString("message")
                            )
                        }
                        val jsonArray = jsonObject.getJSONArray(objects.result)
                        val jsonObject=jsonArray.getJSONObject(0)
                        userName = jsonObject.getString("UserName")
                        userMobileNo = (jsonObject.getString("UserMobileNo"))
                        userEmailId = (jsonObject.getString("UserEmail"))
                        expiredDate = (jsonObject.getString("AppExipiredDate"))
                        if ((userEmailId != null) && (userMobileNo != null)) {
                            val no: Long = userMobileNo!!.toLong()
                            db = openOrCreateDatabase("NYMEXDB", MODE_PRIVATE, null)
                            db.execSQL(this@NewDeviceoIdUser.objects.createUserTableIfNotExist)
                            db.execSQL("INSERT INTO NYMEX (name,mobileno,email,expired_date)VALUES('$userName', $no ,'$userEmailId','$expiredDate');")
                            //db.execSQL("INSERT INTO NYMEX (name,mobileno,email,expired_date)VALUES('" + userName + "', " + no + " ,'" + userEmailId + "','" + expiredDate + "');")
                            val cursor: Cursor = db.rawQuery("SELECT * FROM NYMEX where mobileno = '$userMobileNo'", null)
                            cursor.moveToFirst()
                            if (cursor.count > 0) {
                                val intent = Intent(applicationContext, Mainframe::class.java)
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                                //  Snackbar.make(coordinatorLayout,userName+userMobileNo+useremailid+expired_date,Snackbar.LENGTH_LONG).show();
                                startActivity(intent)
                                cursor.close()
                            } else {
                                objects.snackbarMessage(coordinatorLayout, resources.getColor(R.color.colorAccent),resources.getColor(R.color.white),objects.processEngaged)
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), result, Toast.LENGTH_LONG).show()
                        }
                    }else if(jsonObject.getString(objects.code).equals(objects.fail)) {
                        if (jsonObject.has(objects.message)) {
                            objects.snackbarMessage(
                                coordinatorLayout,
                                resources.getColor(R.color.colorAccent),
                                resources.getColor(R.color.white),
                                jsonObject.getString("message")
                            )
                        }
                    }
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            //       Snackbar.make(coordinatorLayout,userName+userMobileNo+useremailid+expired_date,Snackbar.LENGTH_LONG).show();

        }
    }
}