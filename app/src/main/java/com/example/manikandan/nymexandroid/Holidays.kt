package com.example.manikandan.nymexandroid

import android.app.AlertDialog
import android.app.ProgressDialog
import android.graphics.Bitmap
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import org.json.JSONException
import org.json.JSONObject
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class Holidays : Fragment() {
    lateinit var HolidayList: HolidayList
    lateinit var ObjectsURLS: ObjectsURLS
    lateinit var recyclerView: RecyclerView
    lateinit var coordinatorLayout: CoordinatorLayout
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val v = inflater.inflate(R.layout.fragment_holidays, container, false)
        ObjectsURLS = ObjectsURLS()
        recyclerView = v.findViewById(R.id.headercontent)
        recyclerView.layoutManager=LinearLayoutManager(context,RecyclerView.VERTICAL,false)
        coordinatorLayout = v.findViewById(R.id.holidays)
        HolidaysListView().execute(ObjectsURLS!!.holidays,"",ObjectsURLS.post)
        return v
    }

    inner class HolidaysListView: com.example.manikandan.nymexandroid.AsyncTask.AsyncTaskClass() {
        var progressDialog: ProgressDialog? = null
        var alertDialog: AlertDialog? = null
        var bitmap: Bitmap? = null
        override fun onPreExecute() {
            progressDialog = ProgressDialog.show(context, "", "Please wait...", true)
            alertDialog = AlertDialog.Builder(context).create()
        }

        override fun onPostExecute(result: String?) {
            progressDialog!!.dismiss()
            alertDialog!!.dismiss()

            //check if exist or not
            // Toast.makeText(getContext(),result,Toast.LENGTH_SHORT).show();
            try {
                val jsonObject= JSONObject(result)
                if(jsonObject.has(objects.code)) {
                    if (jsonObject.getString(objects.code).equals(objects.success)) {
                        val jsonArray = jsonObject.getJSONArray(objects.result)
                        val holidays: ArrayList<HolidayDataItems> = ArrayList()
                        val holidayList = HolidayList(holidays)
                        while (i < jsonArray.length()) {
                            val obj = jsonArray.getJSONObject(i)
                            holidays.add(HolidayDataItems(obj.getString("Date"),obj.getString("Days"),obj.getString("Particulars"),
                                obj.getString("Ms"),obj.getString("Es")))
                            i++
                        }
                        recyclerView.adapter=holidayList
                    }
                }
            } catch (e: JSONException) {
            } catch (e: NullPointerException) {
                ObjectsURLS = ObjectsURLS()
                Snackbar.make(coordinatorLayout!!, ObjectsURLS!!.error, Snackbar.LENGTH_INDEFINITE).show()
            }
            progressDialog!!.dismiss()
            alertDialog!!.dismiss()
        }
    }
}