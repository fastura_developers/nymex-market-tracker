package com.example.manikandan.nymexandroid

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.tabs.TabItem
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayout.OnTabSelectedListener
import org.json.JSONException
import org.json.JSONObject
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class Plans : Fragment() {
    lateinit var objects: ObjectsURLS
    lateinit var planList: PlanList
    lateinit var bankDetails: BankDetails
    lateinit var planListView: RecyclerView
    lateinit var deviceID: String
    lateinit var intent: Intent
    lateinit var tabLayout: TabLayout
    //private lateinit var india: TabItem
    //lateinit var bank: TabItem
    lateinit var international: TabItem
    lateinit var coordinatorLayout: CoordinatorLayout
    lateinit var bundle: Bundle
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val v = inflater.inflate(R.layout.fragment_plans, container, false)
        //object definition
        bundle = Bundle()
        planListView= v.findViewById<View>(R.id.planlist) as RecyclerView
        planListView.layoutManager=LinearLayoutManager(context,RecyclerView.VERTICAL,false)
        tabLayout = v.findViewById<View>(R.id.maintablayout) as TabLayout
        //india = v.findViewById(R.id.india) as TabItem

        //international=(TabItem)v.findViewById(R.id.international);  Temproarily Hidden
        //bank = v.findViewById(R.id.bankdetails) as TabItem
        objects = ObjectsURLS()
        coordinatorLayout = v.findViewById(R.id.plan) as CoordinatorLayout
        expireduser()
        intent = activity!!.intent
        PlansListView().execute(objects.plans, "",objects.post, tabLayout.getTabAt(0)!!.text.toString())
        defaultOne()
        return v
    }

    private fun defaultOne() {
        objects = ObjectsURLS()
        tabLayout!!.addOnTabSelectedListener(object : OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                if (tab.position == 0) {
                    PlansListView().execute(objects.plans, "",objects.post, tabLayout.getTabAt(0)!!.text.toString())
                }  else if (tab.position == 1) {
                    intent = activity!!.intent
                    BankDetailsView().execute(objects.bankdetails,"",objects.post)
                } else {
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {}
            override fun onTabReselected(tab: TabLayout.Tab) {}
        })
    }

    fun expireduser() {
        try {
            val expired_date = arguments!!.getString("expired_date")
            if (expired_date != null || expired_date!!.length > 0) {
                Snackbar.make(coordinatorLayout, objects.expiredcote + " " + expired_date, 5000).show()
            }
        } catch (e: NullPointerException) {
        }
    }

    inner class PlansListView : com.example.manikandan.nymexandroid.AsyncTask.AsyncTaskClass() {
        var progressDialog: ProgressDialog? = null
        var alertDialog: AlertDialog? = null
        var bitmap: Bitmap? = null
        private var result: String = ""
        var planname: String=tabLayout.getTabAt(0)?.getText().toString()
        override fun onPreExecute() {
            progressDialog = ProgressDialog.show(context, "", "Please wait...", true)
            alertDialog = AlertDialog.Builder(context).create()
        }

        override fun onPostExecute(result: String?) {
            progressDialog!!.dismiss()
            alertDialog!!.dismiss()
            try {
                var i: Int = 0
                val jsonObject= JSONObject(result)
                val planList:ArrayList<PlanDataItems> = ArrayList()
                val plans=PlanList(planList)
                if(jsonObject.has(objects.code)) {
                    if (jsonObject.getString(objects.code).equals(objects.success)) {
                        val jsonArray = jsonObject.getJSONArray(objects.result)

                        while (i < jsonArray.length()) {
                            val obj = jsonArray.getJSONObject(i)
                            /*planList.add(
                                PlanDataItems(obj.getString("description"),obj.getString("no_of_days"),
                                obj.getString("price"),obj.getString("tagname"),obj.getString("dollar_rate"),
                                obj.getString("tagname"))
                            )*/
                            if (this.planname.equals(resources.getString(R.string.india))!!) {
                                planList.add(PlanDataItems(obj.getString("description"),obj.getString("no_of_days"),
                                    obj.getString("price"),obj.getString("tagname"),obj.getString("dollar_rate"),
                                    planname.toString()))
                                Log.e("132PlanName",planname.toString())
                            } else if (planname.equals(resources.getString(R.string.international))) {
                                val dollar = obj.getString("price").toDouble() / obj.getString("dollar_rate").toDouble()
                                planList.add(PlanDataItems(obj.getString("description"),obj.getString("no_of_days"),
                                    java.lang.Long.toString(Math.round(dollar)),obj.getString("tagname"),obj.getString("dollar_rate"),
                                    planname.toString()))
                                Log.e("138PlanName",planname.toString())
                            }else{
                                planList.add(PlanDataItems(obj.getString("description"),obj.getString("no_of_days"),
                                    obj.getString("price"),obj.getString("tagname"),obj.getString("dollar_rate"),
                                    obj.getString("tagname")))
                                Log.e("143PlanName",planname.toString()+obj.getString("price"))
                            }
                            i++
                        }
                    }else if (jsonObject.getString(objects.code).equals(objects.fail)) {
                        if (jsonObject.has(objects.message)) {
                            objects.snackbarMessage(coordinatorLayout, resources.getColor(R.color.colorAccent), resources.getColor(R.color.white), jsonObject.getString("message"))
                        }
                    }
                    planListView.adapter=plans
                }
            } catch (e: JSONException) {
            } catch (e: NullPointerException) {
                objects.snackbarMessage(coordinatorLayout, resources.getColor(R.color.colorAccent), resources.getColor(R.color.white),objects.error)
            }
            progressDialog!!.dismiss()
            alertDialog!!.dismiss()
        }
    }

    inner class BankDetailsView : com.example.manikandan.nymexandroid.AsyncTask.AsyncTaskClass(){
        var progressDialog: ProgressDialog? = null
        var alertDialog: AlertDialog? = null
        var bitmap: Bitmap? = null
        private var result: String = ""

        override fun onPreExecute() {
            progressDialog = ProgressDialog.show(context, "", "Please wait...", true)
            alertDialog = AlertDialog.Builder(context).create()
        }

        override fun onPostExecute(result: String?) {
            progressDialog!!.dismiss()
            alertDialog!!.dismiss()
            try {
                var i: Int
                val jsonObject=JSONObject(result)
                if(jsonObject.has(objects.code)) {
                    if (jsonObject.getString(objects.code).equals(objects.success)) {
                        if (jsonObject.has(objects.message)) {
                            objects.snackbarMessage(
                                coordinatorLayout,
                                resources.getColor(R.color.colorAccent),
                                resources.getColor(R.color.white),
                                jsonObject.getString("message")
                            )
                        }
                        val jsonArray = jsonObject.getJSONArray(objects.result)
                        val bankDataItems:ArrayList<BankDataItems> = ArrayList()
                        val bankDetails=BankDetails(bankDataItems)
                        i = 0
                        while (i < jsonArray.length()) {
                            val obj = jsonArray.getJSONObject(i)
                            bankDataItems.add(BankDataItems(obj.getString("user_name"),obj.getString("bank_name"),obj.getString("account_no"),
                                obj.getString("ifsc_code"),obj.getString("branch_name")))
                            i++
                        }
                        planListView.adapter = bankDetails
                    }else if (jsonObject.getString(objects.code).equals(objects.fail)) {
                        if (jsonObject.has(objects.message)) {
                            objects.snackbarMessage(
                                coordinatorLayout,
                                resources.getColor(R.color.colorAccent),
                                resources.getColor(R.color.white),
                                jsonObject.getString("message")
                            )
                        }
                    }
                }else{
                    objects.snackbarMessage(
                        coordinatorLayout,
                        resources.getColor(R.color.colorAccent),
                        resources.getColor(R.color.white),
                        jsonObject.getString(objects.error)
                    )
                }
            } catch (e: JSONException) {
            } catch (e: NullPointerException) {
                objects.snackbarMessage(
                    coordinatorLayout,
                    resources.getColor(R.color.colorAccent),
                    resources.getColor(R.color.white),
                    objects.error
                )
            }
            progressDialog!!.dismiss()
            alertDialog!!.dismiss()
        }
    }
}