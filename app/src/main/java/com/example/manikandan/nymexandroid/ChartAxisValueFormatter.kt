package com.example.manikandan.nymexandroid

import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.formatter.IAxisValueFormatter
import java.text.DecimalFormat

class ChartAxisValueFormatter constructor() : IAxisValueFormatter {
    private val mFormat: DecimalFormat
    public override fun getFormattedValue(value: Float, axis: AxisBase): String {
        // "value" represents the position of the label on the axis (x or y)
        return mFormat.format(value.toDouble()) + " $"
    }

    /** this is only needed if numbers are returned, else return 0  */
    init {

        // format values to 1 decimal digit
        mFormat = DecimalFormat("###,###,##0.0")
    }
}