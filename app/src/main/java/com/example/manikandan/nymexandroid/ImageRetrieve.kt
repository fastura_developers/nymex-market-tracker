package com.example.manikandan.nymexandroid

import android.content.Context
import android.graphics.Bitmap
import android.util.LruCache
import com.android.volley.Cache
import com.android.volley.Network
import com.android.volley.RequestQueue
import com.android.volley.toolbox.BasicNetwork
import com.android.volley.toolbox.DiskBasedCache
import com.android.volley.toolbox.HurlStack
import com.android.volley.toolbox.ImageLoader

class ImageRetrieve private constructor(private var mCtx: Context?) {
    private var mRequestQueue: RequestQueue?
    val imageLoader: ImageLoader

    // Don't forget to start the volley request queue
    val requestQueue: RequestQueue
        get() {
            if (mRequestQueue == null) {
                val cache: Cache = DiskBasedCache(mCtx?.getCacheDir(), 10 * 1024 * 1024)
                val network: Network = BasicNetwork(HurlStack())
                mRequestQueue = RequestQueue(cache, network)
                // Don't forget to start the volley request queue
                mRequestQueue!!.start()
            }
            return mRequestQueue!!
        }

    companion object {
        private var mInstance: ImageRetrieve? = null
        @Synchronized
        fun getInstance(context: Context?): ImageRetrieve? {
            if (mInstance == null) {
                mInstance = ImageRetrieve(context)
            }
            return mInstance
        }
    }

    init {
        mRequestQueue = requestQueue
        imageLoader = ImageLoader(mRequestQueue,
                object : ImageLoader.ImageCache {
                    private val cache: LruCache<String, Bitmap> = LruCache(20)
                    public override fun getBitmap(url: String): Bitmap {
                        return cache.get(url)
                    }

                    public override fun putBitmap(url: String, bitmap: Bitmap) {
                        cache.put(url, bitmap)
                    }
                })
    }
}