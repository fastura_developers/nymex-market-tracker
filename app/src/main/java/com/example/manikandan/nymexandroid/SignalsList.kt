package com.example.manikandan.nymexandroid

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TableRow
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class SignalsList (val signalDataItems: List<SignalDataItems>) : RecyclerView.Adapter<SignalsList.MyHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.activity_signallist, parent, false)
        return MyHolder(view)
    }

    override fun onBindViewHolder(holder:MyHolder,  position: Int) {
        holder.bindItems(signalDataItems[position])
    }

    // return total item from List
    public override fun getItemCount(): Int {
        return signalDataItems.size
    }

    class MyHolder (itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(signalDataItems: SignalDataItems) {
            val objects = ObjectsURLS()

            var date = itemView.findViewById<View>(R.id.date) as TextView
            var signals = itemView.findViewById<View>(R.id.calls) as TextView
            var profitLoss = itemView.findViewById<View>(R.id.profitloss) as TextView
            var target1 = itemView.findViewById<View>(R.id.row2) as TableRow
            var target2 = itemView.findViewById<View>(R.id.row3) as TableRow
            var target3 = itemView.findViewById<View>(R.id.row4) as TableRow
            var exit: TableRow = itemView.findViewById<View>(R.id.row5) as TableRow
            var stopLoss = itemView.findViewById<View>(R.id.row6) as TableRow
            var targetText1 = itemView.findViewById<View>(R.id.target1achieved) as TextView
            var targetText2 = itemView.findViewById<View>(R.id.target2achieved) as TextView
            var targetText3 = itemView.findViewById<View>(R.id.target3achieved) as TextView
            var exitText = itemView.findViewById<View>(R.id.exit) as TextView
            var stopLossText = itemView.findViewById<View>(R.id.stoploss) as TextView
            var followUp = itemView.findViewById(R.id.followup) as TextView
            var target1AchievedYes = itemView.findViewById(R.id.target1achievedyes) as TextView
            var target2AchievedYes = itemView.findViewById(R.id.target2achievedyes) as TextView
            var target3AchievedYes = itemView.findViewById(R.id.target3achievedyes) as TextView
            var exitCallYes = itemView.findViewById(R.id.exitcallyes) as TextView
            var stopLossYes = itemView.findViewById(R.id.stoplossyes) as TextView

            val firstSubString: String
            if (signalDataItems.last_sms!!.length > 0 && (signalDataItems.last_sms!!.contains("AM") || signalDataItems.last_sms!!.contains("PM"))) {
                val split: Array<String> = signalDataItems.last_sms!!.split(" ".toRegex()).toTypedArray()
                firstSubString = split.get(0)
            } else {
                firstSubString = "..."
            }
            date.text = signalDataItems.created_on + "\n" + firstSubString
            signals.setText(signalDataItems.type!!.toUpperCase() + " " + signalDataItems.commodities + " " + signalDataItems.contract_month + " " + signalDataItems.additional_commodity_string + " " + signalDataItems.price_type + " " + signalDataItems.at_price + " " + "Target" + " " + signalDataItems.target1 + "/" + signalDataItems.target2 + "/" + signalDataItems.target3 + " " + "SL" + " " + signalDataItems.stop_loss)
            profitLoss.setText(signalDataItems.profit_loss)
            followUp.setText(signalDataItems.follow_up)
            target1AchievedYes.setText(signalDataItems.target1_achieved)
            target2AchievedYes.setText(signalDataItems.target2_achieved)
            target3AchievedYes.setText(signalDataItems.target3_achieved)
            stopLossYes.setText(signalDataItems.stop_loss_met)
            exitCallYes.setText(signalDataItems.exit_call)

            if (signalDataItems.follow_up.equals(objects.target)) {
                if (signalDataItems.profit_loss!!.contains("-")) {
                    var PL: Double = signalDataItems.profit_loss.toDouble()
                    PL = PL * (-1)
                    profitLoss.setText(PL.toString())
                }
                if (signalDataItems.target1_achieved.equals(objects.yes)) {
                    target1.setVisibility(View.VISIBLE)
                    targetText1.setText(objects.target1message)
                    date.setTextColor(itemView.context.getResources().getColor(R.color.lightgreen))
                    signals.setTextColor(itemView.context.getResources().getColor(R.color.lightgreen))
                    profitLoss.setTextColor(itemView.context.getResources().getColor(R.color.lightgreen)
                    )

                }
                if (signalDataItems.target2_achieved.equals(objects.yes)) {
                    target2.setVisibility(View.VISIBLE)
                    targetText2.setText(objects.target2message)
                    date.setTextColor(itemView.context.getResources().getColor(R.color.lightgreen))
                    signals.setTextColor(itemView.context.getResources().getColor(R.color.lightgreen))
                    profitLoss.setTextColor(itemView.context.getResources().getColor(R.color.lightgreen))
                }
                if (signalDataItems.target3_achieved.equals(objects.yes)) {
                    target3.setVisibility(View.VISIBLE)
                    targetText3.setText(objects.target3message)
                    date.setTextColor(itemView.context.getResources().getColor(R.color.lightgreen))
                    signals.setTextColor(itemView.context.getResources().getColor(R.color.lightgreen))
                    profitLoss.setTextColor(itemView.context.getResources().getColor(R.color.lightgreen))
                }
            }
            if (signalDataItems.follow_up.equals(objects.exit)) {
                if (exitCallYes.text.toString().equals(objects.yes)) {
                    exit.setVisibility(View.VISIBLE)
                    exitText.setText(objects.exittext + " " + signalDataItems.exit_price)
                    date.setTextColor(itemView.context.getResources().getColor(R.color.exit))
                    signals.setTextColor(itemView.context.getResources().getColor(R.color.exit))
                    profitLoss.setTextColor(itemView.context.getResources().getColor(R.color.exit))
                }
            }
            if (signalDataItems.follow_up.equals(objects.stoploss)) {
                if (signalDataItems.profit_loss!!.contains("-")) {
                } else {
                    var PL: Double = signalDataItems.profit_loss!!.toDouble()
                    PL = PL * (-1)
                    profitLoss.setText(PL.toString().toString())
                }
                if (stopLossYes.text.toString().equals(objects.yes)) {
                    stopLoss.setVisibility(View.VISIBLE)
                    stopLossText.setText(objects.stoplosstext)
                    date.setTextColor(itemView.context.getResources().getColor(R.color.stoploss))
                    signals.setTextColor(itemView.context.getResources().getColor(R.color.stoploss))
                    profitLoss.setTextColor(
                        itemView.context.getResources().getColor(R.color.stoploss))
                }
            }
            signals.setOnClickListener(View.OnClickListener {
                if (signalDataItems.url.equals("null") && !signalDataItems.url.equals("")) {
                    val intent = Intent(itemView.context, Mainframe::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
                    intent.putExtra("url", signalDataItems.url)
                    itemView.context.startActivity(intent)
                    //Toast.makeText(context,"URL"+current.url,Toast.LENGTH_SHORT).show();
                }
            })
        }
    }
}